#!/usr/bin/env python
# coding: utf-8

# In[36]:


items = [{
        "metadata": {
            "album": "Amar Varso Gunatitno",
            "artist": "Sadhu Madhurvadandas",
            "category": "mangalacharan",
            "duration": 111,
            "title": "Mangalacharan - Amar Varso",
            "es":"1",
            "user":"dipak"
        },
        "views": 5,
        "item_type": 300,
        "item_uuid": "1c0495bd-1632-464a-a67c-42b21239c742",
        "tags": {
            "location": { "location_id": 1, "location_name": "mumbai" },
            "tag": { "tag_id": 1, "tag_name": "janm jayanti" }
        }
    },
    {
        "metadata": {
            "album": "Amar Varso Gunatitno",
            "artist": "Jaideep Swadiya",
            "category": "multiple",
            "duration": 1123,
            "title": "Amar Varso Gunatitno",
            "es":"1",
            "user":"dipak"
        },
        "views": 3133,
        "item_type": 300,
        "item_uuid": "9148cbd5-b0a2-442d-a9b9-4fd7b5039fb4",
        "tags": {
            "location": { "location_id": 1, "location_name": "mumbai" },
            "tag": { "tag_id": 1, "tag_name": "janm jayanti" }
        }
    },
    {
        "metadata": {
            "album": "Amar Varso Gunatitno",
            "artist": "Jaideep Swadiya",
            "category": "multiple",
            "duration": 316,
            "title": "Varasi Harini Krupa Apar",
            "es":"1",
            "user":"dipak"
        },
        "views": 20,
        "item_type": 300,
        "item_uuid": "1740ff4b-798a-48ca-a9d3-a41762076d0b",
        "tags": {
            "location": { "location_id": 1, "location_name": "mumbai" },
            "tag": { "tag_id": 1, "tag_name": "janm jayanti" }
        }
    },
    {
        "metadata": {
            "album": "Amar Varso Gunatitno",
            "artist": "Sadhu Madhurkirtandas",
            "category": "vachnamrut",
            "duration": 145,
            "title": "Vachnamrut Poojan Shloks",
            "es":"1",
            "user":"dipak"
        },
        "views": 32,
        "item_type": 300,
        "item_uuid": "34bd12f9-c75a-42f2-990c-41591338510d",
        "tags": {
            "location": { "location_id": 1, "location_name": "mumbai" },
            "tag": { "tag_id": 1, "tag_name": "janm jayanti" }
        }
    },
    {
        "metadata": {
            "album": "Amar Varso Gunatitno",
            "artist": "Jaideep Swadiya",
            "category": "vachnamrut",
            "duration": 859,
            "title": "Vachnamrut Mein Sant Ke Gun",
            "es":"1",
            "user":"dipak"
        },
        "views": 42,
        "item_type": 300,
        "item_uuid": "cf1107c0-7eaa-4cca-befb-c443f371c64b",
        "tags": {
            "location": { "location_id": 1, "location_name": "mumbai" },
            "tag": { "tag_id": 1, "tag_name": "janm jayanti" }
        }
    },
    {
        "metadata": {
            "album": "Amar Varso Gunatitno",
            "artist": "Sadhu Shobhitswarupdas",
            "category": "vachnamrut",
            "duration": 769,
            "title": "Vachnamrut Ki Jay Jay Ho",
            "es":"1",
            "user":"dipak"
        },
        "views": 24,
        "item_type": 300,
        "item_uuid": "b1041717-7a6d-4473-8f3b-89898fe09b76",
        "tags": {
            "location": { "location_id": 1, "location_name": "mumbai" },
            "tag": { "tag_id": 1, "tag_name": "janm jayanti" }
        }
    },
    {
        "metadata": {
            "album": "Amar Varso Gunatitno",
            "artist": "Sadhu Somyakirtandas",
            "category": "vachnamrut",
            "duration": 575,
            "title": "Vachnamrut Mein Shree Hari Kahete",
            "es":"1",
            "user":"dipak"
        },
        "views": 53,
        "item_type": 300,
        "item_uuid": "3c511ef8-b78a-438e-968f-15b10051c915",
        "tags": {
            "location": { "location_id": 1, "location_name": "mumbai" },
            "tag": { "tag_id": 1, "tag_name": "janm jayanti" }
        }
    },
    {
        "metadata": {
            "album": "Amar Varso Gunatitno",
            "artist": "Jaideep Swadiya",
            "category": "mahant swami",
            "duration": 616,
            "title": "Gunona Mahasagar Mahant Swami",
            "es":"1",
            "user":"dipak"
        },
        "views": 241,
        "item_type": 300,
        "item_uuid": "db858f33-b284-44f2-9b7f-3ccec2401874",
        "tags": {
            "location": { "location_id": 1, "location_name": "mumbai" },
            "tag": { "tag_id": 1, "tag_name": "janm jayanti" }
        }
    },
    {
        "metadata": {
            "album": "Param Ekantik Avya",
            "artist": "Jaydeep Swadla",
            "category": "mangalacharan",
            "duration": 83,
            "title": "Mangalacharan",
            "es":"1",
            "user":"dipak"
        },
        "views": 32,
        "item_type": 300,
        "item_uuid": "f67555ac-c9a5-4937-ba73-acae2b5f867f",
        "tags": {
            "location": { "location_id": 2, "location_name": "brisbane" },
            "tag": { "tag_id": 2, "tag_name": "shibir" }
        }
    },
    {
        "metadata": {
            "album": "Param Ekantik Avya",
            "artist": "Jaydeep Swadla",
            "category": "swagat",
            "duration": 955,
            "title": "Swagat Geet",
            "es":"1",
            "user":"dipak"
        },
        "views": 232,
        "item_type": 300,
        "item_uuid": "2ebfad91-1aee-4296-a4b9-a40d95862c28",
        "tags": {
            "location": { "location_id": 2, "location_name": "brisbane" },
            "tag": { "tag_id": 2, "tag_name": "shibir" }
        }
    },
    {
        "metadata": {
            "album": "Param Ekantik Avya",
            "artist": "Amey Daate",
            "category": "diksha",
            "duration": 868,
            "title": "Diksha Geet",
            "es":"1",
            "user":"dipak"
        },
        "views": 5,
        "item_type": 300,
        "item_uuid": "59aa6192-1536-4d76-9248-59a43b0265f7",
        "tags": {
            "location": { "location_id": 2, "location_name": "Brisbane" },
            "tag": { "tag_id": 2, "tag_name": "Shibir" }
        }
    },
    {
        "metadata": {
            "album": "Param Ekantik Avya",
            "artist": "Jaydeep Swadla",
            "category": "bal",
            "duration": 653,
            "title": "Bal Geet",
            "es":"1",
            "user":"dipak"
        },
        "views": 35,
        "item_type": 300,
        "item_uuid": "9de92a71-44b1-4a9a-8b60-d0103de79fc8",
        "tags": {
            "location": { "location_id": 2, "location_name": "Brisbane" },
            "tag": { "tag_id": 2, "tag_name": "Shibir" }
        }
    },
    {
        "metadata": {
            "album": "Param Ekantik Avya",
            "artist": "Amey Daate",
            "category": "swagat",
            "duration": 927,
            "title": "Cfi Nrutyamala",
            "es":"1",
            "user":"dipak"
        },
        "views": 537,
        "item_type": 300,
        "item_uuid": "ff4f1ee9-1820-41ed-bd09-f2e8c35746e9",
        "tags": {
            "location": { "location_id": 2, "location_name": "brisbane" },
            "tag": { "tag_id": 2, "tag_name": "Shibir" }
        }
    },
    {
        "metadata": {
            "album": "Param Ekantik Avya",
            "artist": "Jaydeep Swadla",
            "category": "prayer",
            "duration": 672,
            "title": "Ek Phool Amaru Swikaro",
            "es":"1",
            "user":"dipak"
        },
        "views": 357,
        "item_type": 300,
        "item_uuid": "e62542c0-13fa-4cb1-8af8-e705d9bfc9d1",
        "tags": {
            "location": { "location_id": 2, "location_name": "brisbane" },
            "tag": { "tag_id": 2, "tag_name": "Shibir" }
        }
    }
]


# In[37]:


device = {
            "site_id": 2,
            "device_type": 2,
            "details": {
                "key": "value"
            },
            "resource_path": "/"
        }


# In[38]:


item_types = [
        {
            "item_type_id": 100,
            "item_type": "video"
        },
        {
            "item_type_id": 200,
            "item_type": "image"
        },
        {
            "item_type_id": 300,
            "item_type": "audio"
        }
]


# In[39]:


mappings = {
    "100":"video",
    "200":"image",
    "300":"audio"
}


# In[40]:


import requests
from elasticsearch import Elasticsearch
import pandas as pd
import json


# In[41]:


HOST = "http://0.0.0.0:8000"
USERNAME = "docker@gmail.com"
PASSWORD = "docker"
ES_URI = "http://elastic:elastic@0.0.0.0:9200"


# In[42]:


AUTH_URL = HOST + "/auth/login/"
ITEM_TYPE_URL = HOST + "/api/item-types/"
ITEM_URL = HOST + "/api/item/"
ASSET_URL = HOST + "/api/asset/"
FILE_URL = HOST + "/api/file/"
DEVICE_URL = HOST + "/api/device/"


# In[43]:


auth_body = {
    "email":USERNAME,
    "password":PASSWORD
}


# In[44]:


response = requests.post(AUTH_URL, json=auth_body)


# In[45]:


if response.status_code == 200:
    TOKEN = response.json()['token']
else:
    raise Exception("Unable to Login")


# In[46]:


header = {"Authorization":f"jwt {TOKEN}"}


# In[47]:


TOKEN


# In[151]:


data = pd.read_csv("./audioData.csv", header=[0,1,2])


# In[152]:


filename = data['filename', 'filename', 'filename']


# In[153]:


for item_type in  item_types:
    print(item_type)
    response = requests.post(ITEM_TYPE_URL, json=item_type, headers=header)
    if response.status_code != 201:
        print(response.status_code)
        raise Exception("Unable to add item type")


# In[ ]:


response = requests.post(DEVICE_URL, json=device, headers=header)
if response.status_code != 201:
    print(response.status_code)
    raise Exception("Unable to add device")
device_uuid = response.json()['device_uuid']


# In[50]:


es = Elasticsearch(hosts=ES_URI)


# In[ ]:


for index, item in  enumerate(items):
    try:
        del item['item_uuid']
    except:
        pass
    response = requests.post(ITEM_URL, json=item, headers=header)
    if response.status_code != 201:
        raise Exception("Unable to add item")
    item_uuid = response.json()['item_uuid']
    item['item_uuid'] = item_uuid
    res = es.index(index="audio", id=item["item_uuid"], body=item)
    asset = {
                "asset_type": 300,
                "mime_type": "audio/mp3",
                "metadata": {
                    "key": "value"
                },
                "item_uuid": item_uuid
            }
    response = requests.post(ASSET_URL, json=asset, headers=header)
    if response.status_code != 201:
        raise Exception("Unable to add asset")
    asset_uuid = response.json()['asset_uuid']
    file = {
                "path": "data",
                "filename": filename[index],
                "asset_uuid": asset_uuid,
                "device_uuid": device_uuid
            }
    response = requests.post(FILE_URL, json=file, headers=header)
    if response.status_code != 201:
        raise Exception("Unable to add file")


# In[156]:


response.text


# In[23]:


item = items[0]


# In[26]:


es_index = mappings[str(item['item_type'])]


# In[ ]:


mappings


# In[35]:


videos = [{
    "metadata": {
        "album": "Moksha",
        "artist": "Sadhu Madhurvadandas",
        "category": "shibir",
        "duration": 111,
        "title": "Jo hoy himmat re",
    },
    "views": 5,
    "item_type": 100,
    "item_uuid": "1c0495bd-1632-464a-a67c-42b21239c742",
    "tags": {
        "location": { "location_id": 10,"location_name": "Atlanta" },
        "tag": { "tag_id": 1, "tag_name": "NC18" }
    }
},
{
    "metadata": {
        "album": "Moksha",
        "artist": "Jaideep Swadiya",
        "category": "dhun",
        "duration": 270,
        "title": "NC18 Moksha Dhun",
    },
    "views": 210,
    "item_type": 100,
    "item_uuid": "1c0495bd-1632-464a-a67c-42b21239c742",
    "tags": {
        "location": { "location_id": 10,"location_name": "Atlanta" },
        "tag": { "tag_id": 1, "tag_name": "NC18" }
    }
},
{
    "metadata": {
        "album": "Moksha",
        "artist": "Sadhu Madhurvadandas",
        "category": "shibir",
        "duration": 324,
        "title": "Mohan Ne Gamva ne Ichchho",
    },
    "views": 521,
    "item_type": 100,
    "item_uuid": "1c0495bd-1632-464a-a67c-42b21239c742",
    "tags": {
        "location": { "location_id": 10,"location_name": "Atlanta" },
        "tag": { "tag_id": 1, "tag_name": "NC18" }
    }
}]


# In[48]:


video_files = ["v1.mp4", "v2.mp4", "v3.mp4"]


# In[52]:


for index, item in  enumerate(videos):
    try:
        del item['item_uuid']
    except:
        pass
    es_index = mappings[str(item['item_type'])]
    response = requests.post(ITEM_URL, json=item, headers=header)
    if response.status_code != 201:
        raise Exception("Unable to add item")
    item_uuid = response.json()['item_uuid']
    item['item_uuid'] = item_uuid
    res = es.index(index=es_index, id=item["item_uuid"], body=item)
    asset = {
                "asset_type": 100,
                "mime_type": "video/mp4",
                "metadata": {
                    "key": "value"
                },
                "item_uuid": item_uuid
            }
    response = requests.post(ASSET_URL, json=asset, headers=header)
    if response.status_code != 201:
        raise Exception("Unable to add asset")
    asset_uuid = response.json()['asset_uuid']
    file = {
                "path": "data",
                "filename": video_files[index],
                "asset_uuid": asset_uuid,
                "device_uuid": device_uuid
            }
    response = requests.post(FILE_URL, json=file, headers=header)
    if response.status_code != 201:
        raise Exception("Unable to add file")


# In[ ]:




