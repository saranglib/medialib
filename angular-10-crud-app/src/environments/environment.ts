// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  host : 'http://127.0.0.1:8000',
  nginx_host: 'http://127.0.0.1:8000',
  device_uuid:'77fd9e08-96a9-46dc-a2aa-803deb8f0228',
  path : 'E:\Project\MediaLibrary\medialib\restApisMySQL\data'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
