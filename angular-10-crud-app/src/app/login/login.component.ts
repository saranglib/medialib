import { Component, OnInit,Inject } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { AuthService } from './auth.service'
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  editMode = false;
  loginForm: FormGroup;
  error: any;

  constructor(private authService:AuthService,private toastr:ToastrService,private router: Router,@Inject(DOCUMENT) private document: Document) { }

  ngOnInit(): void {
   
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    const email = form.value.username;
    const password = form.value.password;
    
    this.authService.login(email, password).subscribe(
      (success =>{
        this.router.navigate(['/cover-page'])
      }) ,
      error => this.toastr.error('UserId and Password is wrong')
    );
    
    form.reset();
  }

}
