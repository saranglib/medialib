import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastViewedRendererComponent } from './last-viewed-renderer.component';

describe('LastViewedRendererComponent', () => {
  let component: LastViewedRendererComponent;
  let fixture: ComponentFixture<LastViewedRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastViewedRendererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastViewedRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
