import { Component } from '@angular/core';
import { ICellRendererAngularComp } from  'ag-grid-angular';

@Component({
  selector: 'app-last-viewed-renderer',
  templateUrl: './last-viewed-renderer.component.html',
  styleUrls: ['./last-viewed-renderer.component.css']
})
export class LastViewedRendererComponent implements ICellRendererAngularComp {

  public params;
  public date;
  
  constructor(){
  }
  
  agInit(params: any): void {
    this.params = params;
    this.date = this.params.data.time;
    this.date = this.formatAMPM(this.date)
  }

  checkDigit(t)
  {
    return ( t < 10 ) ? `0${t}` : t;
  }

  formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12;
    hours = this.checkDigit(hours)
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

  refresh(): boolean {
    return false;
  }
}
