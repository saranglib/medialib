import { Component } from '@angular/core';
import { ICellRendererAngularComp  } from 'ag-grid-angular';
import { faStar, faEye } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-video-title-renderer',
  templateUrl: './video-title-renderer.component.html',
  styleUrls: ['./video-title-renderer.component.css']
})
export class VideoTitleRendererComponent implements ICellRendererAngularComp {

  public params: any;
  public title;
  public view;

  faStar = faStar;
  faEye = faEye;
  
  constructor() { }

  agInit(params:any): void {
    this.params = params;
    this.title = this.params.data.title;
    this.view = this.params.data.view;
  }
  refresh(): boolean {
    return false;
  }
}
