import { Component,ViewEncapsulation } from '@angular/core';
import { ICellRendererAngularComp  } from 'ag-grid-angular';
import { faInfoCircle, faEye, faStar } from '@fortawesome/free-solid-svg-icons'; 

@Component({
  selector: 'app-info-renderer',
  templateUrl: './info-renderer.component.html',
  styleUrls: ['./info-renderer.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class InfoRendererComponent implements ICellRendererAngularComp {

  public params: any;
  public views: number;
  public title: string; 
  public rating: number;
  public desc;

  faInfoCircle = faInfoCircle;
  faEye = faEye;
  faStar = faStar;

  constructor() { }

  agInit(params: any): void {
    this.params = params;
    this.title = this.params.data.title;
    this.desc = this.params.data.trackInfo;
    this.views = this.params.data.view;
    this.rating = this.params.data.rating;
  }

  refresh(): boolean {
    return false;
  }
}
