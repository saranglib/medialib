import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoRendererComponent } from './info-renderer.component';

describe('InfoRendererComponent', () => {
  let component: InfoRendererComponent;
  let fixture: ComponentFixture<InfoRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoRendererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
