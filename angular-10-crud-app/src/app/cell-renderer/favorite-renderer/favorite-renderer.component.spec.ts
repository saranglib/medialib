import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoriteRendererComponent } from './favorite-renderer.component';

describe('FavoriteRendererComponent', () => {
  let component: FavoriteRendererComponent;
  let fixture: ComponentFixture<FavoriteRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoriteRendererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoriteRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
