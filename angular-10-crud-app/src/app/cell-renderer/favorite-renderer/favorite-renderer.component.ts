import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp  } from 'ag-grid-angular';
import { faHeart ,faHeartBroken} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-favorite-renderer',
  templateUrl: './favorite-renderer.component.html',
  styleUrls: ['./favorite-renderer.component.css']
})
export class FavoriteRendererComponent implements ICellRendererAngularComp,OnInit {

  public params: any;
  public favourites?: boolean;

  faHeart = faHeart;
  faHeartBroken = faHeartBroken;
  constructor() { 
  }
  ngOnInit():void{
  }
  agInit(params: any): void{
    
    this.params = params;
    this.favourites = this.params.data.favourites;
  }

  refresh() :boolean{
   return false;
 }
}
