import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-edit-renderer',
  templateUrl: './edit-renderer.component.html',
  styleUrls: ['./edit-renderer.component.css']
})
export class EditRendererComponent implements ICellRendererAngularComp {

  public params: any;

  faPencilAlt = faPencilAlt;
  
  constructor() { }

  agInit(params: any): void {
    this.params = params;
  }

  refresh(): boolean {
    return false;
  }
}
