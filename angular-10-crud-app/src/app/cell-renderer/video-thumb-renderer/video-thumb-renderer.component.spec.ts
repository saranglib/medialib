import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoThumbRendererComponent } from './video-thumb-renderer.component';

describe('VideoThumbRendererComponent', () => {
  let component: VideoThumbRendererComponent;
  let fixture: ComponentFixture<VideoThumbRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoThumbRendererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoThumbRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
