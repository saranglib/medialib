import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-video-thumb-renderer',
  templateUrl: './video-thumb-renderer.component.html',
  styleUrls: ['./video-thumb-renderer.component.css']
})
export class VideoThumbRendererComponent implements ICellRendererAngularComp {
  public isImage:boolean = true;
  public params: any;
  public src;
  public duration;
  public id;

  constructor() { }

  agInit(params:any): void {
    this.params = params;
    this.src = this.params.data.vposter;
    this.id = this.params.data.id;
    if(this.src.includes('#')){
      this.isImage = false;
    }
    
    this.duration = this.params.data.duration;
  }

  refresh(): boolean {
    return false;
  }
}
