import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-delete-renderer',
  templateUrl: './delete-renderer.component.html',
  styleUrls: ['./delete-renderer.component.css']
})
export class DeleteRendererComponent implements ICellRendererAngularComp {

  public params: any;
  faTrashAlt = faTrashAlt;

  constructor() { }

  agInit(params: any): void {
    this.params = params;    
  }

  refresh(): boolean {
      return false;
  }
}
