import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-group-date-renderer',
  templateUrl: './group-date-renderer.component.html',
  styleUrls: ['./group-date-renderer.component.css']
})
export class GroupDateRendererComponent implements ICellRendererAngularComp {

  public params: any;
  public date;
  constructor() { }

  agInit(params: any): void {
    this.params = params;
    this.date = this.params.node.key;
    this.date = this.formatDate(this.params.node.key.split(" "));
  }

  formatDate(d){
    var date = d[2];
    var month = d[1];
    var year = d[3];
    var today = new Date();
    if(today.getDate() === date && today.getMonth() === this.formatMonth(month) && today.getFullYear() === year){
      return 'Today';
    }
    else if(today.getDate()-1 === date && today.getMonth() === this.formatMonth(month) && today.getFullYear() === year)
    {
      return 'Yesterday';
    }
    else
    {
      var post;
      if(date%10 == 1)
      {
        post = 'st';
      }
      else if (date%10 == 2)
      {
        post = 'nd';
      }
      else if (date%10 == 3)
      {
        post = 'rd';
      }
      else
      {
        post = 'th';
      }
      return `${date}${post} ${month}. ${year}`;
    }
  }  

  formatMonth(month)
  { 
    var m;
    if(month == 'Jan'){ m = 0 }
    else if(month == 'Feb'){ m = 1}
    else if(month == 'Mar'){ m = 2}
    else if(month == 'Apr'){ m = 3}
    else if(month == 'May'){ m = 4}
    else if(month == 'Jun'){ m = 5}
    else if(month == 'Jul'){ m = 6}
    else if(month == 'Aug'){ m = 7}
    else if(month == 'Sep'){ m = 8}
    else if(month == 'Oct'){ m = 9}
    else if(month == 'Nov'){ m = 10}
    else { m = 11}
    return m;
  }

  refresh(): boolean {
      return false;
  }
}
