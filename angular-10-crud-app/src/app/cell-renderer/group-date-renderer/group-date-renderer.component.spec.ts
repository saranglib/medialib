import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupDateRendererComponent } from './group-date-renderer.component';

describe('GroupDateRendererComponent', () => {
  let component: GroupDateRendererComponent;
  let fixture: ComponentFixture<GroupDateRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupDateRendererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupDateRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
