import { Component } from '@angular/core';
import { ICellRendererAngularComp  } from 'ag-grid-angular';
import { faStar } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-rating-renderer',
  templateUrl: './rating-renderer.component.html',
  styleUrls: ['./rating-renderer.component.scss']
})
export class RatingRendererComponent  implements ICellRendererAngularComp {
  public params: any;
  faStar =faStar;
  public rating: number;
  constructor() { }

  agInit(params: any): void{
    this.params = params;
    this.rating = this.params.data.rating;
  }

  refresh(): boolean {
    return false;
  }
} 
