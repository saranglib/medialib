import { Component, NgZone, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { DeleteRendererComponent } from 'src/app/cell-renderer/delete-renderer/delete-renderer.component';
import { EditRendererComponent } from 'src/app/cell-renderer/edit-renderer/edit-renderer.component';
import { BookmarkList } from '../bookmark.model';
import { BookmarkService } from '../bookmark.service';
@Component({
  selector: 'app-bookmark-details',
  templateUrl: './bookmark-details.component.html',
  styleUrls: ['./bookmark-details.component.css']
})
export class BookmarkDetailsComponent implements OnInit {

  faTrashAlt = faTrashAlt;

  public gridApi;
  public gridColumnApi;
  public rowData;
  public columnDefs;
  public rowSelection;
  public frameworkComponents;
  public rowMultiSelectWithClick;
  public selectedList:  BookmarkList;
  public total: number;
  public rowSelected: boolean;

  constructor(private bs: BookmarkService, private route: ActivatedRoute, private router: Router, private ngZone: NgZone) {

    this.selectedList = this.bs.getBookmarkById(this.route.snapshot.params['id']);
    this.total = this.selectedList.bookmarks.length;
    this.rowSelected = false;
    this.rowData = this.selectedList.bookmarks;

    this.columnDefs  = [
      {
        flex: 1,
        field: 'bookmark',
        headerName:"Time",
        checkboxSelection: true,
        headerCheckboxSelection: true,
        maxWidth :120,
        sortable: true,
        filter: true,
        cellClass: "grid-cell-centered",
      },
      {
        flex: 1,
        field: 'description',
        headerName:"Notes",
        resizable: true,
        wrapText: true,
        autoHeight: true,
        editable: true,
      },
      {
        flex: 1,
        field: 'Edit',
        maxWidth: 100,
        cellClass: ["grid-cell-centered","icon-size"],
        cellRenderer: 'editRenderer',
      },
      {
        flex: 1,
        field: 'Delete',
        maxWidth: 100,
        cellClass: ["grid-cell-centered","icon-size"],
        cellRenderer: 'deleteRenderer',
      }
    ];
    this.frameworkComponents = {
      deleteRenderer:DeleteRendererComponent,
      editRenderer:EditRendererComponent
    };
    this.rowSelection= 'multiple';
    this.rowMultiSelectWithClick= true;
   }

  ngOnInit(): void {
    this.route.params
    .subscribe(
      (params: Params)=>{
        this.selectedList = this.bs.getBookmarkById(this.route.snapshot.params['id']);
        this.rowData = this.selectedList.bookmarks;
        this.total = this.rowData.length;
      }
    );
  }

  onFirstDataRendered(params)
  {
    params.api.sizeColumnsToFit();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  onSelectionChanged(params){
    var selectedRows = this.gridApi.getSelectedRows();
    this.rowSelected = selectedRows.length > 0 ? true: false;
  }

  onCellCilcked(params){
    if(params.column.colDef.field === "Edit"){
      this.ngZone.run(()=>
      this.router.navigate(['edit'],{relativeTo: this.route, queryParams:{'id':params.data.id}})
      );
    }
    else if(params.column.colDef.field === "Delete"){
      this.bs.bookmarkDelete(params.data.id);      
    }
  }
}
