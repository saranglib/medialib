import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Bookmark, BookmarkList } from '../bookmark.model';
import { BookmarkService } from '../bookmark.service';

@Component({
  selector: 'app-bookmark-edit',
  templateUrl: './bookmark-edit.component.html',
  styleUrls: ['./bookmark-edit.component.css']
})
export class BookmarkEditComponent implements OnInit {

  public selectedList: BookmarkList;
  public selectedBookmark: Bookmark;
  public total;
  public edit_id:string;
  public desc:string ;
  constructor(private bs:BookmarkService, private route: ActivatedRoute, private router:Router) {
   }

  ngOnInit(): void {
    this.route.queryParams
    .subscribe(
      (queryParams:Params)=>{
        this.edit_id = queryParams['id'];
        this.selectedList = this.bs.getBookmarkById(this.route.snapshot.params['id']);
        this.total = this.selectedList.bookmarks.length;
        var id = this.selectedList.bookmarks.findIndex(x=>x.id == queryParams['id']);
        this.selectedBookmark = this.selectedList.bookmarks[id];
        this.desc = this.selectedBookmark.description;
      }
    );
  };

  onCancel(){
    this.router.navigate(['../../', this.route.snapshot.params['id']], {relativeTo: this.route});
  }

  onDone(){   
    this.bs.bookmarkEdit(this.edit_id,this.selectedBookmark.description);
    this.router.navigate(['../../',this.route.snapshot.params['id']],{relativeTo:this.route});
  }

}
