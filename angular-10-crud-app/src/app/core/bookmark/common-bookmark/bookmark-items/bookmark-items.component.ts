import { Component, OnInit } from '@angular/core';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { BookmarkList } from '../bookmark.model';
import { BookmarkService } from '../bookmark.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-bookmark-items',
  templateUrl: './bookmark-items.component.html',
  styleUrls: ['./bookmark-items.component.css']
})
export class BookmarkItemsComponent implements OnInit {

  faTrashAlt = faTrashAlt;
  subscription: Subscription;


  public bookmarks: BookmarkList[];
  constructor(private bs:BookmarkService) { 
    this.bookmarks = this.bs.getBookmarks();
  }

  ngOnInit(): void {
    this.subscription = this.bs.BookmarkChanged.subscribe(
      (videos: BookmarkList[]) => {
        this.bookmarks = this.bs.getBookmarks();                
      }
      );
    this.bookmarks = this.bs.getBookmarks();
  }

}
