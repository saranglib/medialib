import { BookmarkList,Bookmark } from "./bookmark.model";
import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Subject } from 'rxjs';
import { Video } from '../../play/video-player/video.model'
import { Router } from "@angular/router";

@Injectable()
export class BookmarkService implements OnInit{
    private bookmarks : BookmarkList[] = [];
    BookmarkChanged = new Subject<BookmarkList[]>();
    host = environment.host;
    public href: string = "";
    ;
    BookmarkRequest() {
      this.href = this.router.url;
        if(this.href.includes('audio')){
          this.http
          .get<BookmarkList[]>(this.host+'/api/bookmark/?type=audio',
          )
          .subscribe(              
            data => {                         
              this.setBookmarkList(data);           
            }        
          );  
        }
        if(this.href.includes('video')){
          this.http
          .get<BookmarkList[]>(this.host+'/api/bookmark/?type=video',
          )
          .subscribe(              
            data => {                         
              this.setBookmarkList(data);           
            }        
          );   
        }
          
     }
   
     constructor(public http: HttpClient,private router: Router) {    
       this.BookmarkRequest();       
     }

     setBookmarkList(bmmlist: any[]) {
       
        var tempBookmarksArray:BookmarkList[]=[];
        bmmlist.forEach(function(bookmrklistelement){                  
            var bmlist:Bookmark[]=[];
            bookmrklistelement.data.forEach(function(bm){
                var obj:Bookmark = new Bookmark(bm.id,bm.time,bm.description);
                bmlist.push(obj)
            })

            var bookmark_list_element:BookmarkList = new BookmarkList(bookmrklistelement.id,bookmrklistelement.title,bmlist);
            tempBookmarksArray.push(bookmark_list_element);            
        })

        
        this.bookmarks = tempBookmarksArray;
        this.BookmarkChanged.next(this.bookmarks.slice());    
        
      }
    getBookmarks(){
        return this.bookmarks.slice();
    }

    getBookmarkById(id:string){        
        return this.bookmarks[this.bookmarks.findIndex(x=>x.id == id)];
    }

    bookmarkEdit(id:string,description:string){            
      let bodyData = {"name": description};

      return this.http      
        .patch(this.host+'/api/bookmark/'+id+'/',bodyData,{observe:'response'})
        .subscribe(
          data => {                     
          }
        )  
    }

    bookmarkDelete(id:string){
      return this.http.delete(this.host+'/api/bookmark/'+id+'/',{observe:'response'})
        .subscribe(
          data => {            
            this.ngOnInit();
            this.BookmarkRequest();            
          }
        )
    }

    ngOnInit(){
      this.BookmarkRequest(); 
    }
}