import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonBookmarkComponent } from './common-bookmark.component';

describe('CommonBookmarkComponent', () => {
  let component: CommonBookmarkComponent;
  let fixture: ComponentFixture<CommonBookmarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonBookmarkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonBookmarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
