export class Bookmark {
    public id: string;
    public bookmark: string;
    public description: string;

    constructor(id: string, bookmark: string, description:string){
        this.id = id;
        this.bookmark = bookmark;
        this.description = description;
    }
}

export class BookmarkList{
    public id: string;
    public title: string;
    public bookmarks: Bookmark[];

    constructor(id:string, title:string, bookmarks: Bookmark[])
    {
        this.id = id;
        this.title = title;
        this.bookmarks = bookmarks;
    }
}