import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { outputs } from '@syncfusion/ej2-angular-navigations/src/accordion/accordion.component';
import { AdminService } from 'src/app/admin/admin-service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],

})
export class CategoryComponent implements OnInit {

  @Output() categoryName = new EventEmitter<string>();

  showMyContainer: boolean = false;
  buttonflag: boolean = true;
  categories = [];
  categories1 = [];
  buttonname = "View All";


  ngOnInit() {
    this.adminService.getMetadata().subscribe(res => {

      this.categories = res["category"].slice(0, 7);
      this.categories1 = res["category"].slice(5, res["category"].length);
    });
  }
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private adminService: AdminService
  ) { }


  onClick() {
    this.showMyContainer = !this.showMyContainer;
    this.buttonflag = !this.buttonflag;
    if (this.buttonflag === true) {
      this.buttonname = "View All";
    }
    else {
      this.buttonname = "View Less";
    }
  }

  showDataofCategory(name:string){
    this.categoryName.emit(name);
  }
}
