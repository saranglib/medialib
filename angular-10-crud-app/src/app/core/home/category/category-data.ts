import { Category } from './category';

export const CATEGORIES: Category[] = [
  { id: 11, name: 'Album' },
  { id: 12, name: 'Artist' },
  { id: 13, name: 'Parayan' },
  { id: 14, name: 'Satsang' },
  { id: 15, name: 'Mahima' },
  { id: 16, name: 'Akshar-purushottam' },
  { id: 17, name: 'Dharm' },
  { id: 18, name: 'Kirtan' },
  { id: 19, name: 'Pravchan' },
];

export const CATEGORIES1: Category[] = [
  { id: 11, name: 'Album' },
  { id: 12, name: 'Artist' },
  { id: 13, name: 'Parayan' },
  { id: 14, name: 'Satsang' },
  { id: 15, name: 'Mahima' },
  { id: 16, name: 'Akshar' },
  { id: 17, name: 'Dharm' },
  { id: 18, name: 'Kirtan' },
  { id: 19, name: 'Pravchan' },
  { id: 11, name: 'Album' },
  { id: 12, name: 'Artist' },
  { id: 13, name: 'Parayan' },
  { id: 14, name: 'Satsang' },
  { id: 15, name: 'Mahima' },
  { id: 16, name: 'Akshar' },
  { id: 17, name: 'Dharm' },
  { id: 18, name: 'Kirtan' },
  { id: 19, name: 'Pravchan' },
];
