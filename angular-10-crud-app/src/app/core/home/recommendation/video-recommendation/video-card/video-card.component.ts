import { Component, Input, OnInit } from '@angular/core';
import { faStar,faEye,faHeart,faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { Video } from '../../../../play/video-player/video.model';

@Component({
  selector: 'app-video-card',
  templateUrl: './video-card.component.html',
  styleUrls: ['./video-card.component.css']
})
export class VideoCardComponent implements OnInit {

  @Input() video: Video;

  faStar = faStar;
  faEye =  faEye;
  faHeart = faHeart;
  faInfo = faInfoCircle;

  constructor() { }

  ngOnInit(): void {    
  }

}
