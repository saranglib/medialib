import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoRecommendationComponent } from './video-recommendation.component';

describe('VideoRecommendationComponent', () => {
  let component: VideoRecommendationComponent;
  let fixture: ComponentFixture<VideoRecommendationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoRecommendationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoRecommendationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
