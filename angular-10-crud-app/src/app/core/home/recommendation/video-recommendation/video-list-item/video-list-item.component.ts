import { Component, OnInit, Input } from '@angular/core';
import { Video } from '../../../../play/video-player/video.model';

import { faStar,faEye, faHeart,faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faStar as faStarR } from "@fortawesome/free-regular-svg-icons";

@Component({
  selector: 'app-video-list-item',
  templateUrl: './video-list-item.component.html',
  styleUrls: ['./video-list-item.component.css'],
})
export class VideoListItemComponent implements OnInit {
  @Input() video: Video;
  @Input() index: number;

  Arr = Array; //Array type captured in a variable
  

  faEye = faEye;
  faStarR = faStarR;
  faStar = faStar;
  faHeart = faHeart;
  faInfoCircle = faInfoCircle;

  constructor() {}

  ngOnInit(): void {}
}
