import { Component, OnInit, OnDestroy } from '@angular/core';

import { Video } from '../../../play/video-player/video.model';
import { VideoService } from '../../../play/video-player/video.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { faTh, faThList } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-video-recommendation',
  templateUrl: './video-recommendation.component.html',
  styleUrls: ['./video-recommendation.component.css'],
})
export class VideoRecommendationComponent implements OnInit, OnDestroy {
  
  subscription: Subscription;
  videos: Video[];

  faTh = faTh;
  faThList = faThList;

  public thumbview: boolean = true;

  constructor(private videoService: VideoService) {
    
  }

  onViewChange(){
    this.thumbview = !this.thumbview ;
  }

  ngOnInit(): void {
    
    
  }

  ngOnDestroy(){
    
  }

}
