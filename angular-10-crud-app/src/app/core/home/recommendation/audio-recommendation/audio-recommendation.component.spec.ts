import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioRecommendationComponent } from './audio-recommendation.component';

describe('AudioRecommendationComponent', () => {
  let component: AudioRecommendationComponent;
  let fixture: ComponentFixture<AudioRecommendationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudioRecommendationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioRecommendationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
