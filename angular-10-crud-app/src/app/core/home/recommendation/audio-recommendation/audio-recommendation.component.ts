import { Component, OnInit } from '@angular/core';
import { AudioService } from '../../../play/audio-player/audio.service';
import { ToastrService } from 'ngx-toastr';
import { InfoRendererComponent } from 'src/app/cell-renderer/info-renderer/info-renderer.component';
import { FavoriteRendererComponent } from 'src/app/cell-renderer/favorite-renderer/favorite-renderer.component';
import { RatingRendererComponent } from 'src/app/cell-renderer/rating-renderer/rating-renderer.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Audio } from '../../../play/audio-player/audio.model'
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AudioAddToPlaylistComponent } from 'src/app/core/play/audio-player/audio-add-to-playlist/audio-add-to-playlist.component';
import { AudioFavoriteService } from 'src/app/core/Favorite/audio-favorite/audio-favorite.service';
import { QuestionService } from 'src/app/core/search/question.service';
@Component({
  selector: 'app-audio-recommendation',
  templateUrl: './audio-recommendation.component.html',
  styleUrls: ['./audio-recommendation.component.css']

})
export class AudioRecommendationComponent implements OnInit {
  buttonname = "View All";
  showMyContainer: boolean = false;
  buttonflag: boolean = true;
  audios: Audio[];
  subscription: Subscription;
  currentItem: Audio;
  modalRef: BsModalRef;
  bsModalRef: BsModalRef;
  id: number;
  public gridApi;
  public rowData;
  public columnDefs;
  public rowSelection;
  public rowClassRules;
  public gridColumnApi;
  public selectedRowId;
  public frameworkComponents;
  public tooltipShowDelay;
  public gridOptions;
  headerName: string = "Recently Played";
  
  ngOnInit(): void {
    this.subscription = this.as.SuggestionChanged.subscribe(
      (audios: Audio[]) => {
        this.rowData = this.as.getPlaylist();
      }
    );
    this.rowData = this.as.getPlaylist();

  }
  
  constructor(
    protected as: AudioService,
    private toastr: ToastrService,
    private fs: AudioFavoriteService,
    private router: Router,
    private qs: QuestionService,
    private route: ActivatedRoute,
    private modalService: BsModalService) {
    this.rowData = this.as.getPlaylist();
    this.selectedRowId = as.selectedRowId;

    this.columnDefs = [
      {
        field: 'id',
        headerName: "",
        cellRenderer: function (params) {
          if (params.data.id == as.selectedRowId)
            return '<img src="./assets/loading-final.gif" width="20px" height="20px" style="margin-left:1rem">'
          return '<div class="icon icon-play" style="font-size:18px; margin-left: 0.5rem; padding-left:0.5rem"></div>';
        },
        width: 10,
        cellStyle: { textAlign: "center", 'font-size': '24px', 'letter-spacing': '1.2px' },

      },
      {
        field: 'title',
        sortable: true,
        filter: true,
        resizable: true,
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'artist',
        headerName: "Artist/Vakta",
        sortable: true,
        filter: true,
        resizable: true,
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'duration',
        sortable: true,
        resizable: false,
        width: 100,
        cellClass: "grid-cell-centered",
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'rating',
        sortable: true,
        resizable: false,
        width: 80,
        cellRenderer: 'ratingRenderer',
        cellClass: "grid-cell-centered",
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'favorite',
        cellRenderer: 'favoriteRenderer',
        width: 50,
        cellClass: "grid-cell-centered",
        sortable: true,
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'Queue',
        cellRenderer: function (params) { return '<div class="icon icon-queue"></div>' },
        width: 50,
        cellClass: "grid-cell-centered",
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'Playlist',
        cellRenderer: function (params) { return '<div class="icon icon-add-to-play-list"  style="font-size: 30px"></div>' },
        width: 50,
        cellClass: "grid-cell-centered",
      },
      {
        field: 'Info.',
        cellRenderer: 'infoRenderer',
        width: 50,
        cellClass: "grid-cell-centered",
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      }
    ];
    this.rowClassRules = {
      'playing': function (params) {
        var data = params.data.Id;
        return data == as.selectedRowId;
      },
    };
    this.rowSelection = 'single';
    this.frameworkComponents = {
      infoRenderer: InfoRendererComponent,
      favoriteRenderer: FavoriteRendererComponent,
      ratingRenderer: RatingRendererComponent,
    }
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

  }

  oncellClicked(params) {

    if (params.column.colDef.field === "favorite") {
      params.data.favourites = !params.data.favourites;
      const prom = this.as.FavoriteChange(params.data.id).toPromise();
      prom.then(res => {
        this.fs.audioFavoritesRequest().subscribe(res => {
          this.gridApi.redrawRows();
        });
      })
      if (params.data.favourites === true) {

        this.toastr.success('Added to favorite!');
      } else {
        this.toastr.warning('Removed from favorite!');
      }
      //this.favoriteChange();
    }
    else if (params.column.colDef.field === "Queue") {

      this.as.addItemToQueue(params.data.id);
      this.toastr.success(params.data.title + ' added to queue');
    }
    else if (params.column.colDef.field === "Playlist") {
      const initialState = {
        addAudio: params.data,
      };
      this.bsModalRef = this.modalService.show(AudioAddToPlaylistComponent, { initialState });
      // this.bsModalRef.content.id = params.data.id;
    }
    else if (params.column.colDef.field === "id" || params.column.colDef.field === "title") {
      var selectedRows = this.gridApi.getSelectedRows();
      this.as.selectedRowId = selectedRows.length === 1 ? selectedRows[0].Id : '';
      this.gridApi.redrawRows(params);
      this.router.navigate(['/audio', params.data.id, 'home']);

    }
    else if (params.column.colDef.field === "Info") {
      document.querySelector('#info').innerHTML === '<span popover="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."></span>'
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  getCategoryData(name: any) {
    //this.qs.getAudiobySearch(name) ;
    this.headerName = name;
    this.rowData = this.as.getNewmethod();
    this.gridApi.redrawRows();
  }

}
