import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Params, Router } from '@angular/router';
import { AudioService } from '../../play/audio-player/audio.service';
import { Audio } from '../../play/audio-player/audio.model'
import { Subscription } from 'rxjs';
import { ConsoleService } from '@ng-select/ng-select/lib/console.service';

@Component({
  selector: 'app-left-panel',
  templateUrl: './left-panel.component.html',
  styleUrls: ['./left-panel.component.css'],
})
export class LeftPanelComponent implements OnInit {
audioUrl:string = this.router.url;
currentItem:Audio;
id;
subscription: Subscription;
  constructor(private router: Router,
    private route: ActivatedRoute,
    private as :AudioService) {
    }
  ngOnInit(): void {
    this.as.currentItemChanged.subscribe(res => {
      this.id = res;
    })
      
  }
  getCurrentId(){
    this.currentItem = this.as.currentItem;
  }
 audio = this.audioUrl.includes("/audio");
 }

