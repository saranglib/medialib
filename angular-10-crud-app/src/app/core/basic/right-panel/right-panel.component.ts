import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AudioHistoryService } from '../../History/audio-history/audio-history.service';
import { AudioService } from '../../play/audio-player/audio.service';
@Component({
  selector: 'app-right-panel',
  templateUrl: './right-panel.component.html',
  styleUrls: ['./right-panel.component.css'],
})
export class RightPanelComponent implements OnInit {
  constructor(private ahs:AudioHistoryService,private as:AudioService,private router: Router,private route: ActivatedRoute) { }
  id:number;
  ngOnInit(): void {}

  audioHome(){
    this.id = this.as.suggestions[0].id;
    this.router.navigate(['/audio',this.id, 'home']);
    
   }
}
