export class Bookmark {
    public time: string;
    public description: string;
    
    constructor(time: string, description: string) {
      this.time = time;
      this.description = description;
    }
  }
  