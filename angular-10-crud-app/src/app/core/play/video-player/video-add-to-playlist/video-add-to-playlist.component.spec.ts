import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoAddToPlaylistComponent } from './video-add-to-playlist.component';

describe('VideoAddToPlaylistComponent', () => {
  let component: VideoAddToPlaylistComponent;
  let fixture: ComponentFixture<VideoAddToPlaylistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoAddToPlaylistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoAddToPlaylistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
