import { TestBed } from '@angular/core/testing';

import { VideoPlaylistForItemService } from './video-playlist-for-item.service';

describe('VideoPlaylistForItemService', () => {
  let service: VideoPlaylistForItemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VideoPlaylistForItemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
