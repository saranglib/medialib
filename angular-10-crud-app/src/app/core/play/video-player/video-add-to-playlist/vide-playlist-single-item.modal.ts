export class  VideoPlaylistForItem {
    private name:string;
    private list_uuid:string;
    private is_added:boolean;
    constructor(name: string,list_uuid: string, is_added:boolean) {
        this.name = name;
        this.list_uuid = list_uuid;
        this.is_added = is_added;
    }
}