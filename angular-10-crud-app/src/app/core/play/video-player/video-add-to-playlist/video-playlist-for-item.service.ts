import { VideoPlaylistForItem } from './vide-playlist-single-item.modal'
import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VideoPlaylistForItemService implements OnInit{

  
    playlistForItemchanged = new Subject<VideoPlaylistForItem[]>();
    playlistForItem:VideoPlaylistForItem[];
    
    host = environment.host;

    PlaylistForItemRequest(id:string) {
      this.http
        .get<VideoPlaylistForItem[]>(this.host+'/api/list/?item_uuid='+id+'?type=video',
        )
        .subscribe(              
          data => {                         
            this.setPlayListForItem(data);           
          }        
        );      
   }

    getPlaylistForId(){
      if(this.playlistForItem.length != 0){
        return this.playlistForItem.slice();
        // return this.playlistForItemchanged.next(this.playlistForItem.slice());
      }
    }

    setPlayListForItem(playlist_data: any[]) {       
      var tempPlaylistArray:VideoPlaylistForItem[]=[];
      playlist_data.forEach(function(playlistelement){   
        var obj:VideoPlaylistForItem = new VideoPlaylistForItem(playlistelement.name,playlistelement.list_uuid,playlistelement.is_added);
        tempPlaylistArray.push(obj)
      })

      this.playlistForItem = tempPlaylistArray;
      this.playlistForItemchanged.next(this.playlistForItem.slice());              
    }
    
    constructor(public http: HttpClient) { }
    
    ngOnInit(){

    }
  
}
