import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { VideoPlaylistService } from '../../../playlist/video-playlist/video-playlist.service';
import { VideoPlaylistForItemService } from './video-playlist-for-item.service';
import { VideoPlaylistForItem } from './vide-playlist-single-item.modal';
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-video-add-to-playlist',
  templateUrl: './video-add-to-playlist.component.html',
  styleUrls: ['./video-add-to-playlist.component.css'],
})
export class VideoAddToPlaylistComponent implements OnInit {
  id:string;
  playlists:VideoPlaylistForItem[];
  subscription:Subscription;


  constructor( public bsModalRef: BsModalRef,private videoPlaylistService:VideoPlaylistService,private videoPlaylistServiceForItem:VideoPlaylistForItemService,private toastr:ToastrService) { }

  ngOnInit(): void {  
   
    this.videoPlaylistServiceForItem.PlaylistForItemRequest(this.id)
    this.videoPlaylistServiceForItem.playlistForItemchanged.
    subscribe(
      (playlists: VideoPlaylistForItem[])=>{
        this.playlists = this.videoPlaylistServiceForItem.getPlaylistForId();
      }
    );
    this.playlists = this.videoPlaylistServiceForItem.getPlaylistForId(); 
       
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }    
    // const playlist_name = form.value.new_playlist_name;   
    this.videoPlaylistService.createPlaylist(this.id,form.value.new_playlist_name)
    // .subscribe(
    //   success => this.toastr.error('Added to '+ form.value.new_playlist_name),
    //   error => this.toastr.error('Failed to add into playlist')
    // );
    this.bsModalRef.hide()
    
  }

}
