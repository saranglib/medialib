import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { VgApiService } from '@videogular/ngx-videogular/core';
import { Video } from './video.model';

import { ActivatedRoute, Params, Router } from '@angular/router';
import { Bookmark } from "./bookmark.model";

import { VideoService } from './video.service';

interface VideoElement extends HTMLVideoElement {
  requestPictureInPicture(): any;
}

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css'],
  providers: [VgApiService],
})
export class VideoPlayerComponent implements OnInit,OnDestroy {

  // queuelist: Video[];
  subscription: Subscription;
  subscription2: Subscription;
  subCurrentItem: Subscription;
  bookmarks:Bookmark[];

  id: number;
  currentItem: Video;
  rateItem:number;

  // api: VgApiService;

  // onClickPlaylistItem(item: Video) {
  //   this.currentItem = item;
  // }

  constructor(
    private videoService: VideoService,
    private route: ActivatedRoute,
    private router: Router)
  {}

  ngOnInit(): void {

    let link:string = this.route.toString();

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];  
      this.subscription2 = this.videoService.videoChanged.subscribe(
        (video: Video) => {
          this.currentItem = this.videoService.getVideofromQueue(this.id);
        }
      );
      //old logic
      this.currentItem = this.videoService.getVideofromQueue(this.id);
      
      this.rateItem = this.videoService.getVideoRating(this.id);    
      this.subscription = this.videoService.bookmarkChanged.subscribe(
        (bookmarks: Bookmark[]) => {
          this.bookmarks = bookmarks;
        }
      );
      this.bookmarks = this.videoService.getBookmarks(this.currentItem.id);
    });

  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
    this.subscription2.unsubscribe();
  }
}
