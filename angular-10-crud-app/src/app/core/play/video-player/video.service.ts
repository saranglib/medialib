import { Injectable, OnInit } from '@angular/core';
import { pipe, Subject } from 'rxjs';

import { HttpClient,HttpHeaders } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { AuthService } from '../../../login/auth.service'

import { Video } from './video.model';
import { Bookmark } from './bookmark.model'
import { environment } from 'src/environments/environment';
import * as moment from 'moment';

@Injectable()
export class VideoService implements OnInit{
  videoChanged = new Subject<Video>();
  QueueChanged = new Subject<Video[]>();
  SuggestionChanged = new Subject<Video[]>();
  CurrentItemChanged = new Subject<Video>();
  bookmarkChanged = new Subject<Bookmark[]>();
  RatingChanged = new Subject<Number>();
  FavouriteChanged = new Subject<Boolean>();

  host = environment.host;
  tempVideo:Video;
  bookmarks:Array<Bookmark>=[];
 

  private suggestions: Video[] = [];
  private queuelist: Video[] = [];

  suggestionVideoCall() {
     this.http
      .get<Video[]>(this.host+'/api/item/?type=video',
      )
      .subscribe(              
        data => {                    
          this.setsuggestionlist(data);           
        }        
      );      
  }

  constructor(private http: HttpClient,private authService: AuthService) {    
    this.suggestionVideoCall();
    
  }

  ngOnInit(){
    this.suggestionVideoCall();    
  }
  


  setqueuelist(videos: any[]) {
    this.queuelist = videos;
    this.QueueChanged.next(this.queuelist.slice());
  }

  setsuggestionlist(videos: any[]) {

    var tempVideoArray:Video[]=[];
    videos.forEach(function(videoElement){
      let vposter;
      if(Object.keys(videoElement.src).length == 1){
        vposter=videoElement.src.video+'#t=5'
      }else{
        vposter=videoElement.src.image_thumbnail
      }
      
      var obj:Video = new Video(videoElement.id,videoElement.title,'http://localhost:8000/'+videoElement.src.video,'video/mp4','http://localhost:8000/'+vposter,videoElement.rating,videoElement.view,false,videoElement.duration,videoElement.rating);
      tempVideoArray.push(obj);
    })
    this.suggestions = tempVideoArray;
    this.SuggestionChanged.next(this.suggestions.slice());    
    
  }

  getVideoRating(id:number){
    this.http
      .get(this.host+'/api/rating/?item_uuid='+id,
      )
      .subscribe(              
        data => {                   
          return data[0].rating;                    
        }        
      ); 
    return 0;
  }

  getqueuelist() {
    return this.queuelist.slice();
  }
  
  getsuggestionslist() {         
    return this.suggestions.slice();    
    // return this.SuggestionChanged.next(this.suggestions.slice());    
  }

  getVideofromSuggestions(id: number) {
    return this.suggestions.find(el => el.id===id);
  }

  setSingleVideo(videoElement: any):Video {
    
    return new Video(videoElement.id,videoElement.title,'http://localhost/'+videoElement.src.video,'video/mp4','http://localhost/'+videoElement.src.image_thumbnail,videoElement.rating,videoElement.view,false,videoElement.duration,videoElement.rating);
    
  }
  
  getVideofromQueue(id: number) {    
    
    // if(this.queuelist.length==0){
    //         console.log(id);
    //         this.http
    //         .get(this.host+'/api/items/'+id+'/?type=video',
    //         )
    //         .subscribe(              
    //           (data:any) => {
    //             let vposter;
    //             if(Object.keys(data.src).length == 1){
    //               vposter=data.src.video+'#t=5'
    //             }else{
    //               vposter=data.src.image_thumbnail
    //             }
    //             var obj:Video = new Video(data.id,data.title,'http://localhost/'+data.src.video,'video/mp4','http://localhost/'+vposter,data.rating,data.view,false,data.duration,data.rating);
    //             this.queuelist.push(obj)           
    //             this.QueueChanged.next(this.queuelist.slice())
    //             this.videoChanged.next(obj);  
    //           }        
    //         );
    //         return this.queuelist.find(el => el.id===id); 
    //         // this.suggestionVideoCall()
    //       }
    

    //old logic
    if(!this.queuelist.find(el => el.id===id)){
      this.queuelist.push(this.suggestions.find(el => el.id===id));
      this.QueueChanged.next(this.queuelist.slice());  
    }

    return this.queuelist.find(el => el.id===id);
    
  }

  addToQueueVideo(video: Video) {
    const found = this.queuelist.some(el => el.id === video.id);
    if(!found) this.queuelist.push(video);
    this.QueueChanged.next(this.queuelist.slice());    
  }

  updateVideo(index: number, newVideo: Video) {
    this.queuelist[index] = newVideo;
    this.QueueChanged.next(this.queuelist.slice());
  }

  deleteFromQueuelist(index: number) {
    var tempIndex = this.queuelist.findIndex(x => x.id == index);
    this.queuelist.splice(tempIndex, 1);
    this.QueueChanged.next(this.queuelist.slice());
  }

  nextVideo(index:number){
      var tempIndex = this.queuelist.findIndex(x => x.id == index);
      tempIndex = tempIndex+1;
      if (tempIndex >= this.queuelist.length) {
        tempIndex=0;
      }  

      if(!(this.queuelist.length === 0) && !(this.queuelist.length === 1)){
        this.tempVideo = this.queuelist[tempIndex];
        return this.tempVideo.id;
      }
      return -1;
  }

  previousVideo(index:number){
    var tempIndex = this.queuelist.findIndex(x => x.id == index);
    tempIndex = tempIndex-1;
    if (tempIndex <= 0) {
      tempIndex=0;
    }
        
    if(!(this.queuelist.length === 0) && !(this.queuelist.length === 1)){
      this.tempVideo = this.queuelist[tempIndex];    
      return this.tempVideo.id;
    }
    return -1;
  }

  addBookmark(bookmarkTime:any,description:any,id:number){
    this.bookmarks.push(new Bookmark(bookmarkTime,description));
    this.bookmarkChanged.next(this.bookmarks.slice());

    let bodyData = {"bookmark_type":100,"name":description.toString(),"bookmark":bookmarkTime,"item_uuid":id}
    
    return this.http      
      .post(this.host+'/api/bookmark/',bodyData,{observe:'response'})
      .subscribe(
        data => {                      
        }
      ) 
  }

  getBookmarks(id:any) {

    this.http
      .get<Video[]>(this.host+'/api/bookmark/?item_uuid='+id,
      )
      .subscribe(              
        data => {          
          this.setBookmarklist(data);           
        }        
      );  

    return this.bookmarks.slice();
  }

  setBookmarklist(bms: any[]) {

    var tempVideoArray:Bookmark[]=[];
    bms.forEach(function(bookmarkElement){
      var obj:Bookmark = new Bookmark(bookmarkElement.time,bookmarkElement.description);
      tempVideoArray.push(obj);
    })
    this.bookmarks = tempVideoArray;
    this.bookmarkChanged.next(this.bookmarks.slice());        
  }

  FavoriteChange(id:number){   
    
    return this.http      
      .post(this.host+'/api/favourite/'+id+'/',{observe:'response'})
      .subscribe(
        data => {                    
        }
      )  
  }

  ratingChange(id:number,rate:number){   
    
    return this.http      
      .post(this.host+'/api/rating/',{rating: rate.toString(),item_uuid: id},{observe:'response'})
      .subscribe(
        data => {                      
        }
      )  
  }

  historyAdd(id:number,time:number){
    let visite_at = moment().format('YYYY-MM-DDTh:mm:ssZ');
    let visit_time = Math.ceil(time);
    let bodyData = {
      "history_type": 100,
      "visited_at": visite_at,
      "viewed_time": visit_time,
      "view_counted": true,
      "metrics": {
          "test": "test"
      },
      "item_uuid": id
      };

    return this.http      
      .post(this.host+'/api/history/',bodyData,{observe:'response'})
      .subscribe(
        data => {            
         
        }
      )  
  }
}
