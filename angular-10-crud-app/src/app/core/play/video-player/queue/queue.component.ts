import { Component, OnInit,Input } from '@angular/core';
import { Video } from '../video.model';
import { VideoService } from '../video.service';
import { Subscription } from 'rxjs';
import { faStar,faEye, faEllipsisV } from '@fortawesome/free-solid-svg-icons';
import { faStar as faStarR } from "@fortawesome/free-regular-svg-icons";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { VideoAddToPlaylistComponent } from "../video-add-to-playlist/video-add-to-playlist.component";


@Component({
  selector: 'app-queue',
  templateUrl: './queue.component.html',
  styleUrls: ['./queue.component.css'],
})
export class QueueComponent implements OnInit {
  @Input() currentItem: Video;
  isCollapsed = false; 
  

  bsModalRef: BsModalRef;
  id:number;

  faEye = faEye;
  faStarR = faStarR;
  faStar = faStar;
  faEllipsisV = faEllipsisV;


  videos: Video[];
  subscription: Subscription;

  collapseChange(val:boolean){
    this.isCollapsed = val;
    if(val===true){     
      document.getElementById('queueHeader').classList.remove('roundedcustomheader')
      document.getElementById('queueHeader').classList.add('roundedcustomheaderCollapse')
    }else{
      document.getElementById('queueHeader').classList.remove('roundedcustomheaderCollapse')
      document.getElementById('queueHeader').classList.add('roundedcustomheader')
    }
  }


  addToPlaylist(id:number){
    this.bsModalRef = this.modalService.show(VideoAddToPlaylistComponent);
    this.bsModalRef.content.id = id;
  }

  deleteFromQueue(id:number){
    this.videoService.deleteFromQueuelist(id);
  }

  constructor(private videoService: VideoService,private modalService: BsModalService) { }

  ngOnInit(): void {
    this.subscription = this.videoService.QueueChanged.subscribe(
      (videos: Video[]) => {
        this.videos = videos;
      }
    );
    this.videos = this.videoService.getqueuelist();
  }

}
