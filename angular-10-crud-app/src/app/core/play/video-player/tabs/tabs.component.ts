import { Component, OnInit, ViewEncapsulation,Input} from '@angular/core';

import { Bookmark } from "../bookmark.model";

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TabsComponent implements OnInit {
  @Input() bookmarks: Bookmark[];

  constructor() {}

  ngOnInit(): void {}
}
