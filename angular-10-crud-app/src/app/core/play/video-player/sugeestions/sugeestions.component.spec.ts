import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SugeestionsComponent } from './sugeestions.component';

describe('SugeestionsComponent', () => {
  let component: SugeestionsComponent;
  let fixture: ComponentFixture<SugeestionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SugeestionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SugeestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
