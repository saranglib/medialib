import { Component, OnInit,Input } from '@angular/core';
import { Video } from '../video.model';
import { VideoService } from '../video.service';
import { Subscription } from 'rxjs';
import { faStar,faEye,faEllipsisV } from '@fortawesome/free-solid-svg-icons';
import { faStar as faStarR } from "@fortawesome/free-regular-svg-icons";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { VideoAddToPlaylistComponent } from "../video-add-to-playlist/video-add-to-playlist.component";

@Component({
  selector: 'app-sugeestions',
  templateUrl: './sugeestions.component.html',
  styleUrls: ['./sugeestions.component.css']
})
export class SugeestionsComponent implements OnInit {

  @Input() currentItem: Video;
  isCollapsed = false; 
  
  bsModalRef: BsModalRef;
  id:number;

  faEye = faEye;
  faStarR = faStarR;
  faStar = faStar;
  faEllipsisV=faEllipsisV;


  videos: Video[];
  subscription: Subscription;

  collapseChange(val:boolean){
    this.isCollapsed = val;
    if(val===true){     
      document.getElementById('suggestionHeader').classList.remove('roundedcustomheader')
      document.getElementById('suggestionHeader').classList.add('roundedcustomheaderCollapse')
    }else{
      document.getElementById('suggestionHeader').classList.remove('roundedcustomheaderCollapse')
      document.getElementById('suggestionHeader').classList.add('roundedcustomheader')
    }
  }

  addToQueue(id:number){
    this.videoService.addToQueueVideo(this.videoService.getVideofromSuggestions(id));
  }

  addToPlaylist(id:number){
    this.bsModalRef = this.modalService.show(VideoAddToPlaylistComponent);
    this.bsModalRef.content.id = id;
  }
  constructor(private videoService: VideoService,private modalService: BsModalService) { }

  ngOnInit(): void {
    this.subscription = this.videoService.SuggestionChanged.subscribe(
      (videos: Video[]) => {
        this.videos = videos;
      }
    );
    this.videos = this.videoService.getsuggestionslist();
  }

}
