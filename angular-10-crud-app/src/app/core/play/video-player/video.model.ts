export class Video {
  public id: number;
  public title: string;
  public src: string;
  public type: string;
  public vposter: string;
  public rating: number;
  public user_rating: number;
  public view: number;
  public favorite: boolean;
  public duration: string;

  constructor(id:number, title: string, src: string, type: string, vposter: string,rating:number,view:number, favorite:boolean, duration: string,user_rating:number) {
    this.id = id;
    this.title = title;
    this.src = src;
    this.type = type;
    this.vposter = vposter;
    this.rating = rating;
    this.user_rating = user_rating;
    this.view = view;
    this.favorite = favorite;
    this.duration = duration;
  }
}
