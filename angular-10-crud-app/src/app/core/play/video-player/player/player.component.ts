import { Component, OnInit, Input, ViewChild, ElementRef,TemplateRef,ViewEncapsulation, OnDestroy  } from '@angular/core';
import { VgApiService } from '@videogular/ngx-videogular/core';
import { Video } from '../video.model';
import {
  faEye,
  faThumbsUp,
  faStar,
  faHeart,
  faList,
  faFastForward,
  faFastBackward,
  faRedo,
  faUndo,
  faBookmark,
  faShare,
} from '@fortawesome/free-solid-svg-icons';
import * as far from '@fortawesome/free-regular-svg-icons';

import { VideoAddToPlaylistComponent } from "../video-add-to-playlist/video-add-to-playlist.component";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { VideoService } from '../video.service';
import { Router,Params,  ActivatedRoute } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

interface VideoElement extends HTMLVideoElement {
  requestPictureInPicture(): any;
}

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css'],
  providers: [VgApiService],
  encapsulation:ViewEncapsulation.None
})
export class PlayerComponent implements OnInit, OnDestroy {
  @Input() currentItem: Video;
  @Input() rateItem: number;
  startTime;
  endTime;
  // @Input() id: string;
  modalRef: BsModalRef;
  bsModalRef: BsModalRef;
  id:number;

  isControlShown = true; 
  isBackShow=false;
  isForwardShow=false;
  protected touchtime = 0;


  faEye = faEye;
  faThumbsUp = faThumbsUp;
  faThumbsUpR = far.faThumbsUp;
  faStarR = far.faStar;
  faStar = faStar;
  faHeart = faHeart;
  faHeartR = far.faHeart;
  faList = faList;
  faFastForward = faFastForward;
  faFastBackward = faFastBackward;
  faRedo = faRedo;
  faUndo = faUndo;
  faBookmark = faBookmark;
  faShare = faShare;

  currentSpeed = 1;
  @ViewChild('media') videoElement: ElementRef;

  api: VgApiService;
  bookmarkTime:any;
  bookmarkDesc:any;

  onPlayerReady(api: VgApiService) {
    this.api = api;    
    this.api
      .getDefaultMedia()
      .subscriptions.loadedMetadata.subscribe(this.playVideo.bind(this));
    this.api.getDefaultMedia()
    .subscriptions.ended.subscribe(this.nextVideo.bind(this));
    this.api.getDefaultMedia().subscriptions.play.subscribe(
      () => {
        setTimeout(() => {
          this.isControlShown=false;
        }, 1000);
      }
    );
    this.api.getDefaultMedia().subscriptions.pause.subscribe(
      () => {        
        this.isControlShown=true;
      }
    );
    this.api.getDefaultMedia().subscriptions.seeked.subscribe(
      () => {        
        this.api.play();
      }
    );    
    // this.api.volume = 0; //This shold be deleted after development
  }

  handlespeed(newSpeed: number) {
    this.currentSpeed = newSpeed;
    this.api.playbackRate = newSpeed;
  }

  async pipr() {
    const video: VideoElement = this.videoElement.nativeElement;
    await video.requestPictureInPicture();
  }

  forward10sec(api: VgApiService) {
    if (this.api.currentTime + 10 >= this.api.duration) {
      this.api.seekTime(this.api.duration);
    } else {
      this.api.seekTime(this.api.currentTime + 10);
    }
    this.isForwardShow=true;
    setTimeout(() => {
      this.isForwardShow=false;
    }, 1000);
  }

  backward10sec(api: VgApiService) {
    if (this.api.currentTime - 10 <= 0) {
      this.api.seekTime(0);
    } else {
      this.api.seekTime(this.api.currentTime - 10);
    }
    this.isBackShow=true;
    setTimeout(() => {
      this.isBackShow=false;
    }, 1000);
  }

  forward10secdbltap(api: VgApiService) {
    if (this.touchtime == 0) {
        // set first click
        this.touchtime = new Date().getTime();
    } else {
        // compare first click to this click and see if they occurred within double click threshold
        if (((new Date().getTime()) - this.touchtime) < 800) {
            // double click occurred
            if (this.api.currentTime + 10 >= this.api.duration) {
              this.api.seekTime(this.api.duration);
            } else {
              this.api.seekTime(this.api.currentTime + 10);
            }
            this.isForwardShow=true;
            setTimeout(() => {
              this.isForwardShow=false;
            }, 1000);
            this.touchtime = 0;
        } else {
            // not a double click so set as a new first click
            this.touchtime = new Date().getTime();
        }
    }
  }

  backward10secdbltap(api: VgApiService) {
    if (this.api.currentTime - 10 <= 0) {
      this.api.seekTime(0);
    } else {
      this.api.seekTime(this.api.currentTime - 10);
    }
    this.isBackShow=true;
    setTimeout(() => {
      this.isBackShow=false;
    }, 1000);
  }

  previousVideo(){
    var tempIndex = this.videoService.previousVideo(this.id);
    if(tempIndex != -1){
      this.router.navigate(['../',tempIndex],{relativeTo:this.route});
    }
    
  }

  nextVideo(){
    var tempIndex = this.videoService.nextVideo(this.id);
    if(tempIndex != -1){
      this.router.navigate(['../',tempIndex],{relativeTo:this.route});
    }
  }


  playVideo() {
    this.api.play();
  }

  NumToTime(num) {
    // var num2: number = num.toFixed(0);
    var num2: number = Math.floor(num);
    var hours2: number = Math.floor(num2 / 3600);
    var hours: String = hours2.toString();
    if (hours.length < 2) hours = '0' + hours;
    num2 = num2 % 3600;
    var minutes2: number = Math.floor(num2 / 60);
    var minutes: String = minutes2.toString();
    if (minutes.length < 2) minutes = '0' + minutes;
    num2 = num2 % 60;
    var second: String = num2.toString();
    if (second.length < 2) second = '0' + second;

    return hours + ':' + minutes + ':' + second;
  }

  openModal(addBookmarkModal: TemplateRef<any>) {
    this.api.pause();
    if(document.fullscreenElement){
      document.exitFullscreen();
    }
    let currentTi: number = this.api.currentTime;
    this.bookmarkTime = this.NumToTime(currentTi);
    this.modalRef = this.modalService.show(addBookmarkModal);
  }

  addBookmark(){
    this.bookmarkDesc = (<HTMLInputElement>document.getElementById('bookmark_desc')).value;
 
    
    this.videoService.addBookmark(this.bookmarkTime,this.bookmarkDesc,this.currentItem.id);
    this.modalRef.hide();
    this.api.play();
  }

  addToPlaylist(id:number){
    this.bsModalRef = this.modalService.show(VideoAddToPlaylistComponent);
    this.bsModalRef.content.id = id;
  }

  favoriteChange(){ 
    if(this.currentItem.favorite === false){
      this.videoService.FavoriteChange(this.currentItem.id)   
      this.toastr.success('Added to favorite!');
      this.currentItem.favorite = !this.currentItem.favorite;
    }else{
      this.videoService.FavoriteChange(this.currentItem.id)   
      this.currentItem.favorite = !this.currentItem.favorite;
      this.toastr.warning('Removed from favorite!');
    }
  }


  // Rating method and variable
  max = 5;
  rate:number = 0;
  isReadonly = false;

  ratingChange(event:number){
    this.videoService.ratingChange(this.currentItem.id,this.rate);
    this.toastr.success('Thanks for your '+this.rate+'-star Rating');
  }
 
  userRating(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.isReadonly = true;    
      this.toastr.success('Thanks for your -star Rating');
    }
    this.toastr.success('Thanks for your '+this.rate+'-star Rating');
  }

  constructor(private modalService: BsModalService,private videoService: VideoService,private route: ActivatedRoute,private router: Router,private toastr: ToastrService) {
    // this.rate = this.videoService.getVideoRating(this.currentItem.id);
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
    });
    this.startTime = new Date();
    this.rate = this.rateItem;
   
  }

  ngOnDestroy(){
    this.endTime = new Date()
    let time = (this.endTime.getTime() - this.startTime.getTime())/1000;
    this.videoService.historyAdd(this.currentItem.id,time)
  }
}
