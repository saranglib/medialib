import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import {Location} from '@angular/common';
import { VgApiService, VgStates } from '@videogular/ngx-videogular/core';
import { Audio } from './audio.model';
import { Subscription } from 'rxjs';
import { AudioService } from './audio.service';
import {
  faRedo,
  faUndo,
  faBookmark,
  faAngleUp,
  faAngleDown,
  faStepBackward,
  faStepForward,
  faPlayCircle,
  faPauseCircle,
} from '@fortawesome/free-solid-svg-icons';
import * as far from '@fortawesome/free-regular-svg-icons';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-audio-player',
  templateUrl: './audio-player.component.html',
  styleUrls: ['./audio-player.component.scss'],
  providers: [VgApiService],
})
export class AudioPlayerComponent implements OnInit {
  faRedo = faRedo;
  faUndo = faUndo;
  faBookmark = faBookmark;
  faAngleUp = faAngleUp;
  faStepBackward = faStepBackward;
  faStepForward = faStepForward;
  faPlayCircle = faPlayCircle;
  faPauseCircle = faPauseCircle;
  faAngleDown = faAngleDown;
  displaybookmark = false;
  showarrow:boolean =true;
  autoplay:boolean=true;
  detailsviewurl;
  preload: string = 'auto';
  api: VgApiService;
  id: number;
  currentSpeed = 1;
  subscription: Subscription;
  currentIndex = 0;
  currentItem: Audio;
  queuelist: Audio[];
  time:string;
  displaydetails:boolean = false;
  startTime;
  endTime;
  constructor(
    private as: AudioService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location
  ) {
   
   }

  ngOnInit(): void {
     this.route.params.subscribe((params: Params) => {
     this.id = params['id'];
     this.startTime = new Date();
     this.currentItem = this.as.getAudio(this.id);
     this.as.currentItemChanged.next(this.id);
     let time = (this.endTime.getTime() - this.startTime.getTime())/1000;
     this.as.historyAdd(this.currentItem.id,time);
    });
      this.currentItem = this.as.getAudio(this.id);
      
  }
  onPlayerReady(api: VgApiService) {
    this.api = api;
    let time = (this.endTime.getTime() - this.startTime.getTime())/1000;
    this.as.historyAdd(this.currentItem.id,time);
  }

  handlespeed(newSpeed: number) {
    this.currentSpeed = newSpeed;
    this.api.playbackRate = newSpeed;
  }
  forward10sec(api: VgApiService) {
    if (this.api.currentTime + 10 >= this.api.duration) {
      this.api.seekTime(this.api.duration);
    } else {
      this.api.seekTime(this.api.currentTime + 10);
    }
  }
  backward10sec(api: VgApiService) {
    if (this.api.currentTime - 10 <= 0) {
      this.api.seekTime(0);
    } else {
      this.api.seekTime(this.api.currentTime - 10);
    }
  }
  nextAudio() {
    
    var tempIndex = this.as.nextAudio(this.id);
    var urlLength = this.router.url.length;
    var tempUrl = this.router.url.substring(44,urlLength).toString();
    if(tempIndex != -1){
      this.router.navigateByUrl('/audio/'+tempIndex+'/'+tempUrl);
    }
    else{
      var tem
      this.router.navigateByUrl('/audio/'+tempIndex+'/'+tempUrl);
    }
    this.endTime = new Date()
    let time = (this.endTime.getTime() - this.startTime.getTime())/1000;
    //this.as.historyAdd(this.currentItem.id,time);
  }
  previousAudio() {
    var tempIndex = this.as.previousVideo(this.id);
    var urlLength = this.router.url.length;
    var tempUrl = this.router.url.substring(44,urlLength).toString();
    if(tempIndex != -1){
      this.router.navigateByUrl('/audio/'+tempIndex+'/'+tempUrl);
    }
    else{
      this.router.navigateByUrl('/audio/'+tempIndex+'/'+tempUrl);
    }
    let time = (this.endTime.getTime() - this.startTime.getTime())/1000;
    //this.as.historyAdd(this.currentItem.id,time);
  }
  
  details(){
   if(this.router.url.includes('details-view')){
     this.showarrow = !this.showarrow;
     this.router.navigateByUrl('/audio/'+this.currentItem.id+'/home');
   }
   else{
     this.showarrow = !this.showarrow;
     this.router.navigateByUrl('/audio/'+this.currentItem.id+'/details-view/'+this.currentItem.id);
   }

  }
  bookmark(){
    this.displaybookmark = !this.displaybookmark;
    this.api.pause();
    let currentTi: number = this.api.currentTime;
    this.time = this.NumToTime(currentTi);

  }
NumToTime(num) {
  // var num2: number = num.toFixed(0);
  var num2: number = Math.floor(num);
  var hours2: number = Math.floor(num2 / 3600);
  var hours: String = hours2.toString();
  if (hours.length < 2) hours = '0' + hours;
  num2 = num2 % 3600;
  var minutes2: number = Math.floor(num2 / 60);
  var minutes: String = minutes2.toString();
  if (minutes.length < 2) minutes = '0' + minutes;
  num2 = num2 % 60;
  var second: String = num2.toString();
  if (second.length < 2) second = '0' + second;

  return hours + ':' + minutes + ':' + second;
}

}
