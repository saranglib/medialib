export class Audio {
    public id : number;
    public title: string;
    public src: string;
    public type: string;
    public duration:string;
    public artist: string;
    public album: string;
    public category : string;
    public rating: number;
    public view: number;
    public favourites : boolean;
    public trackInfo: any;

    constructor(id: number,title: string, src: string, type: string, duration:string, artist: string,album:string,category : string,rating:number,view:number,favorite: boolean,trackInfo: any) {
      this.title = title;
      this.src = src;
      this.type = type;
      this.duration = duration;
      this.artist = artist;
      this.album = album;
      this.category =category;
      this.rating = rating;
      this.view = view;
      this.trackInfo =trackInfo;
      this.favourites = favorite;
      this.id = id;
    }
  }
  