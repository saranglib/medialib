import { EventEmitter } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable, pipe, Subject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Audio } from './audio.model';
import { BookmarkAudio } from './bookmark/bookmark-audio.model'
import { environment } from 'src/environments/environment';
import * as moment from 'moment';
import { AudioFavorite } from '../../Favorite/audio-favorite/audio-favorite.model';
import { AudioBookmark } from './details-view/audio-tabs/audio-bookmark-details.model';
import { ToastrService } from 'ngx-toastr';
import { async } from '@rxweb/reactive-form-validators';
@Injectable()
export class AudioService {
  audioChanged = new Subject<Audio[]>();
  bookmarkChanged = new Subject<BookmarkAudio[]>();
  selectedRowId: number = 5;
  queueChanged = new EventEmitter<Audio[]>();
  bookmarks: Array<BookmarkAudio> = [];
  currentItemUpdated = new EventEmitter<Audio>();
  currentItem: Audio;
  currentItemChanged = new Subject<number>();
  host = environment.host;
  nginx_host = environment.nginx_host;
  tempAudio: Audio;
  currentIndex: number;
  SuggestionChanged = new Subject<Audio[]>();
  public suggestions: Audio[] = [];
  queuelist: Audio[] = [];
  queueList1: Audio[] = [];
   suggestionAudioCall() {
    this.http
      .get<Audio[]>(this.host + '/api/item/?type=audio',
    )
      .subscribe(
         data => {
           this.setsuggestionlist(data);
        }
      );
   
  }
  constructor(private http: HttpClient, private toastr: ToastrService) {
    this.suggestionAudioCall()
  }
   ngOnInit() {
     this.suggestionAudioCall();
  }
  setPlaylist(audios: Audio[]) {
    this.queuelist = audios;
    this.audioChanged.next(this.queuelist.slice());
  }

  setsuggestionlist(audios: any[]) {

    var tempAudioArray: Audio[] = [];
    if(audios.length == undefined){
      this.toastr.info("No SearchResult");
    }
    else{
      audios.forEach(function (audioElement) {
        var obj: Audio = new Audio(audioElement.id, audioElement.title, environment.host + "/" + audioElement.src, 'audio/mp3', audioElement.duration, audioElement.artist, audioElement.album, audioElement.category, audioElement.rating, audioElement.view, audioElement.favourites, audioElement.trackInfo);
        tempAudioArray.push(obj);
      })
      this.suggestions = tempAudioArray;
      this.SuggestionChanged.next(this.suggestions.slice());
    }
   
  }
  getNewmethod() {
    return this.suggestions.slice(0, 10);
  }
  getPlaylist() {

    return this.suggestions.slice(0, 5);
  }

  getAudio(index: number) {
    let suggestion = this.suggestions.find(a => a.id == index);
    if (typeof suggestion == 'undefined') {
      return this.suggestions[0];
    }
    else {
      return suggestion;
    }
  }
  addAudio(audio: Audio) {
    this.queuelist.push(audio);
    this.audioChanged.next(this.queuelist.slice());
  }

  updateAudio(index: number, newRecipe: Audio) {
    this.queuelist[index] = newRecipe;
    this.audioChanged.next(this.queuelist.slice());
  }
  nextAudio(index: number) {

    var tempIndex = this.queueList1.findIndex(x => x.id == index);
    if (tempIndex == -1) {
      tempIndex = this.suggestions.findIndex(x => x.id == index);
      tempIndex = tempIndex + 1;
      if (tempIndex >= this.suggestions.length) {
        tempIndex = 0;
      }
      this.tempAudio = this.suggestions[tempIndex];
      return this.tempAudio.id;
    }
    tempIndex = tempIndex + 1;
    if (tempIndex >= this.queueList1.length) {
      tempIndex = 0;
    }

    if (!(this.queueList1.length === 0) && !(this.queueList1.length === 1)) {
      this.tempAudio = this.queueList1[tempIndex];
      return this.tempAudio.id;
    }

  }

  previousVideo(index: number) {

    var tempIndex = this.queueList1.findIndex(x => x.id == index);
    if (tempIndex == -1) {
      tempIndex = this.suggestions.findIndex(x => x.id == index);
      tempIndex = tempIndex + 1;
      if (tempIndex <= 0) {
        tempIndex = 0;
      }
      this.tempAudio = this.suggestions[tempIndex];
      return this.tempAudio.id;
    }
    tempIndex = tempIndex + 1;
    if (tempIndex <= 0) {
      tempIndex = 0;
    }

    if (!(this.queueList1.length === 0) && !(this.queueList1.length === 1)) {
      this.tempAudio = this.queueList1[tempIndex];
      return this.tempAudio.id;
    }
  }
  deleteAudio(index: number) {
    this.queuelist.splice(index, 1);
    this.audioChanged.next(this.queuelist.slice());
  }

  getBookmarkAuaudio() {
    return this.bookmarks.slice();
  }
  addItemToQueue(Id: number) {
    var audio: Audio = this.suggestions.find(x => x.id == Id);
    var index = this.queueList1.findIndex(x => x.id == audio.id);
    if (index == -1) {
      this.queueList1.push(audio);
    }
    else {
      this.toastr.warning("Already Present in queue....");
    }
    this.queueChanged.emit(this.queueList1);
  }

  removeItemFromQueue(Id: number) {
    this.queueList1 = this.queueList1.filter(x => x.id !== Id);
    this.queueChanged.emit(this.queueList1);
  }
  getAudioRating(id: number) {
    this.http
      .get(this.host + '/api/rating/?item_uuid=' + id,
    )
      .subscribe(
        data => {
          return data[0].rating;
        }
      );
    return 0;
  }
  addBookmark(bookmarkTime: any, description: any, id: number) {
    let bodyData = { "bookmark_type": 300, "name": description.toString(), "bookmark": bookmarkTime, "item_uuid": id }
    const prom = this.http.post<any>(this.host + '/api/bookmark/', bodyData, { observe: 'response' }).toPromise();
    prom.then(res => {
      this.getBookmarks(id).subscribe(res => {
        this.bookmarks = res
        this.bookmarkChanged.next(this.bookmarks.slice());
      })
    });

  }
  getBookmarks(id: any): Observable<BookmarkAudio[]> {
    return this.http
      .get<BookmarkAudio[]>(this.host + '/api/bookmark/?type=audio&item_uuid=' + id,
    )
  }


  FavoriteChange(id: number): Observable<Audio> {
    return this.http
      .post<Audio>(this.host + '/api/favourite/' + id + '/', { observe: 'response' })

  }

  ratingChange(id: number, rate: number) {
    return this.http
      .post(this.host + '/api/rating/', { rating: rate.toString(), item_uuid: id }, { observe: 'response' })
      .subscribe(
        data => {
        }
      )
  }
  bookmarkDelete(id: string): Observable<any> {
    return this.http.delete(this.host + '/api/bookmark/' + id + '/', { observe: 'response' })
  }

  historyAdd(id: number, time: number) {
    let visite_at = moment().format('YYYY-MM-DDTh:mm:ssZ');
    let visit_time = Math.ceil(time);
    let bodyData = {
      "history_type": 300,
      "visited_at": visite_at,
      "viewed_time": visit_time,
      "view_counted": true,
      "metrics": {
        "test": "test"
      },
      "item_uuid": id
    };

    return this.http
      .post(this.host + '/api/history/', bodyData, { observe: 'response' })
      .subscribe(
        data => {
        }
      )
  }
}
