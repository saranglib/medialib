import { Component, Input, OnInit } from '@angular/core';
import { VgApiService } from '@videogular/ngx-videogular/core';
import { ToastrService } from 'ngx-toastr';
import { Audio } from '../audio.model';
import { AudioService } from '../audio.service';

@Component({
  selector: 'app-bookmark',
  templateUrl: './bookmark.component.html',
  styleUrls: ['./bookmark.component.css'],
  providers: [VgApiService],
})
export class BookmarkComponent implements OnInit {
  @Input() time;
  @Input() id;
  displaybookmark=true;

  constructor(private as: AudioService,private toastr:ToastrService) {}

  bookmarkDesc:any;
  api: VgApiService;
  ngOnInit(): void {
  }
  addBookmark(){
    this.bookmarkDesc = (<HTMLInputElement>document.getElementById('bookmark_desc')).value;
    this.as.addBookmark(this.time,this.bookmarkDesc,this.id);
    this.displaybookmark=false;
   this.toastr.success("bookmark Added Successfully.....");
  }
  onClick(){
    this.displaybookmark=false;
  }

}
