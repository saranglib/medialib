export class BookmarkAudio{
    public time: string;
    public description: string;
    public id: string;
  
    constructor(id:string,time: string, description: string) {
      this.time = time;
      this.id = id;
      this.description = description;
    }
  }