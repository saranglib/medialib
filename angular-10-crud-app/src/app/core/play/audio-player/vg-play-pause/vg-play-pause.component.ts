import { Component, ElementRef, HostListener, OnInit, Input, ViewEncapsulation, OnDestroy } from '@angular/core';
import {VgApiService, VgStates } from '@videogular/ngx-videogular/core';
import { Subscription } from 'rxjs';
import {
  faPlayCircle,
  faPauseCircle,
} from '@fortawesome/free-solid-svg-icons';
@Component({
    selector: 'app-vg-play-pause',
    encapsulation: ViewEncapsulation.None,
    template: `
        <div class="icon mt-n4 "
        *ngIf="getState() === 'playing'"
             tabindex="0"
             role="button"
             [attr.aria-label]="getState() === 'paused'?'play':'pause'"
             [attr.aria-valuetext]="ariaValue">
             <fa-icon [icon]="faPauseCircle" class="icon text-white fa-4x "></fa-icon>
        </div>
        <div class="icon mt-n4"
            *ngIf="getState() === 'paused' || getState() === 'ended'"
             tabindex="0"
             role="button"
             [attr.aria-label]="getState() === 'paused'?'play':'pause'"
             [attr.aria-valuetext]="ariaValue">
             <fa-icon [icon]="faPlayCircle" class="icon text-white fa-4x"></fa-icon>
           
        </div>`,
    styles: [ `
        app-vg-play-pause {
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            display: flex;
            justify-content: center;
            
            cursor: pointer;
         
        }
        app-vg-play-pause .icon {
            pointer-events: none;
        }
    ` ]
})
export class VgPlayPause implements OnInit, OnDestroy {
    @Input() vgFor: string;

    elem: HTMLElement;
    target: any;

    subscriptions: Subscription[] = [];
  faPlayCircle = faPlayCircle;
  faPauseCircle = faPauseCircle;
    ariaValue = VgStates.VG_PAUSED;

    constructor(ref: ElementRef, public API: VgApiService) {
        this.elem = ref.nativeElement;
    }

    ngOnInit() {
        if (this.API.isPlayerReady) {
            this.onPlayerReady();
        }
        else {
            this.subscriptions.push(this.API.playerReadyEvent.subscribe(() => this.onPlayerReady()));
        }
    }

    onPlayerReady() {
        this.target = this.API.getMediaById(this.vgFor);
    }

    @HostListener('click')
    onClick() {
        this.playPause();
    }

    @HostListener('keydown', ['$event'])
    onKeyDown(event: KeyboardEvent) {
        // On press Enter (13) or Space (32)
        if (event.keyCode === 13 || event.keyCode === 32) {
            event.preventDefault();
            this.playPause();
        }
    }

    playPause() {
        let state = this.getState();

        switch (state) {
            case VgStates.VG_PLAYING:
                this.target.pause();
                break;

            case VgStates.VG_PAUSED:
                
            case VgStates.VG_ENDED:
                this.target.play();
                break;
        }
    }

    getState() {
        this.ariaValue = this.target ? this.target.state : VgStates.VG_PAUSED;
        return this.ariaValue;
    }

    ngOnDestroy() {
        this.subscriptions.forEach(s => s.unsubscribe());
    }
}
