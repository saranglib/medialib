import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VgPlayPause } from './vg-play-pause.component';

describe('VgPlayPauseComponent', () => {
  let component: VgPlayPause;
  let fixture: ComponentFixture<VgPlayPause>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VgPlayPause ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VgPlayPause);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
