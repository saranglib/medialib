import { AudioBookmark } from "./audio-bookmark-details.model";
export class AudioBookmarkService {
    private bookmarks : AudioBookmark[] = [
        
            {
            id:1, bookmark:"05:20",description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. "
            },
            {
            id:2, bookmark:"06:08",description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            },
            {
            id:3, bookmark:"04;05",description:`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vehicula ex non consequat mollis. Nulla mattis sed nulla id dapibus. Nam tortor felis, faucibus vel interdum sit amet, dapibus quis urna. Duis lobortis ullamcorper felis et scelerisque. Nunc sed libero sit amet enim consectetur vehicula et vitae quam. Sed ornare hendrerit risus, a commodo diam placerat ut. Mauris ut justo semper, tincidunt neque et, faucibus est. Pellentesque bibendum diam at pretium placerat. Vivamus commodo ante ac est interdum, ut elementum lorem rhoncus. Etiam maximus aliquam nunc ac pharetra. Nunc viverra erat ac ex vestibulum, in rutrum mi finibus. Duis nec tincidunt lectus.

            Vivamus et facilisis ex. Vestibulum lectus nibh, sagittis vitae metus consectetur, fringilla pellentesque magna. Ut aliquet libero quis eros euismod ullamcorper. Nam vulputate nec ipsum vel pulvinar. Proin sed pulvinar nunc, eu consectetur elit. Vivamus eu orci leo. Quisque eu magna nisi. Suspendisse malesuada dapibus placerat. Mauris ac vulputate eros. Mauris fermentum ante id neque congue iaculis. Morbi vel lectus ut erat euismod scelerisque vel in ante. Donec et luctus tortor.
            
            Nulla iaculis cursus sollicitudin. Phasellus mollis nibh arcu, sit amet varius sapien imperdiet at. Nulla semper, metus interdum egestas cursus, est lectus ultricies libero, eu vestibulum turpis ligula sed mauris. Mauris imperdiet elit ut enim maximus interdum. Morbi quam purus, sollicitudin at ex vestibulum, gravida pharetra lacus. Aliquam felis ante, vestibulum vitae libero vel, tristique dignissim odio. Nullam eu mi luctus, varius lacus nec, faucibus dui. Donec posuere sagittis neque, vitae sodales orci ornare sed. Cras a urna id turpis placerat bibendum quis et lectus.`
            },
            {
            id:4, bookmark:"03:20",description:"Mauris vel ex risus. Praesent sodales lacus purus, ac vulputate dolor ullamcorper sit amet. Pellentesque facilisis sollicitudin quam eu scelerisque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec aliquam at lorem in tempor. Nunc maximus lectus nec vulputate tincidunt. Donec fermentum eros nisl. Fusce ut tellus et velit iaculis ullamcorper. Sed imperdiet hendrerit elit vel sodales. Vivamus a dolor tincidunt metus ornare interdum."
            },
        
       
            {
            id:5, bookmark:"05:20",description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. "
            },
            {
            id:6, bookmark:"04:20",description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            },
            {
            id:7, bookmark:"06:20",description:`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vehicula ex non consequat mollis. Nulla mattis sed nulla id dapibus. Nam tortor felis, faucibus vel interdum sit amet, dapibus quis urna. Duis lobortis ullamcorper felis et scelerisque. Nunc sed libero sit amet enim consectetur vehicula et vitae quam. Sed ornare hendrerit risus, a commodo diam placerat ut. Mauris ut justo semper, tincidunt neque et, faucibus est. Pellentesque bibendum diam at pretium placerat. Vivamus commodo ante ac est interdum, ut elementum lorem rhoncus. Etiam maximus aliquam nunc ac pharetra. Nunc viverra erat ac ex vestibulum, in rutrum mi finibus. Duis nec tincidunt lectus.

            Vivamus et facilisis ex. Vestibulum lectus nibh, sagittis vitae metus consectetur, fringilla pellentesque magna. Ut aliquet libero quis eros euismod ullamcorper. Nam vulputate nec ipsum vel pulvinar. Proin sed pulvinar nunc, eu consectetur elit. Vivamus eu orci leo. Quisque eu magna nisi. Suspendisse malesuada dapibus placerat. Mauris ac vulputate eros. Mauris fermentum ante id neque congue iaculis. Morbi vel lectus ut erat euismod scelerisque vel in ante. Donec et luctus tortor.
            
            Nulla iaculis cursus sollicitudin. Phasellus mollis nibh arcu, sit amet varius sapien imperdiet at. Nulla semper, metus interdum egestas cursus, est lectus ultricies libero, eu vestibulum turpis ligula sed mauris. Mauris imperdiet elit ut enim maximus interdum. Morbi quam purus, sollicitudin at ex vestibulum, gravida pharetra lacus. Aliquam felis ante, vestibulum vitae libero vel, tristique dignissim odio. Nullam eu mi luctus, varius lacus nec, faucibus dui. Donec posuere sagittis neque, vitae sodales orci ornare sed. Cras a urna id turpis placerat bibendum quis et lectus.`
            },
        
       
            {
            id:8, bookmark:"08:20",description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. "
            },
            {
            id:9, bookmark:"03:20",description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            },
            {
            id:10, bookmark:"06:20",description:"Mauris vel ex risus. Praesent sodales lacus purus, ac vulputate dolor ullamcorper sit amet. Pellentesque facilisis sollicitudin quam eu scelerisque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec aliquam at lorem in tempor. Nunc maximus lectus nec vulputate tincidunt. Donec fermentum eros nisl. Fusce ut tellus et velit iaculis ullamcorper. Sed imperdiet hendrerit elit vel sodales. Vivamus a dolor tincidunt metus ornare interdum."
            },
        
    ];

    getBookmarks(){
        return this.bookmarks.slice();
    }

    getBookmarkById(id:number){
        return this.bookmarks[this.bookmarks.findIndex(x=>x.id == id)];
    }
}