import { Component, OnInit,ViewEncapsulation,OnDestroy  } from '@angular/core';
import { Subscription } from 'rxjs';
import { BookmarkAudio } from '../../bookmark/bookmark-audio.model';
import { AudioService } from '../../audio.service';
import { Audio } from '../../audio.model';

@Component({
  selector: 'app-audio-tabs',
  templateUrl: './audio-tabs.component.html',
  styleUrls: ['./audio-tabs.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AudioTabsComponent implements OnInit {
  subscription: Subscription;

  constructor(  private as : AudioService) {   }

  ngOnInit() : void {
 
  }


ngOnDestroy(){
}

}
