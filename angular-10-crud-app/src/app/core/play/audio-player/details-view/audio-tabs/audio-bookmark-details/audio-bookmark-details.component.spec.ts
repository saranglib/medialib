import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioBookmarkDetailsComponent } from './audio-bookmark-details.component';

describe('AudioBookmarkDetailsComponent', () => {
  let component: AudioBookmarkDetailsComponent;
  let fixture: ComponentFixture<AudioBookmarkDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudioBookmarkDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioBookmarkDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
