
import { Component, NgZone, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { Toast, ToastrService } from 'ngx-toastr';
import { DeleteRendererComponent } from 'src/app/cell-renderer/delete-renderer/delete-renderer.component';
import { BookmarkService } from 'src/app/core/bookmark/common-bookmark/bookmark.service';
import { AudioService } from '../../../audio.service';
import { BookmarkAudio } from '../../../bookmark/bookmark-audio.model';
import { AudioBookmark} from '../audio-bookmark-details.model';
import { AudioBookmarkService } from '../audio-bookmark-details.service';
@Component({
  selector: 'app-audio-bookmark-details',
  templateUrl: './audio-bookmark-details.component.html',
  styleUrls: ['./audio-bookmark-details.component.css']
})
export class AudioBookmarkDetailsComponent implements OnInit {

  faTrashAlt = faTrashAlt;
  bookmarks:BookmarkAudio[];
  public gridApi;
  public gridColumnApi;
  public rowData;
  public columnDefs;
  public rowSelection;
  public frameworkComponents;
  public rowMultiSelectWithClick;
  public selectedList:  AudioBookmark;
  public total: number;
  public rowSelected: boolean;
  id:string; 
  subscription: any;
  constructor(private as: AudioService,private toastr:ToastrService,private bs:BookmarkService, private route: ActivatedRoute, private router: Router, private ngZone: NgZone) {
   
    this.rowSelected = false;
   

    this.columnDefs  = [
      {
        flex: 1,
        field: 'bookmark',
        headerName:"Time",
        checkboxSelection: true,
        headerCheckboxSelection: true,
        maxWidth :120,
        sortable: true,
        filter: true,
        cellClass: "grid-cell-centered",
      },
      {
        flex: 1,
        field: 'description',
        headerName:"Notes",
        resizable: true,
        wrapText: true,
        autoHeight: true,
        editable: true,
      },
      {
        flex: 1,
        field: 'Edit',
        maxWidth: 100,
        cellClass: ["grid-cell-centered","icon-size"],
        cellRenderer: 'editRenderer',
      },
      {
        flex: 1,
        field: 'Delete',
        maxWidth: 100,
        cellClass: ["grid-cell-centered","icon-size"],
        cellRenderer: 'deleteRenderer',
      }
    ];
    this.frameworkComponents = {
      deleteRenderer:DeleteRendererComponent,

    };
    this.rowSelection= 'multiple';
    this.rowMultiSelectWithClick= true;
   }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];  
      this.as.getBookmarks(this.id).subscribe(res => {
        this.bookmarks = res;
        this.rowData = this.bookmarks;
      }); 
      this.as.bookmarkChanged.subscribe(
        (bookmarks: BookmarkAudio[]) => {
          this.bookmarks = bookmarks;
          this.rowData = this.bookmarks;
        }
      );
    
    });

    this.rowData = this.bookmarks;
  }

  onFirstDataRendered(params)
  {
    params.api.sizeColumnsToFit();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  onSelectionChanged(params){
    var selectedRows = this.gridApi.getSelectedRows();
    this.rowSelected = selectedRows.length > 0 ? true: false;
  }

  onCellCilcked(params){
    if(params.column.colDef.field === "Edit"){
      this.ngZone.run(()=>
      this.router.navigate(['edit'],{relativeTo: this.route, queryParams:{'id':params.data.id}})
      );
    }
    else if(params.column.colDef.field === "Delete"){
    const prom = this.as.bookmarkDelete(params.data.id).toPromise();

     prom.then(res => {

     
      this.as.getBookmarks(this.id).subscribe(res => {
                this.bookmarks = res;
                this.rowData =res;
                
              });
            });
    this.toastr.warning("bookmark deleted Successfully");
    }
  }
}