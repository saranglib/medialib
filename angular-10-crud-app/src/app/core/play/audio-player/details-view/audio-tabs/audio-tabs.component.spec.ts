import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioTabsComponent } from './audio-tabs.component';

describe('AudioTabsComponent', () => {
  let component: AudioTabsComponent;
  let fixture: ComponentFixture<AudioTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudioTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
