import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioBookmarkEditComponent } from './audio-bookmark-edit.component';

describe('AudioBookmarkEditComponent', () => {
  let component: AudioBookmarkEditComponent;
  let fixture: ComponentFixture<AudioBookmarkEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudioBookmarkEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioBookmarkEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
