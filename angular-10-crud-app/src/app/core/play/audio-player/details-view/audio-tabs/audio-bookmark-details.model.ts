export class AudioBookmark {
    public id: number;
    public bookmark: string;
    public description: string;

    constructor(id: number, bookmark: string, description:string){
        this.id = id;
        this.bookmark = bookmark;
        this.description = description;
    }
}

