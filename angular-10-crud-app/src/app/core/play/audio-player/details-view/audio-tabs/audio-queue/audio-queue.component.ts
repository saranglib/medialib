import { Component, OnInit } from '@angular/core';
import { Audio } from '../../../audio.model';
import { AudioService } from '../../../audio.service';
import { DeleteRendererComponent } from 'src/app/cell-renderer/delete-renderer/delete-renderer.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-audio-queue',
  templateUrl: './audio-queue.component.html',
  styleUrls: ['./audio-queue.component.scss']
})
export class AudioQueueComponent implements OnInit {

  public gridApi;
  public rowData;
  public columnDefs;
  public rowSelection;
  public rowClassRules;
  public gridColumnApi;
  public selectedRowId;
  public frameworkComponents;

  constructor(protected as : AudioService,private router: Router,private route: ActivatedRoute,) { 

    this.rowData = this.getRowData();
    this.columnDefs = [
      {
        field: 'Id',
        headerName: "",
        cellRenderer: function(params)
          { 
            if(params.data.Id == as.selectedRowId)
              return '<img src="./assets/loading-final.gif" width="20px" height="20px" style="margin-left:2rem">'
            return '<div class="icon icon-play" style="font-size:12px; margin-left: 1rem; padding-left:1rem"></div>';
          },
        rowDrag: true,
        resizable: false, 
        width: 70,
        cellStyle:{ textAlign: "center",'font-size': '24px','letter-spacing': '1.2px'}
      },
      { 
        field: 'title' , 
        sortable: true, 
        filter: true,  
        resizable: false, 
        cellStyle:{'font-size': '24px','letter-spacing': '1.2px'}
        // checkboxSelection: true, 
        // headerCheckboxSelection: true,
      },
      { 
        field: 'duration', 
        sortable: true, 
        resizable: false, 
        width:100,
        cellClass: "grid-cell-centered",
        cellStyle:{'font-size': '24px','letter-spacing': '1.2px'}
      },
      {
        field: 'Delete',
        // cellRenderer:function(params){ return '<div class="icon icon-add-to-play-list" style="font-size: 18px"></div>' },
        width:50,
        cellClass: "grid-cell-centered",
        cellRenderer: 'deleteRenderer',
        cellStyle:{'font-size': '24px','letter-spacing': '1.2px'}
      }
  ];
    // this.rowClassRules = {
    //   'playing': function (params) {
    //     var data = params.data.id;
    //     return data == as.selectedRowId;
    //   },
   // };
    // for multiselect
    // this.rowSelection =  'multiple';

    //for play
    // this.rowSelection = 'single';
    this.frameworkComponents = {
      deleteRenderer: DeleteRendererComponent,
    }

    this.as.queueChanged.subscribe(
      (queueList1 : Audio[])=>{
        this.rowData = queueList1;
      this.gridApi.redrawRows();
      }
    )
}

  ngOnInit(): void {
    this.rowData = this.getRowData();
  }

  onGridReady(params) {
    this.gridApi = params.api;
  }

  onFirstDataRendered(params)
  {
    params.api.sizeColumnsToFit();
  }

  getRowData() {
    var rowData = this.as.queueList1;
    return rowData;
  }

  oncellClicked(params){
    if(params.column.colDef.field === "Delete"){
      this.as.removeItemFromQueue(params.data.id);
    }
	else  if (params.column.colDef.field === "title") {
    var selectedRows = this.gridApi.getSelectedRows();
    this.as.selectedRowId = selectedRows.length === 1 ? selectedRows[0].Id : '';
    this.gridApi.redrawRows(params);
    this.router.navigate(['/audio', params.data.id, 'details-view',params.data.id]);
  }
  }
}

