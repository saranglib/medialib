import { Component, Input, OnInit } from '@angular/core';
import { faStar, faEye, faHeart, faList } from '@fortawesome/free-solid-svg-icons';
import { faStar as faStarR, faHeart as faHeartR } from "@fortawesome/free-regular-svg-icons";
import { Audio } from '../audio.model';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AudioService } from '../audio.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AudioAddToPlaylistComponent } from '../audio-add-to-playlist/audio-add-to-playlist.component';
import { ToastrService } from 'ngx-toastr';
import { BookmarkAudio } from '../bookmark/bookmark-audio.model';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-details-view',
  templateUrl: './details-view.component.html',
  styleUrls: ['./details-view.component.css']
})
export class DetailsViewComponent implements OnInit {
  faEye = faEye;
  faHeart = faHeart;
  faStarR = faStarR;
  faStar = faStar;
  faList = faList;
  faHeartR = faHeartR;
  subscription: Subscription;
  bookmarks: BookmarkAudio[];
  rateItem: number;
  constructor(
    private as: AudioService,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private modalService: BsModalService
  ) { }
  modalRef: BsModalRef;
  bsModalRef: BsModalRef;
  id: number;
  currentItem: Audio;
  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.id = (params['id']);
      this.currentItem = this.as.getAudio(this.id)
      this.rate = this.currentItem.rating;
      this.subscription = this.as.bookmarkChanged.subscribe(
        (bookmarks: BookmarkAudio[]) => {
          this.bookmarks = bookmarks;
        }
      );
    });

  }
  favoriteChange() {
    this.as.FavoriteChange(this.currentItem.id).subscribe(res => {
      if (res.favourites.valueOf() == false) {
        this.currentItem.favourites = res.favourites;
        this.toastr.warning('Removed from favorite!');
      }
      else {
        this.currentItem.favourites = res.favourites;
        this.toastr.success('Added to favorite');
      }
    });



  }
  addToPlaylist(addToplaylist: Audio) {
    const initialState = {
      addAudio: addToplaylist,
    };
    this.bsModalRef = this.modalService.show(AudioAddToPlaylistComponent, { initialState });
  }
  max = 5;
  rate: number = 0;
  isReadonly = false;

  ratingChange(event: number) {
    this.as.ratingChange(this.currentItem.id, this.rate);
    this.toastr.success('Thanks for your ' + this.rate + '-star Rating');
  }
  userRating(event: KeyboardEvent) {
    //   if (event.key === 'Enter') {
    //     this.isReadonly = true;    
    //     this.toastr.success('Thanks for your -star Rating');
    //   }
    //   this.toastr.success('Thanks for your '+this.rate+'-star Rating');
  }
}
