import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { AudioPlaylist } from 'src/app/core/playlist/audio-playlist/audio-playlist.model';
import { AudioPlaylistService } from 'src/app/core/playlist/audio-playlist/audio-playlist.service';
import { environment } from 'src/environments/environment';
import { Audio } from '../audio.model';
import { AudioPlaylistItem } from './audio-add-to-playlist.model';

@Injectable({
  providedIn: 'root'
})
export class AudioAddtoPlaylistService {

  
  playlistForItemchanged = new Subject<AudioPlaylistItem[]>();
  playlistForItem:AudioPlaylistItem[];
  
  host = environment.host;

  PlaylistForItemRequest(id:string) {
    this.http
      .get<AudioPlaylistItem[]>(this.host+'/api/list/?item_uuid='+id+'?type=audio',
      )
      .subscribe(              
        data => {                         
          this.setPlayListForItem(data);           
        }        
      );      
 }

  getPlaylistForId(){
    if(this.playlistForItem.length != 0){
      return this.playlistForItem.slice();
      // return this.playlistForItemchanged.next(this.playlistForItem.slice());
    }
  }
  getPlaylistdata(playlistId:string,audio:Audio){
  this.http.get<any>(this.host+'/api/list/'+playlistId)
  .subscribe(              
    data => {        
                  
      data.items.push(audio.id);  
     this.http.put<any>(this.host+'/api/list/'+playlistId+'/',data).subscribe(
       res=>{
        this.aps.audioPlaylistRequest();
       }
     );
    }        
  );      

  }
  setPlayListForItem(playlist_data: any[]) {       
    var tempPlaylistArray:AudioPlaylistItem[]=[];
    playlist_data.forEach(function(playlistelement){   
      var obj:AudioPlaylistItem = new AudioPlaylistItem(playlistelement.name,playlistelement.list_uuid,playlistelement.is_added);
      tempPlaylistArray.push(obj)
    })

    this.playlistForItem = tempPlaylistArray;
    this.playlistForItemchanged.next(this.playlistForItem.slice());              
  }
  
  constructor(public http: HttpClient,private aps : AudioPlaylistService) { }
  
  ngOnInit(){

  }
}
