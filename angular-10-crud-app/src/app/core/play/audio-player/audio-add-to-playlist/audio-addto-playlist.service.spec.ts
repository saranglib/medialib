import { TestBed } from '@angular/core/testing';

import { AudioAddtoPlaylistService } from './audio-addto-playlist.service';

describe('AudioAddtoPlaylistService', () => {
  let service: AudioAddtoPlaylistService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AudioAddtoPlaylistService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
