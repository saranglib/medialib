import { Context, GroupInstanceIdCreator } from '@ag-grid-enterprise/all-modules';
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { BsModalRef,ModalModule ,BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { AudioPlaylistService } from 'src/app/core/playlist/audio-playlist/audio-playlist.service';
import { AudioService } from '../audio.service';
import {Audio} from '../audio.model';
import { FormControl, NgForm } from '@angular/forms';
import { AudioAddtoPlaylistService } from './audio-addto-playlist.service';
import { ToastrService } from 'ngx-toastr';
import { AudioPlaylist } from 'src/app/core/playlist/audio-playlist/audio-playlist.model';
import { AudioPlaylistItem } from './audio-add-to-playlist.model';
@Component({
  selector: 'app-audio-add-to-playlist',
  templateUrl: './audio-add-to-playlist.component.html',
  styleUrls: ['./audio-add-to-playlist.component.css'],
  encapsulation : ViewEncapsulation.None
})
export class AudioAddToPlaylistComponent implements OnInit {

  id:string;
  addAudio:Audio;
  playlists:any;
  subscription:Subscription;
  audioPlaylist:Audio;
  constructor(public bsModalRef: BsModalRef, private audioPlaylistService:AudioPlaylistService,private audioAddtoPlaylist:AudioAddtoPlaylistService,private toastr:ToastrService) { }
  new_playlist_name = new FormControl('');
  ngOnInit(): void {  
    this.audioAddtoPlaylist.PlaylistForItemRequest(this.id);
    this.audioAddtoPlaylist.playlistForItemchanged.
    subscribe(
      (playlists: AudioPlaylistItem[])=>{
        this.playlists = this.audioAddtoPlaylist.getPlaylistForId();
      }
    );
    this.playlists = this.audioAddtoPlaylist.getPlaylistForId(); 
       
  }
addToPlaylist(playlistid:number){

  this.audioAddtoPlaylist.getPlaylistdata(this.playlists[playlistid].list_uuid,this.addAudio);
  this.toastr.success("Added Successfully");
}
  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }    
    this.audioPlaylistService.createPlaylist(this.id,form.value.new_playlist_name);
    
    this.bsModalRef.hide();
    this.toastr.success("Playlist Created Successfully");
  }
}
