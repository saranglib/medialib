import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioAddToPlaylistComponent } from './audio-add-to-playlist.component';

describe('AudioAddToPlaylistComponent', () => {
  let component: AudioAddToPlaylistComponent;
  let fixture: ComponentFixture<AudioAddToPlaylistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudioAddToPlaylistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioAddToPlaylistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
