import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { VideoHistory } from "./video-history.model";
import { Subject } from 'rxjs';
import { Video } from '../../play/video-player/video.model'


@Injectable()
export class VideoHistoryService implements OnInit{
    // private suggestions: Video[] = [];
    private history:VideoHistory[] = [];
    HistoryChanged = new Subject<VideoHistory[]>();
    host = environment.host;

    HistoryRequest() {
        this.http
         .get<VideoHistory[]>(this.host+'/api/history/?type=video',
         )
         .subscribe(              
           data => {                         
             this.setHistorylist(data);           
           }        
         );      
     }
   
     constructor(public http: HttpClient) {    
       this.HistoryRequest();       
     }

     setHistorylist(videos: any[]) {
        var tempVideoArray:VideoHistory[]=[];
        videos.forEach(function(videoElement){  
                          
            let vposter;
            if(videoElement.src.image_thumbnail){
                vposter = videoElement.src.image_thumbnail
            }else{
                vposter = videoElement.src.video + '#t=5'
            }

            
            var date_test = new Date(Date.parse(videoElement.date));
            
            // id: number, date: Date, title: string, vposter: string, rating: number, view: number,  duration: string , time: Date
          var obj:VideoHistory = new VideoHistory(videoElement.id,date_test,videoElement.title,'http://localhost/'+vposter,videoElement.rating,videoElement.view,videoElement.duration,date_test);                    
          tempVideoArray.push(obj);
        })
        
        this.history = tempVideoArray;        
        this.HistoryChanged.next(this.history.slice());    
        
      }

    

    getHistory(){
      // this.HistoryRequest();
        return this.history.slice();
    }

    ngOnInit(){
        this.HistoryRequest();
    }
}