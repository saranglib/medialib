import { Component, OnDestroy, OnInit } from '@angular/core';
import { GroupDateRendererComponent } from 'src/app/cell-renderer/group-date-renderer/group-date-renderer.component';
import { InfoRendererComponent } from 'src/app/cell-renderer/info-renderer/info-renderer.component';
import { LastViewedRendererComponent } from 'src/app/cell-renderer/last-viewed-renderer/last-viewed-renderer.component';
import { VideoThumbRendererComponent } from 'src/app/cell-renderer/video-thumb-renderer/video-thumb-renderer.component';
import { VideoTitleRendererComponent } from 'src/app/cell-renderer/video-title-renderer/video-title-renderer.component';
import { VideoHistoryService } from './video-history.service';
import { RowGroupingModule} from '@ag-grid-enterprise/all-modules/';
import { Module } from '@ag-grid-enterprise/all-modules';
import { Subscription } from 'rxjs';
import { VideoHistory } from './video-history.model';

@Component({
  selector: 'app-video-history',
  templateUrl: './video-history.component.html',
  styleUrls: ['./video-history.component.css'],
  providers: [VideoHistoryService],
})
export class VideoHistoryComponent implements OnInit,OnDestroy {

  public gridApi;
  public gridColumnApi;
  public rowData;
  public columnDefs;
  public frameworkComponents;
  public rowSelection;
  public rowMultiSelectWithClick;
  public autoGroupColumnDef;
  public groupRowRendererParams;
  public defaultGroupSortComparator;
  public modules: Module[] = [
    RowGroupingModule,
  ];
  subscription: Subscription;

  constructor(private vhs: VideoHistoryService) { 
    
    this.columnDefs = [
      {
        field:'date',
        headerName:'Date',
        rowGroup: true,
        hide:true,
      },
      {
        field: 'vposter',
        headerName:'Id',
        cellRenderer: 'agGroupCellRenderer',
        autoHeight: true,
        maxWidth :300,
        checkboxSelection: true, 
        headerCheckboxSelection: true,
        cellRendererParams: {
          innerRenderer: 'videoThumbRenderer', 
        }
      },
      {
        field:'title',
        headerName:'Title',
        resizable: true, 
        cellRenderer: 'agGroupCellRenderer',
        cellRendererParams: {
          innerRenderer: 'titleRenderer', 
        }
      },
      {
        field:'time',
        headerName:'Last viewed',
        maxWidth: 150,
        cellClass: ["grid-cell-centered","icon-size"],
        cellRenderer: 'agGroupCellRenderer',
        cellRendererParams: {
          innerRenderer: 'lastViewedRenderer', 
        }
      },
      {
        field: 'view',
        headerName:'Info.',
        maxWidth: 100,
        cellClass: ["grid-cell-centered","icon-size"],
        cellRenderer: 'agGroupCellRenderer',
        cellRendererParams: {
          innerRenderer: 'infoRenderer', 
        }
      }
    ];

    this.frameworkComponents = {
      videoThumbRenderer: VideoThumbRendererComponent,
      titleRenderer: VideoTitleRendererComponent,
      infoRenderer: InfoRendererComponent,
      lastViewedRenderer: LastViewedRendererComponent,
      dateRenderer: GroupDateRendererComponent,
    }

    this.groupRowRendererParams= {
      innerRenderer: 'dateRenderer',
      checkbox : true,
      suppressCount: true,
    };

    this.rowSelection = 'multiple';
    this.rowMultiSelectWithClick =  true;
    this.autoGroupColumnDef = { 
      minWidth: 200,
    };
    
  }

  onFirstDataRendered(params)
  {
    params.api.sizeColumnsToFit();
  }
  
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  ngOnInit(): void {
    this.subscription = this.vhs.HistoryChanged.subscribe(
      (videos: VideoHistory[]) => {
        this.rowData = this.vhs.getHistory();
      }
      );
    this.rowData = this.vhs.getHistory();
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
  
}