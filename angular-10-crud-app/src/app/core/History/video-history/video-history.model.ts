export class VideoHistory {
    public id: number;
    public date: Date;
    public title: string;
    public vposter: string;
    public rating: number;
    public view: number;
    public duration: string;
    public time: Date;

    constructor( id: number, date: Date, title: string, vposter: string, rating: number, view: number,  duration: string , time: Date){
        this.id = id;
        this.date = date;
        this.title = title;
        this.vposter = vposter;
        this.rating = rating;
        this.view = view;
        this.duration = duration;
        this.time = time; 
    }
}