import { Component, OnInit } from '@angular/core';
import { GroupDateRendererComponent } from 'src/app/cell-renderer/group-date-renderer/group-date-renderer.component';
import { InfoRendererComponent } from 'src/app/cell-renderer/info-renderer/info-renderer.component';
import { LastViewedRendererComponent } from 'src/app/cell-renderer/last-viewed-renderer/last-viewed-renderer.component';
import { AudioHistoryService } from './audio-history.service';
import { RowGroupingModule} from '@ag-grid-enterprise/all-modules/';
import { Module } from '@ag-grid-enterprise/all-modules';
import { Subscription } from 'rxjs';
import { AudioHistory } from './audio-history.model';


@Component({
  selector: 'app-audio-history',
  templateUrl: './audio-history.component.html',
  styleUrls: ['./audio-history.component.css']
})
export class AudioHistoryComponent implements OnInit {
  public gridApi;
  public gridColumnApi;
  public rowData;
  public columnDefs;
  public frameworkComponents;
  public rowSelection;
  public rowMultiSelectWithClick;
  public autoGroupColumnDef;
  public groupRowRendererParams;
  public defaultGroupSortComparator;
  public modules: Module[] = [
    RowGroupingModule,
  ];

  constructor(private vhs: AudioHistoryService) { 
    this.rowData = this.vhs.getHistory();
    this.columnDefs = [
      {
        field:'date',
        headerName:'Date',
        rowGroup: true,
        hide:true,
      },
      
      {
        field:'title',
        headerName:'Title',
        resizable: true, 
        cellRenderer: 'agGroupCellRenderer',
        Width :200,
        checkboxSelection: true, 
        headerCheckboxSelection: true,
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field:'time',
        headerName:'Last viewed',
        maxWidth: 150,
        cellClass: ["grid-cell-centered","icon-size"],
        cellRenderer: 'agGroupCellRenderer',
        cellRendererParams: {
          innerRenderer: 'lastViewedRenderer', 
        }
      },
      {
        field: 'view',
        headerName:'Info.',
        maxWidth: 100,
        cellClass: ["grid-cell-centered","icon-size"],
        cellRenderer: 'agGroupCellRenderer',
        cellRendererParams: {
          innerRenderer: 'infoRenderer', 
        }
      }
    ];

    this.frameworkComponents = {
      infoRenderer: InfoRendererComponent,
      lastViewedRenderer: LastViewedRendererComponent,
      dateRenderer: GroupDateRendererComponent,
    }

    this.groupRowRendererParams= {
      innerRenderer: 'dateRenderer',
      checkbox : true,
      suppressCount: true,
    };

    this.rowSelection = 'multiple';
    this.rowMultiSelectWithClick =  true;
    this.autoGroupColumnDef = { 
      minWidth: 200,
    };
    
  }

  onFirstDataRendered(params)
  {
    params.api.sizeColumnsToFit();
  }
  
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }
  subscription: Subscription;
  ngOnInit(): void {
    this.subscription = this.vhs.HistoryChanged.subscribe(
      (audios: AudioHistory[]) => {
        this.rowData = this.vhs.getHistory();
      }
      );
    this.rowData = this.vhs.getHistory();
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
  
}
