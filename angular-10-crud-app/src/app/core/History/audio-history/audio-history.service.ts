import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { environment } from "src/environments/environment";
import { AudioHistory } from "./audio-history.model";
@Injectable()
export class AudioHistoryService{

    HistoryChanged = new Subject<AudioHistory[]>();
    host = environment.host;
    constructor(public http: HttpClient) {    
        this.audioHistoryRequest();       
      }
      ngOnInit(){
        this.audioHistoryRequest();    
      }
    history:AudioHistory[] = [];
  
    audioHistoryRequest() {
        this.http
         .get<AudioHistory[]>(this.host+'/api/history/?type=audio',
         )
         .subscribe(              
           data => {                         
             this.setHistorylist(data);           
           }        
         );      
     }
    setHistorylist(audios: any[]) {
        var tempAudioArray:AudioHistory[]=[];
        audios.forEach(function(audioElement){

          var date_test = new Date(Date.parse(audioElement.date));
          
          var obj:AudioHistory = new AudioHistory(audioElement.id,date_test,audioElement.title,audioElement.rating,audioElement.view,audioElement.duration,date_test);                    
          tempAudioArray.push(obj);
        })
        
        this.history = tempAudioArray;        
        this.HistoryChanged.next(this.history.slice());    
        
      }
    getHistory(){
        return this.history.slice();
    }
}