export class AudioHistory {
    public id: number;
    public date: Date;
    public title: string;
    public rating: number;
    public view: number;
    public duration: string;
    public time: Date;

    constructor( id: number, date: Date, title: string,  rating: number, view: number,  duration: string , time: Date){
        this.id = id;
        this.date = date;
        this.title = title;
        this.rating = rating;
        this.view = view;
        this.duration = duration;
        this.time = time; 
    }
}