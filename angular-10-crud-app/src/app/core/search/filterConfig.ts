import { AdminService } from "src/app/admin/admin-service";


export const audio_filters = [
  
  {
    key: 'artist',
    label: 'Artist',
    placeHolder: 'Enter artist name',          
    order: 1,
    controlType: 'textbox',
    multipleSelection: true,      
    suggestions: ['Sadhu Madhurvadandas','Jaideep Swadiya','Sadhu Madhurkirtandas','Sadhu Madhurkirtandas','Sadhu Shobhitswarupdas','Sadhu Somyakirtandas' ,'Amey Daate'  ]
  },  
  {
    key: 'event',
    label: 'Event',
    placeHolder: 'Enter event',
    order: 2,
    controlType: 'textbox', 
    multipleSelection: true,     
    suggestions: ['PSM100','shibir','janm jayanti','shatabdi','fuldol','Prayer'],
  },
  {
      key: 'start_date',
      label: 'Start Date',
      placeHolder: 'mm/dd/yyyy',          
      order: 4,
      controlType: 'date',
      type:'date'
    },
    {
      key: 'end_date',
      label: 'End Date',
      placeHolder: 'mm/dd/yyyy',          
      order: 5,
      controlType: 'date',
      type:'date'
    },
    {
      key: 'subject',
      label: 'Subject',
      placeHolder: 'Enter subject',          
      order: 3,
      controlType: 'textbox',  
      multipleSelection: true,      
      suggestions: ['swagat','diksha','bal','prayer','sanskar','manglacharan'],
    },
    
];


export const video_filters = [
  
  {
    key: 'location',
    label: 'Location',
    placeHolder: 'Enter location',          
    order: 1,
    controlType: 'textbox',
    multipleSelection: false,      
    suggestions: ['Ahmedabad','surat','sarangpur','sankari','mumbai','rajkot'],
  },  
  {
    key: 'event',
    label: 'Event',
    placeHolder: 'Enter event',
    order: 2,
    controlType: 'textbox', 
    multipleSelection: true,     
    suggestions: ['PSM100','shibir','sabha','shatabdi','fuldol','Prayer'],
  },
  {
      key: 'start_date',
      label: 'Start Date',
      placeHolder: 'mm/dd/yyyy',          
      order: 4,
      controlType: 'date',
      type:'date'
    },
    {
      key: 'end_date',
      label: 'End Date',
      placeHolder: 'mm/dd/yyyy',          
      order: 5,
      controlType: 'date',
      type:'date'
    },
    {
      key: 'subject',
      label: 'Subject',
      placeHolder: 'Enter subject',          
      order: 3,
      controlType: 'textbox',  
      multipleSelection: true,      
      suggestions: ['Mahima','Aagna','Upasna','seva','sanskar','vyasan-mukti'],
    },
    
];

