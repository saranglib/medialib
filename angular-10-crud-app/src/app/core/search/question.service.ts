import { Injectable } from '@angular/core';
import { video_filters, audio_filters } from "./filterConfig";
import { Observable, of, Subject } from 'rxjs';
import { Audio } from '../play/audio-player/audio.model'
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';
import { Video } from '../play/video-player/video.model';

@Injectable()
export class QuestionService {
  audioFilterChanged = new Subject<Audio[]>();
  videoFilterChanged = new Subject<Video[]>();
  audioResult: Audio[];
  videoResult: Video[];
  host = environment.host;
  constructor(private http: HttpClient, private toastr: ToastrService) {

  }
  getVideoQuestions() {
    const questions: any[] = video_filters;
    return of(questions.sort((a, b) => a.order - b.order));
  }

  getAudioQuestions() {
    const questions: any[] = audio_filters;
    return of(questions.sort((a, b) => a.order - b.order));
  }
  getAudiobyFilter(artist: string[], event: string[], start_date: Date, end_date: Date, subject: string[]) {
    var artistString = artist.join(', ');

    this.http
      .get<Audio[]>(this.host + '/api/item/?type=audio&artist=' + artist
      )
      .subscribe(
        data => {
          this.setsuggestionAudiolist(data);
        }
      );
  }
  getVideobyFilter(artist: string[], event: string[], start_date: Date, end_date: Date, subject: string[], location: string[]) {
    if (artist != null) {
      var artistString = artist.join(', ');
    }

    this.http
      .get<Audio[]>(this.host + '/api/item/?type=video&artist=' + artistString
      )
      .subscribe(
        data => {
          this.setsuggestionVideolist(data);
        }
      );
  }
  getAudiobySearch(searchText: string) {
    this.http
      .get<Audio[]>(this.host + '/api/item/?type=audio&query=' + searchText
      )
      .subscribe(
        data => {

          this.setsuggestionAudiolist(data);
        }
      );

  }

  getVideobySearch(searchText: string) {
    this.http
      .get<Audio[]>(this.host + '/api/item/?type=video&query=' + searchText
      )
      .subscribe(
        data => {

          this.setsuggestionVideolist(data);
        }
      );

  }
  setsuggestionVideolist(videos: any[]) {
    var tempVideoArray: Video[] = [];
    if (videos.length == undefined) {
      this.toastr.info("No SearchResult");
    }
    else {
      videos.forEach(function (videoElement) {
        let vposter;
        if (Object.keys(videoElement.src).length == 1) {
          vposter = videoElement.src.video + '#t=5'
        } else {
          vposter = videoElement.src.image_thumbnail
        }

        var obj: Video = new Video(videoElement.id, videoElement.title, environment.host + videoElement.src.video, 'video/mp4', 'http://localhost:8000/' + vposter, videoElement.rating, videoElement.view, false, videoElement.duration, videoElement.rating);
        tempVideoArray.push(obj);
      })
      this.videoResult = tempVideoArray;
      this.videoFilterChanged.next(this.videoResult.slice());

    }
  }

  setsuggestionAudiolist(audios: any[]) {
    var tempAudioArray: Audio[] = [];
    if (audios.length == undefined) {
      this.toastr.info("No SearchResult");
      this.audioResult = [];
      this.audioFilterChanged.next(this.audioResult.slice());
    }
    else {
      audios.forEach(function (audioElement) {
        var obj: Audio = new Audio(audioElement.id, audioElement.title, environment.host + audioElement.src, 'audio/mp3', audioElement.duration, audioElement.artist, audioElement.album, audioElement.category, audioElement.rating, audioElement.view, audioElement.favourites, audioElement.trackInfo);
        tempAudioArray.push(obj);
      })
      this.audioResult = tempAudioArray;
      this.audioFilterChanged.next(this.audioResult.slice());
    }
  }
}
