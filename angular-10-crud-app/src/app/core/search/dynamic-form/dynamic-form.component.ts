import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { QuestionControlService } from '../question-control.service';
import { QuestionService } from '../question.service';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  providers: [ QuestionControlService ]
})
export class DynamicFormComponent implements OnInit {

  @Input() questions: any[] = [];
  form: FormGroup;
  payLoad;
  urlString:String = this.router.url;
  filters_array:any[] ;
  artist:string[];
  event:string[];
  subject:string[];
  start_date:Date;
  end_date:Date;
  location:string[];

  constructor(private qcs: QuestionControlService,private qs:QuestionService,private router: Router, private route: ActivatedRoute) {
    
  }
  
  ngOnInit() {
    this.filters_array = this.questions;
    this.form = this.qcs.toFormGroup(this.filters_array.sort((a, b) => a.order - b.order));    
  }

  onSubmit() {
    this.payLoad = JSON.stringify(this.form.getRawValue());
    if(this.form.controls["start_date"].value != undefined)
    {
      this.start_date = this.form.controls["start_date"].value;
    }
    if(this.form.controls["end_date"].value != undefined)
    {
      this.end_date = this.form.controls["end_date"].value;
    }
    if(this.form.controls["subject"].value)
    {
      this.subject = this.form.controls["subject"].value;
    }
     if(this.form.controls["event"].value)
     {
      this.event = this.form.controls["event"].value;
     }
   
    
     if (this.urlString.includes("/audio"))
     {
        if (this.form.controls["artist"].value)
        {
          this.artist = this.form.controls["artist"].value;
        }
        this.qs.getAudiobyFilter(this.artist, this.event, this.start_date, this.end_date, this.subject);
      }
     if(this.urlString.includes("/video"))
     {
        if(this.form.controls["location"].value)
        {
          this.location = this.form.controls["location"].value;
        }
        this.qs.getVideobyFilter(this.artist,this.event,this.start_date,this.end_date,this.subject,this.location);
     }
    
  }
}
