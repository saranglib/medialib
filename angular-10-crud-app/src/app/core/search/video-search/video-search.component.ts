import { Component, OnInit } from '@angular/core';
import { faArrowUp, faCalendarDay, faStar, faEye, faFilter, faTh, faThList,faSearch } from '@fortawesome/free-solid-svg-icons';


import { QuestionService } from '../question.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-video-search',
  templateUrl: './video-search.component.html',
  styleUrls: ['./video-search.component.css'],
  providers: [QuestionService]
})
export class VideoSearchComponent implements OnInit {

  faArrowUp = faArrowUp;
  faCalendarDay = faCalendarDay;
  faStar = faStar;
  faEye = faEye;
  faFilter = faFilter;
  faTh = faTh;
  faThList = faThList;
  faSearch = faSearch;
  questions$: Observable<any[]>;

  public thumbview: boolean = true;
  public filterOpened: boolean = false;
  constructor(private service: QuestionService) {
    this.questions$ = service.getVideoQuestions();
  }

  ngOnInit(): void {
  }

  onViewChange() {
    this.thumbview = !this.thumbview;
  }

  onToggleFilter() {
    this.filterOpened = !this.filterOpened;
  }

  search(searchText: string) {
    this.service.getVideobySearch(searchText);
  }

}
