import { Component, OnDestroy, OnInit } from '@angular/core';
import { FavoriteRendererComponent } from 'src/app/cell-renderer/favorite-renderer/favorite-renderer.component';
import { InfoRendererComponent } from 'src/app/cell-renderer/info-renderer/info-renderer.component';
import { VideoThumbRendererComponent } from 'src/app/cell-renderer/video-thumb-renderer/video-thumb-renderer.component';
import { VideoTitleRendererComponent } from 'src/app/cell-renderer/video-title-renderer/video-title-renderer.component';
import { VideoService } from 'src/app/core/play/video-player/video.service';
import { Subscription } from 'rxjs';
import { Video } from 'src/app/core/play/video-player/video.model';
import { QuestionService } from '../../question.service';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {

  public gridApi;
  public gridColumnApi;
  public rowData;
  public columnDefs;
  public frameworkComponents;
  public domLayout;
  subscription: Subscription;
  videos:Video[];

  constructor(private vs: VideoService,private qs:QuestionService) { 
    this.rowData = this.vs.getsuggestionslist();
    this.columnDefs = [
      {
        field: 'Id',
        headerName:"",
        cellRenderer: 'videoThumbRenderer',
        autoHeight: true,
        maxWidth :300,
      },
      {
        field:'title',
        headerName:'Title',
        sortable: true,
        filter: true,
        resizable: true, 
        cellRenderer: 'titleRenderer'
      },
      {
        field:'favorite',
        sortable: true,
        maxWidth: 150,
        cellClass: ["grid-cell-centered","icon-size"],
        cellRenderer: 'favoriteRenderer'
      },
      {
        field: 'Queue',
        maxWidth: 100,
        cellClass: ["grid-cell-centered","icon-size"],
        cellRenderer: function(params){
          return '<div class="icon icon-queue"></div>';
        }
      },
      {
        field: 'playlist',
        maxWidth: 100,
        cellClass: ["grid-cell-centered"],
        cellRenderer: function(params){
          return '<div class="icon icon-add-to-play-list" style="font-size:30px"></div>';
        }
      },
      {
        field: 'info',
        maxWidth: 100,
        cellClass: ["grid-cell-centered","icon-size"],
        cellRenderer: 'infoRenderer'
      }
    ];

    this.frameworkComponents = {
      videoThumbRenderer : VideoThumbRendererComponent,
      titleRenderer : VideoTitleRendererComponent,
      infoRenderer :  InfoRendererComponent,
      favoriteRenderer : FavoriteRendererComponent
    };

    this.domLayout = "autoHeight";
  }

  onFirstDataRendered(params)
  {
    params.api.sizeColumnsToFit();
  }
  
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  ngOnInit(): void {
    this.qs.videoFilterChanged.subscribe(
      (videos: Video[]) => {
        this.videos = videos;
        this.rowData = this.videos;
        console.log(this.rowData);
      }
    );
  }

}
