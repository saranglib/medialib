import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoThumbCardComponent } from './video-thumb-card.component';

describe('VideoThumbCardComponent', () => {
  let component: VideoThumbCardComponent;
  let fixture: ComponentFixture<VideoThumbCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoThumbCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoThumbCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
