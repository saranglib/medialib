import { Component, OnInit, Input } from '@angular/core';
import { faStar,faEye,faHeart,faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { Video } from 'src/app/core/play/video-player/video.model';

@Component({
  selector: 'app-video-thumb-card',
  templateUrl: './video-thumb-card.component.html',
  styleUrls: ['./video-thumb-card.component.css']
})
export class VideoThumbCardComponent implements OnInit {
  @Input() video: Video;
  public isImage:boolean = true;
  faStar = faStar;
  faEye =  faEye;
  faHeart = faHeart;
  faInfo = faInfoCircle;

  constructor() {
  }

  ngOnInit(): void {
    if(this.video.vposter.includes('#')){
      this.isImage = false;
    }
  }

}
