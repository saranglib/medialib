import { Component, OnDestroy, OnInit } from '@angular/core';
import { Video } from 'src/app/core/play/video-player/video.model';
import { VideoService } from 'src/app/core/play/video-player/video.service';
import { Subscription } from 'rxjs';
import { QuestionService } from '../../question.service';

@Component({
  selector: 'app-video-thumb',
  templateUrl: './video-thumb.component.html',
  styleUrls: ['./video-thumb.component.css']
})
export class VideoThumbComponent implements OnInit {
  subscription: Subscription;
  videos: Video[];
  constructor(private vs:VideoService,private qs:QuestionService) {
    this.videos = this.vs.getsuggestionslist();
   }

  ngOnInit(): void {
    this.qs.videoFilterChanged.subscribe(
      (videos: Video[]) => {
        this.videos = videos; 
      }
    );
  }
 
}
