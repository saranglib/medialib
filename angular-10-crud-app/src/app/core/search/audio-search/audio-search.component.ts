import { Component, OnInit } from '@angular/core';
import { faArrowUp, faCalendarDay, faStar, faEye, faFilter, faTh, faThList,faSearch } from '@fortawesome/free-solid-svg-icons';
import { QuestionService } from '../question.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-audio-search',
  templateUrl: './audio-search.component.html',
  styleUrls: ['./audio-search.component.css'],
  providers:  [QuestionService]
})
export class AudioSearchComponent implements OnInit {
  faArrowUp = faArrowUp;
  faCalendarDay = faCalendarDay;
  faStar = faStar;
  faEye = faEye;
  faFilter = faFilter;
  faTh = faTh;
  faThList = faThList;
  faSearch = faSearch;
  questions$: Observable<any[]>;

  public filterOpened: boolean = false;
  constructor(private service: QuestionService) {
    this.questions$ = service.getAudioQuestions();    
  }

  ngOnInit(): void {
  }

  onToggleFilter(){
    this.filterOpened = !this.filterOpened;
  }

   search(searchText:string){
       
       this.service.getAudiobySearch(searchText);
    }
}
