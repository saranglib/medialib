import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService,BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { FavoriteRendererComponent } from 'src/app/cell-renderer/favorite-renderer/favorite-renderer.component';
import { InfoRendererComponent } from 'src/app/cell-renderer/info-renderer/info-renderer.component';
import { RatingRendererComponent } from 'src/app/cell-renderer/rating-renderer/rating-renderer.component';
import { AudioAddToPlaylistComponent } from 'src/app/core/play/audio-player/audio-add-to-playlist/audio-add-to-playlist.component';
import { AudioService } from 'src/app/core/play/audio-player/audio.service';
import {Audio} from 'src/app/core/play/audio-player/audio.model'
import { QuestionService } from '../../question.service';
import { AudioFavoriteService } from 'src/app/core/Favorite/audio-favorite/audio-favorite.service';

@Component({
  selector: 'app-audio-list',
  templateUrl: './audio-list.component.html',
  styleUrls: ['./audio-list.component.css']
})
export class AudioListComponent implements OnInit {
  buttonname = "View All";
  showMyContainer: boolean = false;
  buttonflag: boolean = true;
  audios: Audio[];
  subscription: Subscription;
  currentItem: Audio;
  modalRef: BsModalRef;
  bsModalRef: BsModalRef;
  id: number;
 
  public gridApi;
  public rowData;
  public columnDefs;
  public rowSelection;
  public rowClassRules;
  public gridColumnApi;
  public selectedRowId;
  public frameworkComponents;
  public tooltipShowDelay;
  public gridOptions;
  constructor(
    protected as: AudioService,
    private fs:AudioFavoriteService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private qs:QuestionService,
    private modalService: BsModalService) {
    this.rowData = as.suggestions;
    this.selectedRowId = as.selectedRowId;

    this.columnDefs = [
      {
        field: 'id',
        headerName: "",
        cellRenderer: function (params) {
          if (params.data.id == as.selectedRowId)
            return '<img src="./assets/loading-final.gif" width="20px" height="20px" style="margin-left:1rem">'
          return '<div class="icon icon-play" style="font-size:18px; margin-left: 0.5rem; padding-left:0.5rem"></div>';
        },
        width: 10,
        cellStyle: { textAlign: "center", 'font-size': '24px', 'letter-spacing': '1.2px' },

      },
      {
        field: 'title',
        sortable: true,
        filter: true,
        resizable: true,
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'artist',
        headerName: "Artist/Vakta",
        sortable: true,
        filter: true,
        resizable: true,
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'duration',
        sortable: true,
        resizable: false,
        width: 100,
        cellClass: "grid-cell-centered",
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'rating',
        sortable: true,
        resizable: false,
        width: 80,
        cellRenderer: 'ratingRenderer',
        cellClass: "grid-cell-centered",
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'favorite',
        cellRenderer: 'favoriteRenderer',
        width: 50,
        cellClass: "grid-cell-centered",
        sortable: true,
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'Queue',
        cellRenderer: function (params) { return '<div class="icon icon-queue"></div>' },
        width: 50,
        cellClass: "grid-cell-centered",
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'Playlist',
        cellRenderer: function (params) { return '<div class="icon icon-add-to-play-list"  style="font-size: 30px"></div>' },
        width: 50,
        cellClass: "grid-cell-centered",
      },
      {
        field: 'Info.',
        cellRenderer: 'infoRenderer',
        width: 50,
        cellClass: "grid-cell-centered",
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      }
    ];
    this.rowClassRules = {
      'playing': function (params) {
        var data = params.data.Id;
        return data == as.selectedRowId;
      },
    };
    this.rowSelection = 'single';
    this.frameworkComponents = {
      infoRenderer: InfoRendererComponent,
      favoriteRenderer: FavoriteRendererComponent,
      ratingRenderer: RatingRendererComponent,
    }
  }


  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

  }

  oncellClicked(params) {
    if (params.column.colDef.field === "favorite") {
      params.data.favorite = !params.data.favorite;
      const prom =  this.as.FavoriteChange(params.data.id).toPromise();
      prom.then(res => {
       this.fs.audioFavoritesRequest().subscribe(res => {
         this.gridApi.redrawRows();
       });
     }) ;
      if (params.data.favorite === true) {
        this.toastr.success('Added to favorite!');
      } else {
        this.toastr.warning('Removed from favorite!');
      }
    }
    else if (params.column.colDef.field === "Queue") {

      this.as.addItemToQueue(params.data.id);
      this.toastr.success(params.data.title + ' added to queue');
    }
    else if (params.column.colDef.field === "Playlist") {
      this.bsModalRef = this.modalService.show(AudioAddToPlaylistComponent);
      this.bsModalRef.content.id = params.data.id;

    }
    else if (params.column.colDef.field === "id" || params.column.colDef.field === "title") {
      var selectedRows = this.gridApi.getSelectedRows();
      this.as.selectedRowId = selectedRows.length === 1 ? selectedRows[0].Id : '';
      this.gridApi.redrawRows(params);
      this.router.navigate(['/audio', params.data.id, 'search']);
    }
    else if (params.column.colDef.field === "Info") {
      document.querySelector('#info').innerHTML === '<span popover="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."></span>'
    }

  }
  ngOnInit(): void {
    debugger
     this.qs.audioFilterChanged.subscribe(
      (audios: Audio[]) => {
        this.audios = audios; 
        this.rowData = this.audios;
        console.log(this.rowData);
      }
    );
  
  }



}
