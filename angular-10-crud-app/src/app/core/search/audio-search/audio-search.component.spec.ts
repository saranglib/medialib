import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioSearchComponent } from './audio-search.component';

describe('AudioSearchComponent', () => {
  let component: AudioSearchComponent;
  let fixture: ComponentFixture<AudioSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudioSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
