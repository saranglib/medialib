import { Component, OnInit } from '@angular/core';
import { faPencilAlt, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { AudioPlaylist } from './audio-playlist.model';
import { AudioPlaylistService } from './audio-playlist.service';
@Component({
  selector: 'app-audio-playlist',
  templateUrl: './audio-playlist.component.html',
  styleUrls: ['./audio-playlist.component.css']
})
export class  AudioPlaylistComponent implements OnInit {

  faPencilAlt = faPencilAlt;
  faTrashAlt = faTrashAlt;                             
  public playlists:  AudioPlaylist[];
  
  constructor(private cps:  AudioPlaylistService) {
    this.playlists = cps.getAllPlaylist();
   }


  ngOnInit(): void {
  }

}
