import { Component, Input, OnInit } from '@angular/core';
import { AudioPlaylist } from '../audio-playlist.model';
import { faPencilAlt, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { AudioPlaylistService } from '../audio-playlist.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AudioPlaylistItem } from 'src/app/core/play/audio-player/audio-add-to-playlist/audio-add-to-playlist.model';

@Component({
  selector: 'app-audio-playlist-items',
  templateUrl: './audio-playlist-items.component.html',
  styleUrls: ['./audio-playlist-items.component.css']
})
export class AudioPlaylistItemsComponent implements OnInit {

  public playlists;
  faPencilAlt = faPencilAlt;
  faTrashAlt = faTrashAlt;
  public name:string;
  constructor(private cps: AudioPlaylistService, private toastr: ToastrService, private route: ActivatedRoute, private router: Router) {
   
  }

  ngOnInit(): void {
    this.cps.playlistschanged.
      subscribe(
        (playlists: AudioPlaylist[]) => {
          this.playlists = playlists;
        }
      );
      this.playlists = this.cps.getAllPlaylist();
  }

  onDeletePlaylist(id) {
    
   const prom =  this.cps.deletePlaylist(id).toPromise();
    
   prom.then(res => {
     
          this.cps.audioPlaylistRequest();
          this.playlists = this.cps.getAllPlaylist();
          this.toastr.warning('Removed Playlist');
          this.router.navigate(['..', 'playlist'], { relativeTo: this.route });
   });
   
 
   
  }
 
}
