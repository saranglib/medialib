import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioPlaylistItemsComponent } from './audio-playlist-items.component';

describe('AudioPlaylistItemsComponent', () => {
  let component: AudioPlaylistItemsComponent;
  let fixture: ComponentFixture<AudioPlaylistItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudioPlaylistItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioPlaylistItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
