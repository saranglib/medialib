import { Audio } from './../../play/audio-player/audio.model';
export class  AudioPlaylist {
    public Id:string;
    public name:string;
    public audios:Audio[];
    constructor(Id:string, name: string, audios:Audio[]) {
        this.Id = Id;
        this.name = name;
        this.audios = audios;
    }

get getId(){
    return this.Id;
}

get getName(){
    return this.name;
}

get getVideos(){
    return this.audios;
}
}