import { AudioPlaylist } from "./audio-playlist.model";
import { EventEmitter } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";
import { Audio } from '../../play/audio-player/audio.model';
import { TabHeadingDirective } from "ngx-bootstrap/tabs";
@Injectable()
export class AudioPlaylistService {

  playlistschanged = new EventEmitter<AudioPlaylist[]>();
  playlistsaudioChanged = new EventEmitter<AudioPlaylist[]>();
  playlist: AudioPlaylist[];

  host = environment.host;
  public playlists: AudioPlaylist[] = [];

  constructor(public http: HttpClient) {
    this.playlist = [];
    this.audioPlaylistRequest();
  }
  audioPlaylistRequest(){
   const prom = this.http.get<AudioPlaylist[]>(this.host + '/api/list/?type=audio').toPromise();
   prom.then(res => {
     this.setPlayList(res);
   });
  }
  setPlayList(playlist_data: any[]) {
    var tempPlaylistArray: AudioPlaylist[] = [];
    playlist_data.forEach(function (playlistelement) {
      var pllist: Audio[] = [];
      playlistelement.audios.forEach(function (audioElement) {

        var obj: Audio = new Audio(audioElement.id, audioElement.title, 'http://localhost/' + audioElement.src.audio, 'audio/mp3', audioElement.duration, audioElement.artist, audioElement.album, audioElement.category, audioElement.rating, audioElement.view, audioElement.favorite, audioElement.trackInfo);
        pllist.push(obj);
      })

      var playlist_element: AudioPlaylist = new AudioPlaylist(playlistelement.id, playlistelement.name, pllist);
      tempPlaylistArray.push(playlist_element);
    })
    this.playlist = tempPlaylistArray;
    this.playlistschanged.next(this.playlist.slice());
  }
  getAllPlaylist() {
    if (this.playlist.length != 0) {
      return this.playlist.slice();
    }
  }
  getPlaylist(id: string) {
    if (this.playlist[this.playlist.findIndex(x => x.getId == id)]) {
      return this.playlist[this.playlist.findIndex(x => x.getId == id)];
    }
    return null;
  }
  createPlaylist(id: any, playlist_name: any) {
    let bodyData = {
      "name": playlist_name,
      "items": [],
      "list_type": 300
    };

    return this.http
      .post(this.host + '/api/list/', bodyData, { observe: 'response' })
      .subscribe(
        data => {
          this.audioPlaylistRequest();
        }
      )
    
  }
  deletePlaylist(id: string): Observable<any> {
    return this.http.delete<any>(this.host + '/api/list/' + id + '/', { observe: 'response' });
  }
  removeItemFromPlaylist(playlistId: string, Id: string) {
    this.http.get<any>(this.host + '/api/list/' + playlistId)
      .subscribe(
        data => {
        
          data.items = data.items.filter(x => x != Id);
        
          this.http.put<any>(this.host + '/api/list/' + playlistId + '/', data).subscribe(
            res => {
              this.audioPlaylistRequest();
            }
          );
        }
      );

    return this.getPlaylist(playlistId);
  }
  removeMultipleItemFromPlaylist(playlistId: string, items: string[]) {
    this.http.get<any>(this.host + '/api/list/' + playlistId)
      .subscribe(
        data => {
          for (let i = 0; i < items.length; i++) {
            data.items = data.items.filter(x => x != items[i]);
          }

          this.http.put<any>(this.host + '/api/list/' + playlistId + '/', data).subscribe(
            res => {
              this.audioPlaylistRequest();
            }
          );
        }
      );
    return this.getPlaylist(playlistId);
  }

}

