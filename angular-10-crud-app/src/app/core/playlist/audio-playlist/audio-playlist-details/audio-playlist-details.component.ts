import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { faTrashAlt, faPencilAlt, faList } from '@fortawesome/free-solid-svg-icons';
import { ToastrService } from 'ngx-toastr';
import { DeleteRendererComponent } from 'src/app/cell-renderer/delete-renderer/delete-renderer.component';
import { InfoRendererComponent } from 'src/app/cell-renderer/info-renderer/info-renderer.component';
import { VideoThumbRendererComponent } from 'src/app/cell-renderer/video-thumb-renderer/video-thumb-renderer.component';
import { VideoTitleRendererComponent } from 'src/app/cell-renderer/video-title-renderer/video-title-renderer.component';
import { AudioService } from 'src/app/core/play/audio-player/audio.service';
import { AudioPlaylist } from '../audio-playlist.model';
import { AudioPlaylistService } from '../audio-playlist.service';

@Component({
  selector: 'app-audio-playlist-details',
  templateUrl: './audio-playlist-details.component.html',
  styleUrls: ['./audio-playlist-details.component.css']
})
export class AudioPlaylistDetailsComponent implements OnInit {
  id: number;
  faTrashAlt = faTrashAlt;
  faPencilAlt = faPencilAlt;
  faList = faList;
  i: number;
  j: number;
  public gridApi;
  public gridColumnApi;
  public rowData;
  public columnDefs;
  public rowSelection;
  public rowClassRules;
  public frameworkComponents;
  public rowMultiSelectWithClick;
  public selectedPlaylist: AudioPlaylist;
  public totalAudios: number;
  public rowSelected: boolean;
  ngOnInit(): void {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.selectedPlaylist = this.cps.getPlaylist(this.route.snapshot.params['id']);
          this.rowData = this.selectedPlaylist.audios;
          this.totalAudios = this.rowData.length;
        }
        
      );
      this.cps.playlistschanged.subscribe(
        (playlist: AudioPlaylist[]) => {
          this.selectedPlaylist = this.cps.getPlaylist(this.route.snapshot.params['id']);
          this.rowData = this.selectedPlaylist.audios;
  
        }
      );
      this.cps.playlistsaudioChanged.subscribe(
        
        (playlistAudio: AudioPlaylist) => {
          
          this.selectedPlaylist = this.cps.getPlaylist(this.route.snapshot.params['id']);
          this.rowData = this.selectedPlaylist.audios;
        }
      );
  }
  constructor(private cps: AudioPlaylistService, private toastr: ToastrService, private route: ActivatedRoute, private router: Router, private as: AudioService) {
    this.selectedPlaylist = this.cps.getPlaylist(this.route.snapshot.params['id']);
    
    this.totalAudios = this.selectedPlaylist.audios.length;
    this.rowSelected = false;
    this.rowData = this.selectedPlaylist.audios;
    this.columnDefs = [

      {
        field: 'title',
        headerName: 'Title',
        sortable: true,
        filter: true,
        resizable: true,
        checkboxSelection: true,
        headerCheckboxSelection: true,
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'artist',
        headerName: 'Artist/Vakta',
        sortable: true,
        filter: true,
        resizable: true,
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }

      },
      {
        field: 'duration',
        headerName: 'Duration',
        sortable: true,
        filter: true,
        resizable: true,
        cellClass: "grid-cell-centered",
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'Delete',
        maxWidth: 100,
        cellClass: ["grid-cell-centered"],
        cellRenderer: 'deleteRenderer',
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'Info.',
        maxWidth: 100,
        cellClass: ["grid-cell-centered"],
        cellRenderer: 'infoRenderer',
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      }
    ];
    this.rowSelection = 'multiple';
    
    this.rowClassRules = {
      'playing': function (params) {
        var data = params.data.Id;
        return data == as.selectedRowId;
      },
    }
    this.frameworkComponents = {
      infoRenderer: InfoRendererComponent,
      deleteRenderer: DeleteRendererComponent,
    }

  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  oncellClicked(params) {
    if (params.column.colDef.field === "title") {
      var selectedRows = this.gridApi.getSelectedRows();
      this.as.selectedRowId = selectedRows.length === 1 ? selectedRows[0].Id : '';
      this.gridApi.redrawRows(params);
      this.router.navigate(['/audio',selectedRows[0].id, 'playlist', this.selectedPlaylist.Id]);
    }
    else if (params.column.colDef.field === "Delete") {
     this.selectedPlaylist =  this.cps.removeItemFromPlaylist(this.route.snapshot.params['id'], params.data.id);
     this.rowData = this.selectedPlaylist.audios;
      this.toastr.warning('Removed From Playlist');
    }
  }
  addToQueue() {
    var selectedRows = this.gridApi.getSelectedRows();
    
    for (this.i = 0; this.i < selectedRows.length; this.i++) {
      this.as.addItemToQueue(selectedRows[this.i].id);
      this.toastr.success('Added to Queue Successfully!');
    }
  }
  onDeleteMultiple() {
    var items:string[] = [];
    var selectedRows = this.gridApi.getSelectedRows();
    
    for (this.j = 0; this.j < selectedRows.length; this.j++) {
      items[this.j] = selectedRows[this.j].id;
    }
    
    this.selectedPlaylist = this.cps.removeMultipleItemFromPlaylist(this.route.snapshot.params['id'],items);
    this.rowData = this.selectedPlaylist.audios;
    this.toastr.warning('Removed From Playlist');
  }
  onSelectionChanged(params) {
    var selectedRows = this.gridApi.getSelectedRows();
    this.rowSelected = selectedRows.length > 0 ? true : false;
  }
}
