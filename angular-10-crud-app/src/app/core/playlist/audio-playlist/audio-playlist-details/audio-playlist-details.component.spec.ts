import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioPlaylistDetailsComponent } from './audio-playlist-details.component';

describe('AudioPlaylistDetailsComponent', () => {
  let component: AudioPlaylistDetailsComponent;
  let fixture: ComponentFixture<AudioPlaylistDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudioPlaylistDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioPlaylistDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
