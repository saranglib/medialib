import { Video } from './../../play/video-player/video.model';
export class  VideoPlaylist {
    private Id:string;
    private name:string;
    private videos:Video[];
    constructor(Id:string, name: string, videos: Video[]) {
        this.Id = Id;
        this.name = name;
        this.videos = videos;
    }

    get getId(){
        return this.Id;
    }

    get getName(){
        return this.name;
    }

    get getVideos(){
        return this.videos;
    }
}

