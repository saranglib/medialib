import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistItemsComponent } from './playlist-items.component';

describe('PlaylistItemsComponent', () => {
  let component: PlaylistItemsComponent;
  let fixture: ComponentFixture<PlaylistItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaylistItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
