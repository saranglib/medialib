import { Component, Input, OnInit } from '@angular/core';
import { VideoPlaylist } from '../video-playlist.model';
import { faPencilAlt, faTrashAlt } from '@fortawesome/free-solid-svg-icons'; 
import { VideoPlaylistService } from '../video-playlist.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-playlist-items',
  templateUrl: './playlist-items.component.html',
  styleUrls: ['./playlist-items.component.css']
})
export class PlaylistItemsComponent implements OnInit {
  
  // @Input() playlists: VideoPlaylist[];

  public playlists;
  faPencilAlt = faPencilAlt;
  faTrashAlt = faTrashAlt;

  constructor(private vps: VideoPlaylistService, private route: ActivatedRoute, private router: Router) {
    // this.playlists = this.vps.getAllPlaylist();
   }

  ngOnInit(): void {

    this.vps.playlistschanged.
    subscribe(
      (playlists: VideoPlaylist[])=>{
        this.playlists = this.vps.getAllPlaylist();
      }
    );
    this.playlists = this.vps.getAllPlaylist();
    
  }

  onDeletePlaylist(id){
    this.vps.deletePlaylist(id);
    this.router.navigate(['..','playlist'],{relativeTo: this.route});
  }
}
