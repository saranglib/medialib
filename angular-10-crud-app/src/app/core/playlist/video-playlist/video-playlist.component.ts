import { Component, OnInit } from '@angular/core';
import { faPencilAlt, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { VideoPlaylist } from './video-playlist.model';
import { VideoPlaylistService } from './video-playlist.service';
@Component({
  selector: 'app-video-playlist',
  templateUrl: './video-playlist.component.html',
  styleUrls: ['./video-playlist.component.css']
})
export class VideoPlaylistComponent implements OnInit {

  faPencilAlt = faPencilAlt;
  faTrashAlt = faTrashAlt;
  public playlists: VideoPlaylist[];
  
  constructor(private vps: VideoPlaylistService) {
    this.playlists = vps.getAllPlaylist();
   }

  ngOnInit(): void {
  }

}
