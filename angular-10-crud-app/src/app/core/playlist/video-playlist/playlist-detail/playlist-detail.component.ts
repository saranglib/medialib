import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { faTrashAlt, faPencilAlt } from '@fortawesome/free-solid-svg-icons';
import { DeleteRendererComponent } from 'src/app/cell-renderer/delete-renderer/delete-renderer.component';
import { InfoRendererComponent } from 'src/app/cell-renderer/info-renderer/info-renderer.component';
import { VideoThumbRendererComponent } from 'src/app/cell-renderer/video-thumb-renderer/video-thumb-renderer.component';
import { VideoTitleRendererComponent } from 'src/app/cell-renderer/video-title-renderer/video-title-renderer.component';
import { VideoPlaylist } from '../video-playlist.model';
import { VideoPlaylistService } from '../video-playlist.service';

@Component({
  selector: 'app-playlist-detail',
  templateUrl: './playlist-detail.component.html',
  styleUrls: ['./playlist-detail.component.css']
})
export class PlaylistDetailComponent implements OnInit {

  faTrashAlt = faTrashAlt;
  faPencilAlt = faPencilAlt;
  
  public gridApi;
  public gridColumnApi;
  public rowData;
  public columnDefs;
  public rowSelection;
  public frameworkComponents;
  public rowMultiSelectWithClick;
  public selectedPlaylist:  VideoPlaylist;
  public totalVideos: number;
  public rowSelected: boolean;
  
  constructor(private vps: VideoPlaylistService, private route: ActivatedRoute, private router: Router) { 
    this.selectedPlaylist = this.vps.getPlaylist(this.route.snapshot.params['id']);
    this.totalVideos = this.selectedPlaylist.getVideos.length;
    this.rowSelected = false;
    this.rowData = this.selectedPlaylist.getVideos;
    this.columnDefs = [
      {
        field: 'Id',
        headerName:"Id",
        cellRenderer: 'videoThumbRenderer',      
        autoHeight: true,
        maxWidth :300,
        checkboxSelection: true, 
        headerCheckboxSelection: true,
      },
      {
        field:'title',
        headerName:'Title',
        sortable: true,
        filter: true,
        resizable: true, 
        cellRenderer: 'titleRenderer'
      },
      {
        field: 'Delete',
        maxWidth: 100,
        cellClass: ["grid-cell-centered","icon-size"],
        cellRenderer: 'deleteRenderer'
      },
      {
        field: 'Info.',
        maxWidth: 100,
        cellClass: ["grid-cell-centered","icon-size"],
        cellRenderer: 'infoRenderer'
      }
    ];
    this.frameworkComponents = {
      videoThumbRenderer: VideoThumbRendererComponent,
      infoRenderer: InfoRendererComponent,
      titleRenderer: VideoTitleRendererComponent,
      deleteRenderer: DeleteRendererComponent,
    }
    this.rowSelection= 'multiple';
    this.rowMultiSelectWithClick =  true;
  }

  onFirstDataRendered(params)
  {
    params.api.sizeColumnsToFit();
  }
  
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }


  onSelectionChanged(params){
    var selectedRows = this.gridApi.getSelectedRows();
    this.rowSelected = selectedRows.length > 0 ? true: false;
  }

  ngOnInit(): void {
    this.route.params
    .subscribe(
      (params: Params)=>{
        this.selectedPlaylist = this.vps.getPlaylist(this.route.snapshot.params['id']);
        this.rowData = this.selectedPlaylist.getVideos;
        this.totalVideos = this.rowData.length;
      }
    );
  }

  onDelete(id){
    this.vps.deletePlaylist(id);
    this.router.navigate(['..'],{relativeTo: this.route});
  }

}
