import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Subject } from 'rxjs';
import { Video } from '../../play/video-player/video.model'
import { VideoPlaylist } from './video-playlist.model'

@Injectable()
export class VideoPlaylistService implements OnInit{

    playlistschanged = new Subject<VideoPlaylist[]>();
    playlist:VideoPlaylist[];
    
      host = environment.host;
  
      PlaylistRequest() {
          this.http
           .get<VideoPlaylist[]>(this.host+'/api/list/?type=video',
           )
           .subscribe(              
             data => {                         
               this.setPlayList(data);           
             }        
           );      
       }
     
       constructor(public http: HttpClient) { 
        this.playlist = [];
         this.PlaylistRequest();       
       }
  
       setPlayList(playlist_data: any[]) {
        
          var tempPlaylistArray:VideoPlaylist[]=[];
          playlist_data.forEach(function(playlistelement){   
              
              var pllist:Video[]=[];
              playlistelement.videos.forEach(function(video){
              
                  let vposter;
                  if(Object.keys(video.src).length == 1){
                    vposter=video.src.video+'#t=5'
                  }else{
                    vposter=video.src.image_thumbnail
                  }

                  var obj:Video = new Video(video.id,video.title,video.src.video,'video/mp4','http://localhost/'+vposter,video.rating,video.view,video.favourites,video.duration,video.rating);
                  pllist.push(obj)
              })
  
              var playlist_element:VideoPlaylist = new VideoPlaylist(playlistelement.id,playlistelement.name,pllist);
              tempPlaylistArray.push(playlist_element);            
          })
  
          
          this.playlist = tempPlaylistArray;
          
          this.playlistschanged.next(this.playlist.slice());    
          
        }

    getAllPlaylist(){
      // this.PlaylistRequest();
      if(this.playlist.length != 0){
        return this.playlist.slice();
      }
    }


    createPlaylist(id:any, playlist_name:any){  
      let bodyData = {
        "name": playlist_name,
        "items": [id],
        "list_type": 100
        };
        
      return this.http      
        .post(this.host+'/api/list/',bodyData,{observe:'response'})
        .subscribe(
          data => {                  
          }
        )  
    }

    getPlaylist(id:string)
    {
      if(this.playlist[this.playlist.findIndex(x => x.getId == id)])
      {
        return this.playlist[this.playlist.findIndex(x => x.getId == id)];
      }
      return null;
    }

    deletePlaylist(id: string){
      this.playlist = this.playlist.filter(x=>x.getId !== id);
      this.playlistschanged.next(this.playlist.slice());
    }

    deleteMultiple(id:number, ids:number[]){
      
    }

    ngOnInit(){

    }

}