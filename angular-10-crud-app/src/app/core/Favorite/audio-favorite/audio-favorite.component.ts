import { Component, OnInit } from '@angular/core';
import { InfoRendererComponent } from 'src/app/cell-renderer/info-renderer/info-renderer.component';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { AudioService } from '../../play/audio-player/audio.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DeleteRendererComponent } from 'src/app/cell-renderer/delete-renderer/delete-renderer.component';
import { ToastrService } from 'ngx-toastr';
import { AudioFavorite } from './audio-favorite.model';
import { AudioFavoriteService } from './audio-favorite.service'
@Component({
  selector: 'app-audio-favorite',
  templateUrl: './audio-favorite.component.html',
  styleUrls: ['./audio-favorite.component.css']
})

export class AudioFavoriteComponent implements OnInit {

  faTrashAlt = faTrashAlt;
  i: number;
  j: number
  public gridApi;
  public gridColumnApi;
  public rowData;
  public columnDefs;
  public rowSelection;
  public frameworkComponents;
  public rowMultiSelectWithClick;
  public rowSelected;
  public domLayout;
  ngOnInit(): void {
    this.afs.favoriteChanged.subscribe(
      (favorites: AudioFavorite[]) => {
        this.rowData = this.afs.getFavorites();
        this.gridApi.redrawRows();
      }
    )
   this.afs.audioFavoritesRequest().subscribe(res =>{
      this.rowData = res;
    });
  }
  constructor(private afs: AudioFavoriteService, private route: ActivatedRoute, private toastr: ToastrService, private router: Router, private as: AudioService) {
 
    this.rowSelected = false;
    this.columnDefs = [

      {
        field: 'title',
        headerName: 'Title',
        sortable: true,
        filter: true,
        resizable: true,
        checkboxSelection: true,
        headerCheckboxSelection: true,
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'artist',
        headerName: 'Artist/Vakta',
        sortable: true,
        filter: true,
        resizable: true,
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'duration',
        headerName: 'Duration',
        sortable: true,
        filter: true,
        resizable: true,
        cellClass: "grid-cell-centered",
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'Delete',
        maxWidth: 100,
        cellClass: ["grid-cell-centered"],
        cellRenderer: 'deleteRenderer',
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'Info.',
        maxWidth: 100,
        cellClass: ["grid-cell-centered"],
        cellRenderer: 'infoRenderer',
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      }
    ];
    this.rowSelection = 'multiple';
   
    this.domLayout = 'autoHeight';
    this.frameworkComponents = {
      infoRenderer: InfoRendererComponent,
      deleteRenderer: DeleteRendererComponent
    };
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }
   oncellClicked(params) {
    if (params.column.colDef.field === "title") {
      var selectedRows = this.gridApi.getSelectedRows();
      this.as.selectedRowId = selectedRows.length === 1 ? selectedRows[0].Id : '';
      this.gridApi.redrawRows(params);
      this.router.navigate(['/audio', params.data.id, 'favorite']);
    }
    else if (params.column.colDef.field === "Delete") {
      var selectedRows = this.gridApi.getSelectedRows();
      const prom =  this.as.FavoriteChange(params.data.id).toPromise();
      prom.then(res => {
        this.afs.audioFavoritesRequest().subscribe(res =>{
          this.rowData = res;
        });
      })
      this.gridApi.redrawRows(params);
      this.toastr.warning('Removed From Favorites');
    }
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  onSelectionChanged(params) {
    var selectedRows = this.gridApi.getSelectedRows();
    this.rowSelected = selectedRows.length > 0 ? true : false;
  }
  addToQueue() {
    var selectedRows = this.gridApi.getSelectedRows();
    for (this.i = 0; this.i < selectedRows.length; this.i++) {
      this.as.addItemToQueue(selectedRows[this.i].id);
      this.toastr.success('Added to Queue Successfully');
    }
  }
  removeFromFavorite() {
    var selectedRows = this.gridApi.getSelectedRows();
    for (this.j = 0; this.j < selectedRows.length; this.j++) {
      this.as.FavoriteChange(selectedRows[this.j].id).subscribe(res => {
    });
    this.afs.audioFavoritesRequest().subscribe(res =>{
      this.rowData = res;
    });
    this.toastr.warning('Removed From Favorites');
  }
  }
 

}

