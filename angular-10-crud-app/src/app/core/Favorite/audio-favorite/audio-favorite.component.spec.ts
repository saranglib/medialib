import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioFavoriteComponent } from './audio-favorite.component';

describe('AudioFavoriteComponent', () => {
  let component: AudioFavoriteComponent;
  let fixture: ComponentFixture<AudioFavoriteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudioFavoriteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioFavoriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
