import { EventEmitter } from '@angular/core';
import { AudioFavorite } from './audio-favorite.model';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AudioFavoriteService {
  favoriteChanged = new EventEmitter<AudioFavorite[]>();
  private favorites: AudioFavorite[] = [];
  host = environment.host;

  audioFavoritesRequest() :Observable<AudioFavorite[]>{
    
    return  this.http.get<AudioFavorite[]>(this.host + '/api/favourites?type=audio');
     
  }

  constructor(private http: HttpClient) {
    this.audioFavoritesRequest();

  }

  
  getFavorites() {
    return this.favorites.slice();
  }

}