export class AudioFavorite {
    public id: number;
    public title: string;
    public audioId:number;
    public trackInfo : any;
    public artist:string;
    public duration: string;

    constructor(id:number, title: string, audioId:number,artist:string,trackInfo : any,duration: string) {
        this.id = id;
        this.title = title;
        this,audioId=audioId;
        this.trackInfo = trackInfo;
        this.artist = artist;
        this.duration = duration;
    }
}