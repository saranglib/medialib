import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoFavoriteComponent } from './video-favorite.component';

describe('VideoFavoriteComponent', () => {
  let component: VideoFavoriteComponent;
  let fixture: ComponentFixture<VideoFavoriteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoFavoriteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoFavoriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
