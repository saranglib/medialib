import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Subject } from 'rxjs';
import { VideoFavorite } from './video-favorite.model';

@Injectable()
export class VideoFavoriteService implements OnInit{
    private favorites:VideoFavorite[] = [];
    favoritesChanged = new Subject<VideoFavorite[]>();
    host = environment.host;

    favoritesRequest() {
        this.http
         .get<VideoFavorite[]>(this.host+'/api/favourites/',
         )
         .subscribe(              
           data => {          
             this.setfavoriteslist(data);           
           }        
         );      
     }
   
     constructor(private http: HttpClient) {    
       this.favoritesRequest();
       
     }

     setfavoriteslist(videos: any[]) {

      var tempVideoArray:VideoFavorite[]=[];
      videos.forEach(function(videoElement){                  
          let vposter;
          if(videoElement.src.image_thumbnail){
              vposter = videoElement.src.image_thumbnail
          }else{
              vposter = videoElement.src.video + '#t=5'
          }
          // let datetime = new Date(videoElement.visited_at)            
          // id:number, title: string, vposter: string,rating:number,view:number, duration: string
        var obj:VideoFavorite = new VideoFavorite(videoElement.id,videoElement.title,'http://localhost/'+vposter,videoElement.rating,videoElement.view,videoElement.duration);                    
        tempVideoArray.push(obj);
      })  
      this.favorites = tempVideoArray;
      this.favoritesChanged.next(this.favorites.slice());           
      }

    getFavorites(){
        return this.favorites.slice();
    }

    ngOnInit(){}
}