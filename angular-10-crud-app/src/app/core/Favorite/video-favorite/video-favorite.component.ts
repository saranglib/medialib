import { Component, OnInit, OnDestroy } from '@angular/core';
import { InfoRendererComponent } from 'src/app/cell-renderer/info-renderer/info-renderer.component';
import { VideoThumbRendererComponent } from 'src/app/cell-renderer/video-thumb-renderer/video-thumb-renderer.component';
import { VideoTitleRendererComponent } from 'src/app/cell-renderer/video-title-renderer/video-title-renderer.component';
import { VideoFavoriteService } from './video-favorite.service';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';
import { VideoFavorite } from './video-favorite.model'

@Component({
  selector: 'app-video-favorite',
  templateUrl: './video-favorite.component.html',
  styleUrls: ['./video-favorite.component.css']
})
export class VideoFavoriteComponent implements OnInit,OnDestroy {

  faTrashAlt = faTrashAlt;

  public gridApi;
  public gridColumnApi;
  public rowData;
  public columnDefs;
  public rowSelection;
  public frameworkComponents;
  public rowMultiSelectWithClick;
  public rowSelected;
  public domLayout;

  subscription: Subscription;

  constructor(private vfs:VideoFavoriteService) { 
    this.rowData = this.vfs.getFavorites();
    this.rowSelected = false;
    this.columnDefs= [
      {
        field: 'Id',
        headerName:"Id",
        cellRenderer: 'videoThumbRenderer',
        autoHeight: true,
        maxWidth :300,
        checkboxSelection: true, 
        headerCheckboxSelection: true,
      },
      {
        field:'title',
        headerName:'Title',
        sortable: true,
        filter: true,
        resizable: true, 
        cellRenderer: 'titleRenderer'
      },
      {
        field: 'Info.',
        maxWidth: 100,
        cellClass: ["grid-cell-centered","icon-size"],
        cellRenderer: 'infoRenderer'
      }
    ];
    this.rowSelection = 'multiple';
    this.rowMultiSelectWithClick =  true;
    this.domLayout = 'autoHeight';
    this.frameworkComponents = {
      videoThumbRenderer:VideoThumbRendererComponent,
      titleRenderer: VideoTitleRendererComponent,
      infoRenderer: InfoRendererComponent,
    };
  }

  onFirstDataRendered(params)
  {
    params.api.sizeColumnsToFit();
  }
  
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  onSelectionChanged(params){
    var selectedRows = this.gridApi.getSelectedRows();
    this.rowSelected = selectedRows.length > 0 ? true: false;
  }
  
  ngOnInit(): void {
    this.subscription = this.vfs.favoritesChanged.subscribe(
      (videos: VideoFavorite[]) => {
        this.rowData = this.vfs.getFavorites();
      }
      );
    this.rowData = this.vfs.getFavorites();
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
