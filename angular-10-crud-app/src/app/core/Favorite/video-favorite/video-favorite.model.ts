export class VideoFavorite {
    public id: number;
    public title: string;
    public vposter: string;
    public rating: number;
    public view: number;
    public duration: string;

    constructor(id:number, title: string, vposter: string,rating:number,view:number, duration: string) {
        this.id = id;
        this.title = title;
        this.vposter = vposter;
        this.rating = rating;
        this.view = view;
        this.duration = duration;
    }
}