import { EventEmitter } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Admin } from './admin-model'
import { file } from '@rxweb/reactive-form-validators';
@Injectable()
export class AdminService {
  
  itemChanged = new Subject<Admin[]>();
  private items: Admin[] = [];
  public data:Object= [];
  dataChanged = new EventEmitter<Object>();
  host = environment.host 
  public folderData?;
  media_type = new Map([
    [100,"Video"],
    [200,"Document"],
    [300,"Audio"]
  ]);

  getAllItemsRequest() {
    
    this.http
     .get(this.host+'/api/item/',
     )
     .subscribe(              
       data => {  
        this.setAllItemList(data["results"]);           
       }        
    );   
   
     
  }

  setAllItemList(allItems:any[]){
    var tempAudioArray:Admin[]=[];
    allItems.forEach((itemElement) => {
      var  path =new String( itemElement["item_asset"][0].asset_file[0].path);
      var filename =new String( itemElement["item_asset"][0].asset_file[0].filename);
      var fullpath = path.concat("/",filename.toString());
      var category = Array(itemElement.metadata.category.split(", "));
      var artist = Array(itemElement.metadata.artist.split(", "));
      var album  = Array(itemElement.metadata.album.split(", "));
      var type = this.media_type.get(itemElement.item_type);
       
      var obj:Admin = new Admin(itemElement.item_uuid,itemElement.metadata.title,artist,album,category,type,fullpath);
      tempAudioArray.push(obj);
    });
    this.items = tempAudioArray;
  
    this.itemChanged.next(this.items.slice()); 
  }
  constructor(private http: HttpClient) {
    this.getAllItemsRequest(); 
  }

  
  getAllItems() {
    
    return this.items.slice();
  }
  ngOnInit(){
    this.getAllItemsRequest();   
  }

  getItem(index: number) {
    
   return this.items.find(a => a.id == index);
  }

  getMetadata(){
    return  this.http.get(this.host + '/api/metadata');
  }

  getFileMetadata(filepath:string){
    return this.http.get(this.host + '/api/get_metadata/?filepath='+filepath);
  }
 
  postItem(itemObj:any){
    
    const headers = { 'content-type': 'application/json' }
    const body = JSON.stringify(itemObj);
    return this.http.post(this.host +'/api/details/', body, { 'headers': headers });

  }
  
    
   
  }
