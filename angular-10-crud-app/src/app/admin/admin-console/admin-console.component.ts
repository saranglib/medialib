
import {COMMA, ENTER} from '@angular/cdk/keycodes';

import {FormControl, FormGroup} from '@angular/forms';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import { MatChipInput } from '@angular/material/chips';
import { MatChipInputEvent } from '@angular/material/chips';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { Component, OnInit, ViewChild, ViewEncapsulation ,ElementRef} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatChipsModule } from "@angular/material/chips";
import { MatFormFieldModule } from "@angular/material/form-field";
import { AdminService } from '../admin-service';
import { file } from '@rxweb/reactive-form-validators';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Admin } from '../admin-model';
import { environment } from 'src/environments/environment';
import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import {NestedTreeControl} from '@angular/cdk/tree';

@Component({
  selector: 'app-admin-console',
  templateUrl: './admin-console.component.html',
  styleUrls: ['./admin-console.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AdminConsoleComponent implements OnInit {
  fileToUpload: File[] = [];
  disabled = false;
  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = false;
  separatorKeysCodes = [ENTER, COMMA];
  ShowFilter = true;
  limitSelection = false;
  selectedItems: string[] = [];
  dropdownSettings: any = {};
  mediaTypeSettings:any = {};
  albumctrl = new FormControl();
  categoryctrl = new FormControl();
  artistctrl = new FormControl();
  filteredCategory:Observable<any[]>;
  filteredArtist: Observable<any[]>;
  filteredAlbum:Observable<any[]>;
  itemId;
  itemObject:Admin;
  category = [];
  album = [];
  artist =[];
  allAlbum =[];
  allArtist = [];
  allCategory = [];
  itemArray = [];
  albumString:string = "";
  categoryString:string;
  artistString:string;
  titleName:string;
  selectedCategory:string = "";
  accept:string;
  selectedOption:string = "" ;
  selected;
  path = environment.path;
  device_uuid = environment.device_uuid;
  public tree: TreeViewComponent;
  mime_type = ["Audio","Video","Document"]
  media_type = new Map([
    ["Video",100],
    ["Document",200],
    ["Audio",300]
  ]);

  @ViewChild('categoryInput') CategoryInput: ElementRef;
  @ViewChild('artistInput') ArtistInput: ElementRef;
  @ViewChild('albumInput') AlbumInput: ElementRef;
  @ViewChild('labelImport') labelImport: ElementRef;
  @ViewChild('title') title:ElementRef;
 // @ViewChild('filepath') filepath:ElementRef;
 filepath;
  myForm: FormGroup = new FormGroup({
    title: new FormControl(),
    tags: new FormControl(),
    albumctrl: new FormControl(),
    categoryctrl: new FormControl(this.selectedCategory),
    artistctrl: new FormControl(),
    uploadFile : new FormControl(),
    mediatype:new FormControl(this.selectedOption),
  });
  
  constructor(private fb:FormBuilder,private adminService : AdminService,private route: ActivatedRoute, private toastr: ToastrService, private router: Router) {
    this.route.params.subscribe((params: Params) => {
      this.itemId = params['id'];
     console.log(this.itemId);
    });

    this.itemObject = this.adminService.getItem(this.itemId);
    if(this.itemObject!=null || this.itemObject!=undefined){
    this.filepath = this.itemObject.filePath;
      this.selectedCategory = this.itemObject.category[0];
      this.selectedOption = "Audio";
        this.itemObject.album.forEach(al => 
          {
            this.album.push(al);
          });
          this.itemObject.artist.forEach(ar => 
            {
              this.artist.push(ar);
            });
      this.myForm = new FormGroup({
        title: new FormControl(this.itemObject.title, [
          Validators.required,
        ]),
        categoryctrl: new FormControl(this.selectedCategory, [
          Validators.required,
        ]),
        mediatype : new FormControl(this.selectedOption,[
          Validators.required,
        ])
      });
    }
     
    this.filteredAlbum = this.albumctrl.valueChanges.pipe(
        startWith(null),
        map((album: string | null) => album ? this.filterAlbum(album) : this.allAlbum.slice()));

        this.filteredArtist = this.artistctrl.valueChanges.pipe(
          startWith(null),
          map((artist: string | null) => artist ? this.filterArtist(artist) : this.allArtist.slice()));

        
  }

  ngOnInit(): void {
      this.adminService.getMetadata().subscribe(res => {
        console.log(res);
        this.allAlbum = res["album"];
        this.allArtist = res["artist"];
        this.allCategory = res["category"];
    
      });
     
      this.dropdownSettings = {
        singleSelection: false,
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 3,
        allowSearchFilter: this.ShowFilter
    };
    this.mediaTypeSettings = {
      singleSelection: true,
      maxHeight: 197,
    }

    
  }
  
home(){
  this.router.navigate(['admin']);
}
  setAccept(){
    debugger
    console.log(this.myForm.controls["mediatype"].value[0]);
    if(this.myForm.controls["mediatype"].value[0] == "Audio"){
      this.accept = "audio/*";
    }
    else if(this.myForm.controls["mediatype"].value[0]== "Video"){
      this.accept = "video/*";
    }
    else{
      this.accept = ".pdf,.doc";
    }
  }
  
  addArtist(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.artist.push(value.trim());
    }
   
    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.artistctrl.setValue(null);
  }
  onFilterChange(item: any) {
        
  }
  onFileChange(files: FileList) {
    debugger
    console.log(files);
    if(files.length > 1){
        this.title.nativeElement.disabled = true;
    }
    else {
      this.adminService.getFileMetadata("new").subscribe(res => {
        console.log(res);
      })
      this.title.nativeElement.disabled = false;
    }
    this.labelImport.nativeElement.innerText = Array.from(files)
      .map(f => f.name)
      .join(', ');
    for(var i=0; i < files.length; i++){
      var file =new FileReader();
        this.fileToUpload.push(files[i]);
    }
  
    console.log(files);

  }

  onItemSelect(item: any) {
    console.log('onItemSelect', item);
    console.log(this.myForm.controls["mediatype"].value[0]);
    if(this.myForm.controls["mediatype"].value[0] == "Audio"){
      this.accept = "audio/*";
    }
    else if(this.myForm.controls["mediatype"].value[0]== "Video"){
      this.accept = "video/*";
    }
    else{
      this.accept = ".pdf,.doc";
    }
}
onSelectAll(items: any) {
    console.log('onSelectAll', items);
}
  addAlbum(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.album.push(value.trim());
    }
   
    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.albumctrl.setValue(null);
  }
  
  removeArtist(art: any): void {
    const index = this.artist.indexOf(art);

    if (index >= 0) {
      this.artist.splice(index, 1);
    }
  }
  removeAlbum(alb: any): void {
    const index = this.album.indexOf(alb);

    if (index >= 0) {
      this.album.splice(index, 1);
    }
  }


  filterAlbum(name: string) {
    return this.allAlbum.filter(alb =>
        alb.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }
  filterArtist(name: string) {
    return this.allArtist.filter(art =>
        art.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  selectedArtist(event: MatAutocompleteSelectedEvent): void {
    this.artist.push(event.option.viewValue);
    this.ArtistInput.nativeElement.value = '';
    this.artistctrl.setValue(null);
  }
  selectedAlbum(event: MatAutocompleteSelectedEvent): void {
    this.album.push(event.option.viewValue);
    this.AlbumInput.nativeElement.value = '';
    this.albumctrl.setValue(null);
  }
 

  onSubmit(){
    debugger
    var sepratorString = ', ';
    this.albumString = this.album.join(sepratorString);
    this.categoryString = this.myForm.controls["categoryctrl"].value.join(sepratorString);
    this.artistString  = this.artist.join(sepratorString);
    this.fileToUpload.forEach(file => {
      if(this.fileToUpload.length > 1) {
        this.titleName = file.name;
      }
      else{
        this.titleName = this.myForm.controls["title"].value;
      }
      console.log(this.myForm.controls["mediatype"].value[0]);
      var type = this.media_type.get(this.myForm.controls["mediatype"].value[0]);
      console.log(type);
      this.itemArray.push({"item_asset": [{ "asset_file": [{ "path": "data","filename": file.name,"device_uuid": this.device_uuid}],"asset_type": type,"mime_type": file.type, "metadata": {  "key": "value" } }],"metadata": {"album": this.albumString,"title": this.titleName, "artist":this.artistString, "category": this.categoryString, "duration": 111},"tags": { "key" : "value" },"views": 0,"item_type": type}); 
  
    });
    console.log(this.itemArray);
   this.adminService.postItem(this.itemArray).subscribe(res => {
    console.log(res);
    this.toastr.success("Added Item Successfully..")
  })
  }
}