export class Admin{
    public id: number;
    public title: string;
    public artist : string[];
    public album:string[];
    public category:string[];
    public media_type:string;
    public filePath:string;

    constructor(id:number, title: string,artist:string[],album : string[],category: string[],media_type:string,filepath:string) {
        this.id = id;
        this.title = title;
        this.artist = artist;
        this.album = album;
        this.category = category;
        this.media_type = media_type;
        this.filePath = filepath;
    }
}