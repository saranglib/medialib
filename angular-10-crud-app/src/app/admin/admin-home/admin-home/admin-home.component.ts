import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { EditRendererComponent } from 'src/app/cell-renderer/edit-renderer/edit-renderer.component';
import { Admin } from '../../admin-model';
import { AdminService } from '../../admin-service';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {


  public gridApi;
  public gridColumnApi;
  public rowData;
  public columnDefs;
  public rowSelection;
  public frameworkComponents;
  public rowMultiSelectWithClick;
  public rowSelected;
  public domLayout;
  public getRowNodeId: any;
  public defaultColDef: any;
  subscription: Subscription;

  ngOnInit(): void {
    

    this.subscription = this.adminService.itemChanged.subscribe(
      (items: Admin[]) => {
        this.rowData = this.adminService.getAllItems();
      }
      );
    this.rowData = this.adminService.getAllItems();
  }


  constructor(private adminService : AdminService, private route: ActivatedRoute, private toastr: ToastrService, private router: Router) {
    
    this.rowData =  this.adminService.getAllItems();
   // this.selectedRowId = as.selectedRowId;
    this.adminService.getAllItemsRequest();
    this.rowSelected = false;
    this.columnDefs = [

      {
        field: 'title',
        headerName: 'Title',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        editable: true,
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'artist',
        headerName: 'Artist/Vakta',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        editable: true,
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'album',
        headerName: 'Album',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        editable: true,
        cellClass: "grid-cell-centered",
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'category',
        headerName: 'Category',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        editable: true,
        cellClass: ["grid-cell-centered"],
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        field: 'media_type',
        headerName: 'Type',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true,
        editable: true,
        cellClass: ["grid-cell-centered"],
        cellStyle: { 'font-size': '24px', 'letter-spacing': '1.2px' }
      },
      {
        flex: 1,
        field: 'Edit',
        maxWidth: 100,
        cellClass: ["grid-cell-centered","icon-size"],
        cellRenderer: 'editRenderer',
      },
    ];

    this.getRowNodeId = function (data: any) {
      return data.id;
    };
    this.defaultColDef = {
      minWidth: 200,
      resizable: true,
      floatingFilter: true,
      sortable: true,
      editable: true,
      suppressHorizontalScroll: true,
    };
    this.domLayout = 'autoHeight';
    this.frameworkComponents = {
      editRenderer: EditRendererComponent,
    };
  }


  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }
  oncellClicked(params) {
    if (params.column.colDef.field === "Edit") {
      this.router.navigate(['admin-edit', params.data.id]);
    }
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  onSelectionChanged(params) {
    var selectedRows = this.gridApi.getSelectedRows();
    this.rowSelected = selectedRows.length > 0 ? true : false;
  }

  addItem(){
    this.router.navigate(['\admin-add']);
  }

  onCellValueChanged(params:any ) {
   }
}
