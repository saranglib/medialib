import { I } from '@angular/cdk/keycodes';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from '../admin/admin-service';
import { AudioHistoryService } from '../core/History/audio-history/audio-history.service';
import { AudioService } from '../core/play/audio-player/audio.service';

@Component({
  selector: 'app-cover-page',
  templateUrl: './cover-page.component.html',
  styleUrls: ['./cover-page.component.css']
})
export class CoverPageComponent implements OnInit {

  constructor(private ahs:AudioHistoryService,private as:AudioService,private router: Router,private route: ActivatedRoute,private adminService : AdminService) { }

  id:number;
 
  ngOnInit(): void { }

  audioRedirect(){
   this.id = this.as.suggestions[0].id;
   this.router.navigate(['/audio',this.id, 'home']);
   
  }

}
