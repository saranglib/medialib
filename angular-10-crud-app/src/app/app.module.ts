import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule,HttpClientXsrfModule,HTTP_INTERCEPTORS} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';


import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { RxReactiveDynamicFormsModule } from '@rxweb/reactive-dynamic-forms';
 

import { AngularSvgIconModule } from 'angular-svg-icon';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AgGridModule } from 'ag-grid-angular';

import { RatingModule } from 'ngx-bootstrap/rating';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ToastrModule } from 'ngx-toastr';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CollapseModule } from 'ngx-bootstrap/collapse';

import { VgCoreModule } from '@videogular/ngx-videogular/core';
import { VgControlsModule } from '@videogular/ngx-videogular/controls';
import { VgOverlayPlayModule } from '@videogular/ngx-videogular/overlay-play';
import { VgBufferingModule } from '@videogular/ngx-videogular/buffering';

import { AppComponent } from './app.component';
import { CoreComponent } from './core/core.component';
import { BasicComponent } from './core/basic/basic.component';
import { LeftPanelComponent } from './core/basic/left-panel/left-panel.component';
import { RightPanelComponent } from './core/basic/right-panel/right-panel.component';
import { HeaderComponent } from './core/basic/header/header.component';
import { LoginComponent } from './login/login.component';
import { UserInfoComponent } from './core/basic/header/user-info/user-info.component';
import { CoverPageComponent } from './cover-page/cover-page.component';
import { PlayComponent } from './core/play/play.component';
import { VideoPlayerComponent } from './core/play/video-player/video-player.component';
import { TabsComponent } from './core/play/video-player/tabs/tabs.component';
import { HomeComponent } from './core/home/home.component';
import { RecommendationComponent } from './core/home/recommendation/recommendation.component';
import { VideoRecommendationComponent } from './core/home/recommendation/video-recommendation/video-recommendation.component';
import { VideoListItemComponent } from './core/home/recommendation/video-recommendation/video-list-item/video-list-item.component';
import { VideoPlaylistComponent } from './core/playlist/video-playlist/video-playlist.component';
import { PlayerComponent } from './core/play/video-player/player/player.component';
import { PlaylistDetailComponent } from './core/playlist/video-playlist/playlist-detail/playlist-detail.component';
import { VideoThumbRendererComponent } from './cell-renderer/video-thumb-renderer/video-thumb-renderer.component';
import { VideoTitleRendererComponent } from './cell-renderer/video-title-renderer/video-title-renderer.component';
import { InfoRendererComponent } from './cell-renderer/info-renderer/info-renderer.component';
import { FavoriteRendererComponent } from './cell-renderer/favorite-renderer/favorite-renderer.component';
import { DeleteRendererComponent } from './cell-renderer/delete-renderer/delete-renderer.component';

import { VideoService } from './core/play/video-player/video.service';
import { VideoHistoryService } from './core/History/video-history/video-history.service';
import { VideoFavoriteService } from './core/Favorite/video-favorite/video-favorite.service';
import { BookmarkService } from './core/bookmark/common-bookmark/bookmark.service';

import { VideoPlaylistService } from './core/playlist/video-playlist/video-playlist.service';
import { CategoryComponent } from './core/home/category/category.component';
import { AudioPlayerComponent } from './core/play/audio-player/audio-player.component';
import { BookmarkComponent } from './core/play/audio-player/bookmark/bookmark.component';
import { DetailsViewComponent } from './core/play/audio-player/details-view/details-view.component';
import { AudioTabsComponent } from './core/play/audio-player/details-view/audio-tabs/audio-tabs.component';
import { AudioService } from './core/play/audio-player/audio.service';
import { PlaylistItemsComponent } from './core/playlist/video-playlist/playlist-items/playlist-items.component';
import { VideoHistoryComponent } from './core/History/video-history/video-history.component';
import { VideoFavoriteComponent } from './core/Favorite/video-favorite/video-favorite.component';
import { AudioRecommendationComponent } from './core/home/recommendation/audio-recommendation/audio-recommendation.component';
import { RatingRendererComponent } from './cell-renderer/rating-renderer/rating-renderer.component';
import { AudioQueueComponent } from './core/play/audio-player/details-view/audio-tabs/audio-queue/audio-queue.component';
import { AudioBookmarkDetailsComponent } from './core/play/audio-player/details-view/audio-tabs/audio-bookmark-details/audio-bookmark-details.component';
import { AudioBookmarkEditComponent } from './core/play/audio-player/details-view/audio-tabs/audio-bookmark-edit/audio-bookmark-edit.component';
import { AudioBookmarkService } from './core/play/audio-player/details-view/audio-tabs/audio-bookmark-details.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { VgPlayPause } from './core/play/audio-player/vg-play-pause/vg-play-pause.component';
import { AudioAddToPlaylistComponent } from './core/play/audio-player/audio-add-to-playlist/audio-add-to-playlist.component';
import { QueueComponent } from './core/play/video-player/queue/queue.component'
import { SugeestionsComponent } from './core/play/video-player/sugeestions/sugeestions.component';

import { VideoCardComponent } from './core/home/recommendation/video-recommendation/video-card/video-card.component';

import { VideoAddToPlaylistComponent } from './core/play/video-player/video-add-to-playlist/video-add-to-playlist.component';


import { CommonBookmarkComponent } from './core/bookmark/common-bookmark/common-bookmark.component';
import { BookmarkItemsComponent } from './core/bookmark/common-bookmark/bookmark-items/bookmark-items.component';
import { BookmarkDetailsComponent } from './core/bookmark/common-bookmark/bookmark-details/bookmark-details.component';
import { EditRendererComponent } from './cell-renderer/edit-renderer/edit-renderer.component';
import { BookmarkEditComponent } from './core/bookmark/common-bookmark/bookmark-edit/bookmark-edit.component';
import { VideoSearchComponent } from './core/search/video-search/video-search.component';
import { VideoThumbComponent } from './core/search/video-search/video-thumb/video-thumb.component';
import { VideoListComponent } from './core/search/video-search/video-list/video-list.component';
import { VideoThumbCardComponent } from './core/search/video-search/video-thumb/video-thumb-card/video-thumb-card.component';
import { PlaylistRendererComponent } from './cell-renderer/playlist-renderer/playlist-renderer.component';
import { LastViewedRendererComponent } from './cell-renderer/last-viewed-renderer/last-viewed-renderer.component';
import { GroupDateRendererComponent } from './cell-renderer/group-date-renderer/group-date-renderer.component';
import { AudioFavoriteComponent } from './core/Favorite/audio-favorite/audio-favorite.component';
import { AudioFavoriteService } from './core/Favorite/audio-favorite/audio-favorite.service';
import { AudioPlaylistComponent } from './core/playlist/audio-playlist/audio-playlist.component';
import { AudioPlaylistDetailsComponent } from './core/playlist/audio-playlist/audio-playlist-details/audio-playlist-details.component';
import { AudioPlaylistItemsComponent } from './core/playlist/audio-playlist/audio-playlist-items/audio-playlist-items.component';
import { AudioPlaylistService } from './core/playlist/audio-playlist/audio-playlist.service';
import { AudioHistoryComponent } from './core/History/audio-history/audio-history.component';
import { AudioHistoryService } from './core/History/audio-history/audio-history.service';
import { DynamicFormComponentComponent } from './core/search/dynamic-form-component/dynamic-form-component.component';
import { DynamicFormComponent } from './core/search/dynamic-form/dynamic-form.component';
import { NgSelectModule } from "@ng-select/ng-select";
import { AudioSearchComponent } from './core/search/audio-search/audio-search.component';
import { AudioListComponent } from './core/search/audio-search/audio-list/audio-list.component';
import { CookieService } from 'ngx-cookie-service';
import { AuthService,AuthGuard,AuthInterceptor } from './login/auth.service';
import { AudioAddtoPlaylistService } from './core/play/audio-player/audio-add-to-playlist/audio-addto-playlist.service';
import { AdminConsoleComponent } from './admin/admin-console/admin-console.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatChipsModule } from '@angular/material/chips';
import { MatInputModule } from '@angular/material/input';
import { MatListModule, MatSelectionList} from '@angular/material/list';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import { AdminHomeComponent } from './admin/admin-home/admin-home/admin-home.component';
import { AdminService } from './admin/admin-service';

import { ListViewModule } from '@syncfusion/ej2-angular-lists';
import { CheckBoxModule } from '@syncfusion/ej2-angular-buttons';
import { TreeViewModule  } from '@syncfusion/ej2-angular-navigations';
import { QuestionService } from './core/search/question.service';

@NgModule({
  declarations: [
    AppComponent,
    CoreComponent,
    BasicComponent,
    LeftPanelComponent,
    RightPanelComponent,
    HeaderComponent,
    UserInfoComponent,
    CoverPageComponent,
    LoginComponent,
    PlayComponent,
    VideoPlayerComponent,
    PlayerComponent,
    TabsComponent,
    HomeComponent,
    RecommendationComponent,
    VideoRecommendationComponent,
    VideoListItemComponent,
    QueueComponent,
    SugeestionsComponent,
    VideoCardComponent,
    VideoPlaylistComponent,
    PlaylistDetailComponent,
    VideoThumbRendererComponent,
    VideoTitleRendererComponent,
    InfoRendererComponent,
    FavoriteRendererComponent,
    DeleteRendererComponent,
    CategoryComponent,
    AudioPlayerComponent,
    BookmarkComponent,
    DetailsViewComponent,
    AudioTabsComponent,
    PlaylistItemsComponent,
    VideoHistoryComponent,
    VideoFavoriteComponent,
    AudioRecommendationComponent,
    RatingRendererComponent,
    AudioQueueComponent,
    AudioBookmarkDetailsComponent,
    AudioBookmarkEditComponent,
    VgPlayPause,
    AudioAddToPlaylistComponent,
    VideoAddToPlaylistComponent,
    CommonBookmarkComponent,
    BookmarkItemsComponent,
    BookmarkDetailsComponent,
    EditRendererComponent,
    BookmarkEditComponent,
    VideoSearchComponent,
    VideoThumbComponent,
    VideoListComponent,
    VideoThumbCardComponent,
    PlaylistRendererComponent,
    LastViewedRendererComponent,
    GroupDateRendererComponent,
    AudioFavoriteComponent,
    AudioPlaylistComponent,
    AudioPlaylistDetailsComponent,
    AudioPlaylistItemsComponent,
    AudioHistoryComponent,
    DynamicFormComponentComponent,
    DynamicFormComponent,
    AudioSearchComponent,
    AudioListComponent,
    AdminConsoleComponent,
    AdminHomeComponent,
  ],
  imports: [
    CheckBoxModule, 
    ListViewModule,
    TreeViewModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule, 
    RxReactiveFormsModule,  
    RxReactiveDynamicFormsModule,
    HttpClientModule,
    ModalModule.forRoot(),
    PopoverModule.forRoot(),
    RatingModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    CollapseModule.forRoot(),
    AngularSvgIconModule.forRoot(),
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    FontAwesomeModule,
    AgGridModule.withComponents([]),
    TabsModule.forRoot(),
    CommonModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      preventDuplicates: true,
      iconClasses : {
        error: 'toast-error',
        info: 'toast-info',
        success: 'toast-success',
        warning: 'toast-warning',
        danger: 'toast-danger',
      }
    }), // ToastrModule added
    NgSelectModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'csrftoken',
      headerName: 'X-CSRF-TOKEN'
    }),
    NgMultiSelectDropDownModule.forRoot(),
    MatAutocompleteModule,
    MatChipsModule,
    MatInputModule,
    MatListModule
  ],
  
  providers: [    
    VideoPlaylistService,
    AudioService,
    VideoHistoryService,
    VideoFavoriteService,
    AudioFavoriteService,
    AudioBookmarkService,
    BookmarkService,
    AudioPlaylistService,
    VideoService,
    AudioAddtoPlaylistService,
    AudioHistoryService,   
    CookieService,
    AuthService,
    AdminService,
    QuestionService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
