import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoreComponent } from './core/core.component';
import { CoverPageComponent } from './cover-page/cover-page.component';
import { LoginComponent } from './login/login.component';
import { VideoPlayerComponent } from './core/play/video-player/video-player.component';
import { VideoRecommendationComponent } from './core/home/recommendation/video-recommendation/video-recommendation.component';
import { VideoPlaylistComponent } from './core/playlist/video-playlist/video-playlist.component';
import { PlaylistDetailComponent } from './core/playlist/video-playlist/playlist-detail/playlist-detail.component';
import { AudioPlayerComponent } from './core/play/audio-player/audio-player.component';
import { VideoHistoryComponent } from './core/History/video-history/video-history.component';
import { VideoFavoriteComponent } from './core/Favorite/video-favorite/video-favorite.component';
import { AudioFavoriteComponent } from './core/Favorite/audio-favorite/audio-favorite.component';
import { DetailsViewComponent } from './core/play/audio-player/details-view/details-view.component';
import { AudioRecommendationComponent } from './core/home/recommendation/audio-recommendation/audio-recommendation.component';
import { CommonBookmarkComponent } from './core/bookmark/common-bookmark/common-bookmark.component';
import { BookmarkDetailsComponent } from './core/bookmark/common-bookmark/bookmark-details/bookmark-details.component';
import { BookmarkEditComponent } from './core/bookmark/common-bookmark/bookmark-edit/bookmark-edit.component';
import { VideoSearchComponent } from './core/search/video-search/video-search.component';
import { BasicComponent } from './core/basic/basic.component';
import { AudioPlaylistComponent } from './core/playlist/audio-playlist/audio-playlist.component';
import { AudioPlaylistDetailsComponent } from './core/playlist/audio-playlist/audio-playlist-details/audio-playlist-details.component';
import { AudioHistoryComponent } from './core/History/audio-history/audio-history.component';
import { AudioSearchComponent } from './core/search/audio-search/audio-search.component';
import { AuthGuard } from './login/auth.service'
import { AdminConsoleComponent } from './admin/admin-console/admin-console.component';
import { AdminHomeComponent } from './admin/admin-home/admin-home/admin-home.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent},
  { path: 'cover-page', component: CoverPageComponent , canActivate: [AuthGuard] },
  {
    path: 'video',
    component: BasicComponent,
    children: [
      { path: '', redirectTo: '/video/home', pathMatch: 'full' },
      { path: 'home',component: VideoRecommendationComponent, canActivate: [AuthGuard]},          
      { path: 'playlist', component: VideoPlaylistComponent, children:[{path:':id',component:PlaylistDetailComponent}], canActivate: [AuthGuard]},
      { path: 'history', component: VideoHistoryComponent, canActivate: [AuthGuard]},
      { path: 'favorite', component: VideoFavoriteComponent, canActivate: [AuthGuard]},
      { path: 'bookmark', 
        component: CommonBookmarkComponent, 
        children:[
          {path:':id', component:BookmarkDetailsComponent, canActivate: [AuthGuard]},
          {path:':id/edit',component: BookmarkEditComponent, canActivate: [AuthGuard]}          
      ], canActivate: [AuthGuard] },
      { path: 'search', component:VideoSearchComponent, canActivate: [AuthGuard] },
      { path: ':id', component: VideoPlayerComponent , canActivate: [AuthGuard] },
  ], canActivate: [AuthGuard] },
  { path:'admin',component:AdminHomeComponent},
  { path:'admin-edit/:id',component:AdminConsoleComponent},
  { path:'admin-add',component:AdminConsoleComponent},
  {
    path: 'audio',
    component: BasicComponent,
    children: [
     // { path: '', redirectTo: '/audio/:id/home', pathMatch: 'full' },
      { path: ':id', component: AudioPlayerComponent,
      children: [
      { path: 'home',component: AudioRecommendationComponent},          
      { path: 'playlist', component: AudioPlaylistComponent, children:[{path:':id',component:AudioPlaylistDetailsComponent}]},
      { path: 'history', component: AudioHistoryComponent},
      { path:'details-view/:id',component:DetailsViewComponent},
      { path: 'favorite', component: AudioFavoriteComponent},
      { path: 'search', component:AudioSearchComponent},
      { path: 'bookmark', 
        component: CommonBookmarkComponent, 
        children:[
          {path:':id', component:BookmarkDetailsComponent},
          {path:':id/edit',component: BookmarkEditComponent}
      ]},
      
    ]}
  ], canActivate: [AuthGuard] },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
