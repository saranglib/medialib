--
-- PostgreSQL database dump
--

-- Dumped from database version 10.15 (Ubuntu 10.15-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.15 (Ubuntu 10.15-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: api_asset; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.api_asset (
    asset_uuid uuid NOT NULL,
    asset_type integer NOT NULL,
    mine_type character varying(150) NOT NULL,
    metadata jsonb NOT NULL,
    item_uuid_id uuid NOT NULL,
    CONSTRAINT api_asset_asset_type_check CHECK ((asset_type >= 0))
);


ALTER TABLE public.api_asset OWNER TO dipak;

--
-- Name: api_bookmark; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.api_bookmark (
    bookmark_uuid uuid NOT NULL,
    bookmark_type integer NOT NULL,
    name text NOT NULL,
    bookmark integer NOT NULL,
    item_uuid_id uuid NOT NULL,
    user_uuid_id uuid NOT NULL
);


ALTER TABLE public.api_bookmark OWNER TO dipak;

--
-- Name: api_device; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.api_device (
    device_uuid uuid NOT NULL,
    site_id integer NOT NULL,
    device_type integer NOT NULL,
    details jsonb NOT NULL,
    resource_path text NOT NULL,
    CONSTRAINT api_device_device_type_f6cc5a22_check CHECK ((device_type >= 0))
);


ALTER TABLE public.api_device OWNER TO dipak;

--
-- Name: api_file; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.api_file (
    file_uuid uuid NOT NULL,
    path character varying(500) NOT NULL,
    filename character varying(255) NOT NULL,
    asset_uuid_id uuid NOT NULL,
    device_uuid_id uuid NOT NULL
);


ALTER TABLE public.api_file OWNER TO dipak;

--
-- Name: api_history; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.api_history (
    history_uuid uuid NOT NULL,
    visited_at timestamp with time zone NOT NULL,
    viewed_time integer NOT NULL,
    view_counted boolean NOT NULL,
    metrics jsonb NOT NULL,
    item_uuid_id uuid NOT NULL,
    user_uuid_id uuid NOT NULL
);


ALTER TABLE public.api_history OWNER TO dipak;

--
-- Name: api_item; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.api_item (
    item_uuid uuid NOT NULL,
    metadata jsonb NOT NULL,
    tags jsonb NOT NULL,
    views integer NOT NULL,
    item_type_id integer NOT NULL
);


ALTER TABLE public.api_item OWNER TO dipak;

--
-- Name: api_item_favourites; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.api_item_favourites (
    id integer NOT NULL,
    item_id uuid NOT NULL,
    userprofile_id uuid NOT NULL
);


ALTER TABLE public.api_item_favourites OWNER TO dipak;

--
-- Name: api_item_favourites_id_seq; Type: SEQUENCE; Schema: public; Owner: dipak
--

CREATE SEQUENCE public.api_item_favourites_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_item_favourites_id_seq OWNER TO dipak;

--
-- Name: api_item_favourites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dipak
--

ALTER SEQUENCE public.api_item_favourites_id_seq OWNED BY public.api_item_favourites.id;


--
-- Name: api_item_likes; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.api_item_likes (
    id integer NOT NULL,
    item_id uuid NOT NULL,
    userprofile_id uuid NOT NULL
);


ALTER TABLE public.api_item_likes OWNER TO dipak;

--
-- Name: api_item_likes_id_seq; Type: SEQUENCE; Schema: public; Owner: dipak
--

CREATE SEQUENCE public.api_item_likes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_item_likes_id_seq OWNER TO dipak;

--
-- Name: api_item_likes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dipak
--

ALTER SEQUENCE public.api_item_likes_id_seq OWNED BY public.api_item_likes.id;


--
-- Name: api_itemtype; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.api_itemtype (
    item_type_id integer NOT NULL,
    item_type character varying(50) NOT NULL,
    CONSTRAINT api_itemtypes_item_type_id_check CHECK ((item_type_id >= 0))
);


ALTER TABLE public.api_itemtype OWNER TO dipak;

--
-- Name: api_list; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.api_list (
    list_uuid uuid NOT NULL,
    list_type integer NOT NULL,
    name text NOT NULL,
    items integer[] NOT NULL,
    user_uuid_id uuid NOT NULL
);


ALTER TABLE public.api_list OWNER TO dipak;

--
-- Name: api_rating; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.api_rating (
    id integer NOT NULL,
    rating numeric(2,1) NOT NULL,
    item_uuid_id uuid NOT NULL,
    user_uuid_id uuid NOT NULL
);


ALTER TABLE public.api_rating OWNER TO dipak;

--
-- Name: api_rating_id_seq; Type: SEQUENCE; Schema: public; Owner: dipak
--

CREATE SEQUENCE public.api_rating_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_rating_id_seq OWNER TO dipak;

--
-- Name: api_rating_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dipak
--

ALTER SEQUENCE public.api_rating_id_seq OWNED BY public.api_rating.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO dipak;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: dipak
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO dipak;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dipak
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO dipak;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: dipak
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO dipak;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dipak
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO dipak;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: dipak
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO dipak;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dipak
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: authtoken_token; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.authtoken_token (
    key character varying(40) NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id uuid NOT NULL
);


ALTER TABLE public.authtoken_token OWNER TO dipak;

--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id uuid NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO dipak;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: dipak
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO dipak;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dipak
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO dipak;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: dipak
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO dipak;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dipak
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO dipak;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: dipak
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO dipak;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dipak
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO dipak;

--
-- Name: profile_app_userprofile; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.profile_app_userprofile (
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    user_uuid uuid NOT NULL,
    email character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    is_active boolean NOT NULL,
    is_staff boolean NOT NULL
);


ALTER TABLE public.profile_app_userprofile OWNER TO dipak;

--
-- Name: profile_app_userprofile_groups; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.profile_app_userprofile_groups (
    id integer NOT NULL,
    userprofile_id uuid NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.profile_app_userprofile_groups OWNER TO dipak;

--
-- Name: profile_app_userprofile_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: dipak
--

CREATE SEQUENCE public.profile_app_userprofile_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profile_app_userprofile_groups_id_seq OWNER TO dipak;

--
-- Name: profile_app_userprofile_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dipak
--

ALTER SEQUENCE public.profile_app_userprofile_groups_id_seq OWNED BY public.profile_app_userprofile_groups.id;


--
-- Name: profile_app_userprofile_user_permissions; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.profile_app_userprofile_user_permissions (
    id integer NOT NULL,
    userprofile_id uuid NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.profile_app_userprofile_user_permissions OWNER TO dipak;

--
-- Name: profile_app_userprofile_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: dipak
--

CREATE SEQUENCE public.profile_app_userprofile_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profile_app_userprofile_user_permissions_id_seq OWNER TO dipak;

--
-- Name: profile_app_userprofile_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dipak
--

ALTER SEQUENCE public.profile_app_userprofile_user_permissions_id_seq OWNED BY public.profile_app_userprofile_user_permissions.id;


--
-- Name: tutorials_tutorial; Type: TABLE; Schema: public; Owner: dipak
--

CREATE TABLE public.tutorials_tutorial (
    id integer NOT NULL,
    title character varying(70) NOT NULL,
    description character varying(200) NOT NULL,
    published boolean NOT NULL
);


ALTER TABLE public.tutorials_tutorial OWNER TO dipak;

--
-- Name: tutorials_tutorial_id_seq; Type: SEQUENCE; Schema: public; Owner: dipak
--

CREATE SEQUENCE public.tutorials_tutorial_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tutorials_tutorial_id_seq OWNER TO dipak;

--
-- Name: tutorials_tutorial_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dipak
--

ALTER SEQUENCE public.tutorials_tutorial_id_seq OWNED BY public.tutorials_tutorial.id;


--
-- Name: api_item_favourites id; Type: DEFAULT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_item_favourites ALTER COLUMN id SET DEFAULT nextval('public.api_item_favourites_id_seq'::regclass);


--
-- Name: api_item_likes id; Type: DEFAULT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_item_likes ALTER COLUMN id SET DEFAULT nextval('public.api_item_likes_id_seq'::regclass);


--
-- Name: api_rating id; Type: DEFAULT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_rating ALTER COLUMN id SET DEFAULT nextval('public.api_rating_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: profile_app_userprofile_groups id; Type: DEFAULT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.profile_app_userprofile_groups ALTER COLUMN id SET DEFAULT nextval('public.profile_app_userprofile_groups_id_seq'::regclass);


--
-- Name: profile_app_userprofile_user_permissions id; Type: DEFAULT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.profile_app_userprofile_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.profile_app_userprofile_user_permissions_id_seq'::regclass);


--
-- Name: tutorials_tutorial id; Type: DEFAULT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.tutorials_tutorial ALTER COLUMN id SET DEFAULT nextval('public.tutorials_tutorial_id_seq'::regclass);


--
-- Data for Name: api_asset; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.api_asset (asset_uuid, asset_type, mine_type, metadata, item_uuid_id) FROM stdin;
580ba285-1221-4c1b-a30e-74d28e6288d0	200	{"test": "test"}	{"test": "test"}	3c1e5a03-cab1-46c5-8c71-a356050942f1
82c13751-22d6-4d93-842c-06fb06bd1f26	100	{"test": "test"}	{"test": "test"}	c49a4b5f-f0c7-42a2-b857-e2116fe00fac
aa6aa20c-f5ce-4410-827f-8f9dc91098bf	300	{"test":"test"}	{"test": "test"}	5e9932f4-d3aa-4ddc-a280-f075dcb36964
0ba44b44-f50e-47d3-add1-079a24565522	301	{"test":"test"}	{"test": "test"}	5e9932f4-d3aa-4ddc-a280-f075dcb36964
66371883-5e62-46d3-b4fa-5c17deabbdb8	200	{"test":"test"}	{"test": "test"}	f9031a0b-e059-4570-af45-10c072f552c9
69310df8-e880-456f-acaf-aea1ff949dc7	201	{"test":"test"}	{"test": "test"}	f9031a0b-e059-4570-af45-10c072f552c9
52a2eff8-abf6-4f90-91af-36af7edad158	202	{"test":"test"}	{"test": "test"}	f9031a0b-e059-4570-af45-10c072f552c9
481bd339-61cc-48ff-bc44-be4ab94dee3b	100	{"test":"test"}	{"test": "test"}	c49a4b5f-f0c7-42a2-b857-e2116fe00fac
c0b0f337-eb72-4cd2-bce3-5a9803add8c8	101	{"test":"test"}	{"test": "test"}	c49a4b5f-f0c7-42a2-b857-e2116fe00fac
1370b41e-d7c1-423a-8bd6-4d7a2cf796f4	203	{"test":"test"}	{"test": "test"}	c49a4b5f-f0c7-42a2-b857-e2116fe00fac
e131db14-0321-4ee9-b59a-e58010859da7	201	{"test":"test"}	{"test": "test"}	3c1e5a03-cab1-46c5-8c71-a356050942f1
97c63b69-a668-4673-bde7-53fecab86593	202	{"test":"test"}	{"test": "test"}	3c1e5a03-cab1-46c5-8c71-a356050942f1
c7630a04-99cf-44e5-bbe6-072646242355	100	{"test":"test"}	{"test": "test"}	e9ae57a2-8c7d-41ab-be1f-d5af084acd04
7056907d-efd6-4749-b8a9-d6c32c50235f	101	{"test":"test"}	{"test": "test"}	e9ae57a2-8c7d-41ab-be1f-d5af084acd04
df680a63-78d4-4af5-91b2-f225c7a672de	100	{"test":"test"}	{"test": "test"}	4a4801c5-0114-48b3-a757-852fd608e63d
7c327cf7-73a4-4603-8d1c-6d0efc90a138	101	{"test":"test"}	{"test": "test"}	4a4801c5-0114-48b3-a757-852fd608e63d
75a5af36-9c0c-467a-b241-6c180a38f84b	100	{"test":"test"}	{"test": "test"}	ef644edc-c566-46d0-9894-441b9e6b4af7
0bab833f-d959-483b-887c-6db69af76e40	101	{"test":"test"}	{"test": "test"}	ef644edc-c566-46d0-9894-441b9e6b4af7
3e2af4b1-3e77-4964-be3a-885842f2bfe2	203	{"test":"test"}	{"test": "test"}	ef644edc-c566-46d0-9894-441b9e6b4af7
0d452712-26a1-4730-83b1-2a95714b59eb	200	{"test": "test"}	{"test": "test"}	71cb76c1-5452-4d77-9940-a22106f04fe1
c5ecc274-7aa7-4d5c-b3a0-4828fe585e6a	300	{"test":"test"}	{"test": "test"}	71cb76c1-5452-4d77-9940-a22106f04fe1
12724671-cbbd-4e76-a1b8-2f9f948542cd	301	{"test":"test"}	{"test": "test"}	71cb76c1-5452-4d77-9940-a22106f04fe1
\.


--
-- Data for Name: api_bookmark; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.api_bookmark (bookmark_uuid, bookmark_type, name, bookmark, item_uuid_id, user_uuid_id) FROM stdin;
16e3c0e7-000a-4b8b-9128-e29ed90ab669	2	b1	1	3c1e5a03-cab1-46c5-8c71-a356050942f1	0a8462e9-7414-470d-bd7e-18b2235ebd0b
d05c65c8-0ff8-42d4-9b30-4d30203b24d6	13	b2	23	e9ae57a2-8c7d-41ab-be1f-d5af084acd04	4c423eb7-9319-4093-801c-31860cd01d4a
\.


--
-- Data for Name: api_device; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.api_device (device_uuid, site_id, device_type, details, resource_path) FROM stdin;
e17f3104-eebb-4f67-9646-eef2ed8b2e4e	1	1	{"test": "test"}	device1.txt
fce9cbf8-bcc4-4e24-9280-9b57d949c6c6	2	2	{"test": "test"}	device2.txt
\.


--
-- Data for Name: api_file; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.api_file (file_uuid, path, filename, asset_uuid_id, device_uuid_id) FROM stdin;
c8fb64e5-d0e6-4912-8c9b-8fa91abb1314	data	file.txt	82c13751-22d6-4d93-842c-06fb06bd1f26	e17f3104-eebb-4f67-9646-eef2ed8b2e4e
63e3163b-6772-415e-8300-097a0b120f1a	data	file.mp3	12724671-cbbd-4e76-a1b8-2f9f948542cd	e17f3104-eebb-4f67-9646-eef2ed8b2e4e
f857af22-d0a3-4519-a3fb-ce8e2b177598	data	test.mp3	c5ecc274-7aa7-4d5c-b3a0-4828fe585e6a	e17f3104-eebb-4f67-9646-eef2ed8b2e4e
770c4d90-ad63-4955-a576-3cc105c872bd	data	audio.wav	aa6aa20c-f5ce-4410-827f-8f9dc91098bf	fce9cbf8-bcc4-4e24-9280-9b57d949c6c6
f025ca10-cb9c-4d76-978b-c904ed657b15	data	audio.mp3	0ba44b44-f50e-47d3-add1-079a24565522	e17f3104-eebb-4f67-9646-eef2ed8b2e4e
fd252c87-561a-473f-8bb2-4fcfd1628ee0	data	image.png	66371883-5e62-46d3-b4fa-5c17deabbdb8	e17f3104-eebb-4f67-9646-eef2ed8b2e4e
a47499cb-748c-4141-aa8e-55c92791f7bf	data	test.jpeg	69310df8-e880-456f-acaf-aea1ff949dc7	fce9cbf8-bcc4-4e24-9280-9b57d949c6c6
36baca2b-e56e-4037-8c7f-1f988f7c12f9	data	preview.jpeg	52a2eff8-abf6-4f90-91af-36af7edad158	e17f3104-eebb-4f67-9646-eef2ed8b2e4e
54dda88a-ed7a-4496-a02d-9be280611772	data	video.mp3	481bd339-61cc-48ff-bc44-be4ab94dee3b	e17f3104-eebb-4f67-9646-eef2ed8b2e4e
b592ff98-364a-493e-a615-9de33b95db0e	data	video_thumbnail.mp3	c0b0f337-eb72-4cd2-bce3-5a9803add8c8	fce9cbf8-bcc4-4e24-9280-9b57d949c6c6
a0df1063-7bb8-463d-b65e-8793e11c1fa7	data	video_sprite.mp3	1370b41e-d7c1-423a-8bd6-4d7a2cf796f4	e17f3104-eebb-4f67-9646-eef2ed8b2e4e
c55d728f-0c81-4520-9729-ee127cdcc082	data	thumbnail.jpeg	e131db14-0321-4ee9-b59a-e58010859da7	e17f3104-eebb-4f67-9646-eef2ed8b2e4e
ca73ce3d-e1e7-4e63-aa5d-c2de84e584fb	data	image_preview.jpeg	97c63b69-a668-4673-bde7-53fecab86593	e17f3104-eebb-4f67-9646-eef2ed8b2e4e
382dd5d1-3143-4982-a9a1-bde0db417ae2	data	test.mp4	c7630a04-99cf-44e5-bbe6-072646242355	fce9cbf8-bcc4-4e24-9280-9b57d949c6c6
03ef2ee7-95fe-4fc8-b0f2-750b0737ad1d	data	preview.mp4	7056907d-efd6-4749-b8a9-d6c32c50235f	e17f3104-eebb-4f67-9646-eef2ed8b2e4e
29cebf73-e34b-4f6e-8898-bb8200d2b6cd	data	abc.mp3	df680a63-78d4-4af5-91b2-f225c7a672de	e17f3104-eebb-4f67-9646-eef2ed8b2e4e
091464f6-bc52-44f2-bbe8-e84889340e16	data	xyz.mp3	7c327cf7-73a4-4603-8d1c-6d0efc90a138	fce9cbf8-bcc4-4e24-9280-9b57d949c6c6
6780d9af-40dc-4e55-b3c3-3007e1b6b961	data	abc.mp4	75a5af36-9c0c-467a-b241-6c180a38f84b	e17f3104-eebb-4f67-9646-eef2ed8b2e4e
936b1fd2-5897-4f34-9ba6-2dfc967b54c4	data	pqr.mp4	0bab833f-d959-483b-887c-6db69af76e40	e17f3104-eebb-4f67-9646-eef2ed8b2e4e
30a1d584-5de4-42c5-9557-b03afdfd735f	data	xyz.mp4	3e2af4b1-3e77-4964-be3a-885842f2bfe2	e17f3104-eebb-4f67-9646-eef2ed8b2e4e
4f507239-237e-4a87-a857-9f761e5c7b80	data	file.mp3	82c13751-22d6-4d93-842c-06fb06bd1f26	fce9cbf8-bcc4-4e24-9280-9b57d949c6c6
\.


--
-- Data for Name: api_history; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.api_history (history_uuid, visited_at, viewed_time, view_counted, metrics, item_uuid_id, user_uuid_id) FROM stdin;
9af4df2e-7563-44d3-bcb4-aa30752c2e80	2020-12-12 18:28:14+05:30	342	t	{"test": "test"}	c49a4b5f-f0c7-42a2-b857-e2116fe00fac	0a8462e9-7414-470d-bd7e-18b2235ebd0b
96a972d3-781c-4dd4-a89d-10affcfa04e8	2020-12-12 18:28:30+05:30	332	f	{"test": "test"}	3c1e5a03-cab1-46c5-8c71-a356050942f1	0a8462e9-7414-470d-bd7e-18b2235ebd0b
a7856f03-0be8-45c8-8a71-b174fa9b4a73	2020-12-12 18:28:14+05:30	32	t	{"test": "test"}	c49a4b5f-f0c7-42a2-b857-e2116fe00fac	0a8462e9-7414-470d-bd7e-18b2235ebd0b
\.


--
-- Data for Name: api_item; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.api_item (item_uuid, metadata, tags, views, item_type_id) FROM stdin;
71cb76c1-5452-4d77-9940-a22106f04fe1	{"album": "Aaj mare orde", "title": "Aaj mare orde", "artist": "Jaydeepbhai Swadiya"}	{"tag": [{"tag_id": 1, "user_id": "123e4567-e89b-12d3-a456-426614174090", "tag_name": "test_tag", "tag_type": "1", "timestamp": ""}], "event": [{"user_id": "123e4567-e89b-12d3-a456-426614174090", "event_id": 1, "timestamp": "", "event_name": "Pratishtha", "event_type": "1"}], "location": [{"user_id": "123e4567-e89b-12d3-a456-426614174090", "timestamp": "", "location_id": 1, "location_name": "Sarangpur", "location_type": "2"}]}	5	300
ef644edc-c566-46d0-9894-441b9e6b4af7	{"album": "Aaj mare orde", "artist": "Jaydeepbhai Swadiya"}	{"tag": [{"tag_id": 1, "user_id": "123e4567-e89b-12d3-a456-426614174090", "tag_name": "test_tag", "tag_type": "1", "timestamp": ""}], "event": [{"user_id": "123e4567-e89b-12d3-a456-426614174090", "event_id": 1, "timestamp": "", "event_name": "Pratishtha", "event_type": "1"}], "location": [{"user_id": "123e4567-e89b-12d3-a456-426614174090", "timestamp": "", "location_id": 1, "location_name": "Sarangpur", "location_type": "2"}]}	5	100
e9ae57a2-8c7d-41ab-be1f-d5af084acd04	{"test": "test"}	{"test": "test"}	31	100
cc8316cf-308b-4734-847d-2b30694f4636	{"album": "Aaj mare orde", "artist": "Jaydeepbhai Swadiya"}	{"tag": [{"tag_id": 1, "user_id": "123e4567-e89b-12d3-a456-426614174090", "tag_name": "test_tag", "tag_type": "1", "timestamp": ""}], "event": [{"user_id": "123e4567-e89b-12d3-a456-426614174090", "event_id": 1, "timestamp": "", "event_name": "Pratishtha", "event_type": "1"}], "location": [{"user_id": "123e4567-e89b-12d3-a456-426614174090", "timestamp": "", "location_id": 1, "location_name": "Sarangpur", "location_type": "2"}]}	5	100
f9031a0b-e059-4570-af45-10c072f552c9	{"album": "Aaj mare orde", "artist": "Jaydeepbhai Swadiya"}	{"tag": [{"tag_id": 1, "user_id": "123e4567-e89b-12d3-a456-426614174090", "tag_name": "test_tag", "tag_type": "1", "timestamp": ""}], "event": [{"user_id": "123e4567-e89b-12d3-a456-426614174090", "event_id": 1, "timestamp": "", "event_name": "Pratishtha", "event_type": "1"}], "location": [{"user_id": "123e4567-e89b-12d3-a456-426614174090", "timestamp": "", "location_id": 1, "location_name": "Sarangpur", "location_type": "2"}]}	5	200
c49a4b5f-f0c7-42a2-b857-e2116fe00fac	{"test": "test"}	{"test": "test"}	34	100
3c1e5a03-cab1-46c5-8c71-a356050942f1	{"test": "test"}	{"test": "test"}	422	200
4a4801c5-0114-48b3-a757-852fd608e63d	{"album": "Aaj mare orde", "artist": "Jaydeepbhai Swadiya"}	{"tag": [{"tag_id": 1, "user_id": "123e4567-e89b-12d3-a456-426614174090", "tag_name": "test_tag", "tag_type": "1", "timestamp": ""}], "event": [{"user_id": "123e4567-e89b-12d3-a456-426614174090", "event_id": 1, "timestamp": "", "event_name": "Pratishtha", "event_type": "1"}], "location": [{"user_id": "123e4567-e89b-12d3-a456-426614174090", "timestamp": "", "location_id": 1, "location_name": "Sarangpur", "location_type": "2"}]}	5	100
5e9932f4-d3aa-4ddc-a280-f075dcb36964	{"album": "Aaj mare orde", "artist": "Jaydeepbhai Swadiya", "duration": "40"}	{"tag": [{"tag_id": 1, "user_id": "123e4567-e89b-12d3-a456-426614174090", "tag_name": "test_tag", "tag_type": "1", "timestamp": ""}], "event": [{"user_id": "123e4567-e89b-12d3-a456-426614174090", "event_id": 1, "timestamp": "", "event_name": "Pratishtha", "event_type": "1"}], "location": [{"user_id": "123e4567-e89b-12d3-a456-426614174090", "timestamp": "", "location_id": 1, "location_name": "Sarangpur", "location_type": "2"}]}	5	300
\.


--
-- Data for Name: api_item_favourites; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.api_item_favourites (id, item_id, userprofile_id) FROM stdin;
1	c49a4b5f-f0c7-42a2-b857-e2116fe00fac	426efbcd-99b1-4870-8178-39bd4c22e66c
2	3c1e5a03-cab1-46c5-8c71-a356050942f1	426efbcd-99b1-4870-8178-39bd4c22e66c
3	71cb76c1-5452-4d77-9940-a22106f04fe1	4c423eb7-9319-4093-801c-31860cd01d4a
4	71cb76c1-5452-4d77-9940-a22106f04fe1	0a8462e9-7414-470d-bd7e-18b2235ebd0b
\.


--
-- Data for Name: api_item_likes; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.api_item_likes (id, item_id, userprofile_id) FROM stdin;
1	c49a4b5f-f0c7-42a2-b857-e2116fe00fac	0a8462e9-7414-470d-bd7e-18b2235ebd0b
2	3c1e5a03-cab1-46c5-8c71-a356050942f1	426efbcd-99b1-4870-8178-39bd4c22e66c
3	71cb76c1-5452-4d77-9940-a22106f04fe1	426efbcd-99b1-4870-8178-39bd4c22e66c
14	71cb76c1-5452-4d77-9940-a22106f04fe1	0a8462e9-7414-470d-bd7e-18b2235ebd0b
\.


--
-- Data for Name: api_itemtype; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.api_itemtype (item_type_id, item_type) FROM stdin;
300	audio
200	image
100	video
\.


--
-- Data for Name: api_list; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.api_list (list_uuid, list_type, name, items, user_uuid_id) FROM stdin;
42f050f7-b198-4ae0-acfe-b390868958d2	23	l1	{2,4,2,4,2,4}	426efbcd-99b1-4870-8178-39bd4c22e66c
fd613856-e5aa-43f4-86dc-50f0d8890c19	31	l2	{232,42,4,12,2}	0a8462e9-7414-470d-bd7e-18b2235ebd0b
\.


--
-- Data for Name: api_rating; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.api_rating (id, rating, item_uuid_id, user_uuid_id) FROM stdin;
1	5.0	e9ae57a2-8c7d-41ab-be1f-d5af084acd04	426efbcd-99b1-4870-8178-39bd4c22e66c
2	2.0	e9ae57a2-8c7d-41ab-be1f-d5af084acd04	0a8462e9-7414-470d-bd7e-18b2235ebd0b
3	4.0	c49a4b5f-f0c7-42a2-b857-e2116fe00fac	0a8462e9-7414-470d-bd7e-18b2235ebd0b
4	4.0	c49a4b5f-f0c7-42a2-b857-e2116fe00fac	426efbcd-99b1-4870-8178-39bd4c22e66c
5	5.0	71cb76c1-5452-4d77-9940-a22106f04fe1	4c423eb7-9319-4093-801c-31860cd01d4a
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add content type	4	add_contenttype
14	Can change content type	4	change_contenttype
15	Can delete content type	4	delete_contenttype
16	Can view content type	4	view_contenttype
17	Can add session	5	add_session
18	Can change session	5	change_session
19	Can delete session	5	delete_session
20	Can view session	5	view_session
21	Can add Token	6	add_token
22	Can change Token	6	change_token
23	Can delete Token	6	delete_token
24	Can view Token	6	view_token
25	Can add tutorial	7	add_tutorial
26	Can change tutorial	7	change_tutorial
27	Can delete tutorial	7	delete_tutorial
28	Can view tutorial	7	view_tutorial
29	Can add user profile	8	add_userprofile
30	Can change user profile	8	change_userprofile
31	Can delete user profile	8	delete_userprofile
32	Can view user profile	8	view_userprofile
33	Can add item	9	add_item
34	Can change item	9	change_item
35	Can delete item	9	delete_item
36	Can view item	9	view_item
37	Can add rating	10	add_rating
38	Can change rating	10	change_rating
39	Can delete rating	10	delete_rating
40	Can view rating	10	view_rating
41	Can add list	11	add_list
42	Can change list	11	change_list
43	Can delete list	11	delete_list
44	Can view list	11	view_list
45	Can add history	12	add_history
46	Can change history	12	change_history
47	Can delete history	12	delete_history
48	Can view history	12	view_history
49	Can add bookmark	13	add_bookmark
50	Can change bookmark	13	change_bookmark
51	Can delete bookmark	13	delete_bookmark
52	Can view bookmark	13	view_bookmark
53	Can add item	14	add_item
54	Can change item	14	change_item
55	Can delete item	14	delete_item
56	Can view item	14	view_item
57	Can add list	15	add_list
58	Can change list	15	change_list
59	Can delete list	15	delete_list
60	Can view list	15	view_list
61	Can add history	16	add_history
62	Can change history	16	change_history
63	Can delete history	16	delete_history
64	Can view history	16	view_history
65	Can add bookmark	17	add_bookmark
66	Can change bookmark	17	change_bookmark
67	Can delete bookmark	17	delete_bookmark
68	Can view bookmark	17	view_bookmark
69	Can add rating	18	add_rating
70	Can change rating	18	change_rating
71	Can delete rating	18	delete_rating
72	Can view rating	18	view_rating
73	Can add file	19	add_file
74	Can change file	19	change_file
75	Can delete file	19	delete_file
76	Can view file	19	view_file
77	Can add device	20	add_device
78	Can change device	20	change_device
79	Can delete device	20	delete_device
80	Can view device	20	view_device
81	Can add asset	21	add_asset
82	Can change asset	21	change_asset
83	Can delete asset	21	delete_asset
84	Can view asset	21	view_asset
85	Can add user	22	add_user
86	Can change user	22	change_user
87	Can delete user	22	delete_user
88	Can view user	22	view_user
89	Can add item types	23	add_itemtypes
90	Can change item types	23	change_itemtypes
91	Can delete item types	23	delete_itemtypes
92	Can view item types	23	view_itemtypes
93	Can add item type	23	add_itemtype
94	Can change item type	23	change_itemtype
95	Can delete item type	23	delete_itemtype
96	Can view item type	23	view_itemtype
\.


--
-- Data for Name: authtoken_token; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.authtoken_token (key, created, user_id) FROM stdin;
17d096a900afd064ef5a97318b19c2032d48d8d2	2020-12-09 18:17:50.816729+05:30	0a8462e9-7414-470d-bd7e-18b2235ebd0b
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2020-12-09 16:00:27.851449+05:30	a8cbad69-4ad6-45a9-b905-bba638018764	a8cbad69-4ad6-45a9-b905-bba638018764	1	[{"added": {}}]	9	0a8462e9-7414-470d-bd7e-18b2235ebd0b
2	2020-12-09 16:02:00.065262+05:30	1	test@gmail.com	1	[{"added": {}}]	10	0a8462e9-7414-470d-bd7e-18b2235ebd0b
3	2020-12-09 18:13:29.655531+05:30	a8cbad69-4ad6-45a9-b905-bba638018764	a8cbad69-4ad6-45a9-b905-bba638018764	3		9	0a8462e9-7414-470d-bd7e-18b2235ebd0b
4	2020-12-09 18:13:51.785464+05:30	d399ec56-90df-4f79-b950-21703b870ed2	d399ec56-90df-4f79-b950-21703b870ed2	1	[{"added": {}}]	9	0a8462e9-7414-470d-bd7e-18b2235ebd0b
5	2020-12-09 18:17:47.363221+05:30	6dc892ea41fdc2ed57a62f2a9b489520c26a8828	6dc892ea41fdc2ed57a62f2a9b489520c26a8828	3		6	0a8462e9-7414-470d-bd7e-18b2235ebd0b
6	2020-12-12 13:12:02.251663+05:30	4b4d05ec-eb91-492b-9a0b-170ba17fcbbf	4b4d05ec-eb91-492b-9a0b-170ba17fcbbf	1	[{"added": {}}]	12	0a8462e9-7414-470d-bd7e-18b2235ebd0b
7	2020-12-12 13:12:44.727159+05:30	a9789c04-0c0e-47b4-b063-546dc038a319	a9789c04-0c0e-47b4-b063-546dc038a319	1	[{"added": {}}]	9	0a8462e9-7414-470d-bd7e-18b2235ebd0b
8	2020-12-12 13:16:31.520456+05:30	4c423eb7-9319-4093-801c-31860cd01d4a	u2@gmail.com	1	[{"added": {}}]	8	0a8462e9-7414-470d-bd7e-18b2235ebd0b
9	2020-12-12 13:18:31.300852+05:30	04c69d75-c314-458b-bb27-dfbf9b86c639	04c69d75-c314-458b-bb27-dfbf9b86c639	1	[{"added": {}}]	9	0a8462e9-7414-470d-bd7e-18b2235ebd0b
10	2020-12-12 13:21:56.710564+05:30	eef9b725-a048-4dc0-b350-cd3977c9429b	eef9b725-a048-4dc0-b350-cd3977c9429b	1	[{"added": {}}]	11	0a8462e9-7414-470d-bd7e-18b2235ebd0b
11	2020-12-12 13:22:38.348416+05:30	eef9b725-a048-4dc0-b350-cd3977c9429a	eef9b725-a048-4dc0-b350-cd3977c9429a	1	[{"added": {}}]	11	0a8462e9-7414-470d-bd7e-18b2235ebd0b
12	2020-12-12 13:22:58.887728+05:30	2	u1@gmail.com	1	[{"added": {}}]	10	0a8462e9-7414-470d-bd7e-18b2235ebd0b
13	2020-12-12 13:23:06.323567+05:30	3	u1@gmail.com	1	[{"added": {}}]	10	0a8462e9-7414-470d-bd7e-18b2235ebd0b
14	2020-12-12 13:23:14.933932+05:30	4	test@gmail.com	1	[{"added": {}}]	10	0a8462e9-7414-470d-bd7e-18b2235ebd0b
15	2020-12-12 13:23:23.202704+05:30	5	u2@gmail.com	1	[{"added": {}}]	10	0a8462e9-7414-470d-bd7e-18b2235ebd0b
16	2020-12-12 13:24:01.640764+05:30	3362755c-0cfa-4bc5-9d71-f98c9dc4a737	3362755c-0cfa-4bc5-9d71-f98c9dc4a737	1	[{"added": {}}]	13	0a8462e9-7414-470d-bd7e-18b2235ebd0b
17	2020-12-12 13:28:02.955529+05:30	97e78511-e1c8-427a-b377-3dc5fe2fea4d	97e78511-e1c8-427a-b377-3dc5fe2fea4d	1	[{"added": {}}]	13	0a8462e9-7414-470d-bd7e-18b2235ebd0b
18	2020-12-12 13:28:14.81693+05:30	4494ed99-bc43-4212-abd9-900ab729a953	4494ed99-bc43-4212-abd9-900ab729a953	1	[{"added": {}}]	13	0a8462e9-7414-470d-bd7e-18b2235ebd0b
19	2020-12-12 13:28:31.284202+05:30	a0ad70a1-6eb2-4cd3-8759-91765852c9c9	a0ad70a1-6eb2-4cd3-8759-91765852c9c9	1	[{"added": {}}]	13	0a8462e9-7414-470d-bd7e-18b2235ebd0b
20	2020-12-12 17:51:54.83213+05:30	5	u2@gmail.com	2	[{"changed": {"fields": ["Item uuid"]}}]	10	0a8462e9-7414-470d-bd7e-18b2235ebd0b
21	2020-12-12 17:52:04.029743+05:30	3	u1@gmail.com	2	[{"changed": {"fields": ["Item uuid"]}}]	10	0a8462e9-7414-470d-bd7e-18b2235ebd0b
22	2020-12-12 18:27:25.874277+05:30	c49a4b5f-f0c7-42a2-b857-e2116fe00fac	c49a4b5f-f0c7-42a2-b857-e2116fe00fac	1	[{"added": {}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
23	2020-12-12 18:27:47.277374+05:30	3c1e5a03-cab1-46c5-8c71-a356050942f1	3c1e5a03-cab1-46c5-8c71-a356050942f1	1	[{"added": {}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
24	2020-12-12 18:27:59.61602+05:30	e9ae57a2-8c7d-41ab-be1f-d5af084acd04	e9ae57a2-8c7d-41ab-be1f-d5af084acd04	1	[{"added": {}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
25	2020-12-12 18:28:22.71442+05:30	9af4df2e-7563-44d3-bcb4-aa30752c2e80	9af4df2e-7563-44d3-bcb4-aa30752c2e80	1	[{"added": {}}]	16	0a8462e9-7414-470d-bd7e-18b2235ebd0b
26	2020-12-12 18:28:38.218512+05:30	96a972d3-781c-4dd4-a89d-10affcfa04e8	96a972d3-781c-4dd4-a89d-10affcfa04e8	1	[{"added": {}}]	16	0a8462e9-7414-470d-bd7e-18b2235ebd0b
27	2020-12-12 18:29:03.388049+05:30	16e3c0e7-000a-4b8b-9128-e29ed90ab669	16e3c0e7-000a-4b8b-9128-e29ed90ab669	1	[{"added": {}}]	17	0a8462e9-7414-470d-bd7e-18b2235ebd0b
28	2020-12-12 18:29:22.234162+05:30	d05c65c8-0ff8-42d4-9b30-4d30203b24d6	d05c65c8-0ff8-42d4-9b30-4d30203b24d6	1	[{"added": {}}]	17	0a8462e9-7414-470d-bd7e-18b2235ebd0b
29	2020-12-12 18:29:43.190222+05:30	42f050f7-b198-4ae0-acfe-b390868958d2	42f050f7-b198-4ae0-acfe-b390868958d2	1	[{"added": {}}]	15	0a8462e9-7414-470d-bd7e-18b2235ebd0b
30	2020-12-12 18:29:57.694182+05:30	fd613856-e5aa-43f4-86dc-50f0d8890c19	fd613856-e5aa-43f4-86dc-50f0d8890c19	1	[{"added": {}}]	15	0a8462e9-7414-470d-bd7e-18b2235ebd0b
31	2020-12-12 18:30:12.037995+05:30	1	u1@gmail.com	1	[{"added": {}}]	18	0a8462e9-7414-470d-bd7e-18b2235ebd0b
32	2020-12-12 18:30:20.689624+05:30	2	test@gmail.com	1	[{"added": {}}]	18	0a8462e9-7414-470d-bd7e-18b2235ebd0b
33	2020-12-12 18:30:29.169162+05:30	3	test@gmail.com	1	[{"added": {}}]	18	0a8462e9-7414-470d-bd7e-18b2235ebd0b
34	2020-12-12 18:30:40.580549+05:30	4	u1@gmail.com	1	[{"added": {}}]	18	0a8462e9-7414-470d-bd7e-18b2235ebd0b
35	2020-12-24 19:49:01.127215+05:30	82c13751-22d6-4d93-842c-06fb06bd1f26	Asset object (82c13751-22d6-4d93-842c-06fb06bd1f26)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
36	2020-12-24 19:49:18.96352+05:30	580ba285-1221-4c1b-a30e-74d28e6288d0	Asset object (580ba285-1221-4c1b-a30e-74d28e6288d0)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
37	2020-12-24 19:52:19.531756+05:30	e17f3104-eebb-4f67-9646-eef2ed8b2e4e	Device object (e17f3104-eebb-4f67-9646-eef2ed8b2e4e)	1	[{"added": {}}]	20	0a8462e9-7414-470d-bd7e-18b2235ebd0b
38	2020-12-24 19:52:43.176403+05:30	fce9cbf8-bcc4-4e24-9280-9b57d949c6c6	Device object (fce9cbf8-bcc4-4e24-9280-9b57d949c6c6)	1	[{"added": {}}]	20	0a8462e9-7414-470d-bd7e-18b2235ebd0b
39	2020-12-24 19:52:57.602842+05:30	e17f3104-eebb-4f67-9646-eef2ed8b2e4e	Device object (e17f3104-eebb-4f67-9646-eef2ed8b2e4e)	2	[{"changed": {"fields": ["Resource path"]}}]	20	0a8462e9-7414-470d-bd7e-18b2235ebd0b
40	2020-12-24 19:53:04.818199+05:30	fce9cbf8-bcc4-4e24-9280-9b57d949c6c6	Device object (fce9cbf8-bcc4-4e24-9280-9b57d949c6c6)	2	[{"changed": {"fields": ["Device type"]}}]	20	0a8462e9-7414-470d-bd7e-18b2235ebd0b
41	2020-12-24 19:53:56.222031+05:30	c8fb64e5-d0e6-4912-8c9b-8fa91abb1314	File object (c8fb64e5-d0e6-4912-8c9b-8fa91abb1314)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
42	2020-12-24 19:54:15.5968+05:30	4f507239-237e-4a87-a857-9f761e5c7b80	File object (4f507239-237e-4a87-a857-9f761e5c7b80)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
43	2020-12-24 19:54:30.728782+05:30	0f677ed0-e2a7-434e-8ae0-03e6e5154fff	File object (0f677ed0-e2a7-434e-8ae0-03e6e5154fff)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
44	2020-12-25 15:28:06.928506+05:30	71cb76c1-5452-4d77-9940-a22106f04fe1	71cb76c1-5452-4d77-9940-a22106f04fe1	2	[{"changed": {"fields": ["Metadata"]}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
45	2020-12-25 15:29:10.378135+05:30	71cb76c1-5452-4d77-9940-a22106f04fe1	71cb76c1-5452-4d77-9940-a22106f04fe1	2	[{"changed": {"fields": ["Likes", "Favourites"]}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
46	2020-12-25 15:29:41.612139+05:30	71cb76c1-5452-4d77-9940-a22106f04fe1	71cb76c1-5452-4d77-9940-a22106f04fe1	2	[{"changed": {"fields": ["Favourites"]}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
47	2020-12-25 15:30:28.639368+05:30	12724671-cbbd-4e76-a1b8-2f9f948542cd	Asset object (12724671-cbbd-4e76-a1b8-2f9f948542cd)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
48	2020-12-25 15:31:44.493596+05:30	63e3163b-6772-415e-8300-097a0b120f1a	File object (63e3163b-6772-415e-8300-097a0b120f1a)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
49	2020-12-25 15:42:03.776082+05:30	5	u2@gmail.com	1	[{"added": {}}]	18	0a8462e9-7414-470d-bd7e-18b2235ebd0b
50	2020-12-25 16:21:26.471165+05:30	580ba285-1221-4c1b-a30e-74d28e6288d0	Asset object (580ba285-1221-4c1b-a30e-74d28e6288d0)	2	[{"changed": {"fields": ["Asset type"]}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
51	2020-12-31 17:07:23.288766+05:30	090d8a1e-3a43-4d0d-a600-9d781e529097	Asset object (090d8a1e-3a43-4d0d-a600-9d781e529097)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
52	2020-12-31 17:08:01.112189+05:30	4a551b4b-db0d-4797-a38d-a7b4c658e05a	File object (4a551b4b-db0d-4797-a38d-a7b4c658e05a)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
53	2020-12-31 17:09:31.773654+05:30	090d8a1e-3a43-4d0d-a600-9d781e529097	Asset object (090d8a1e-3a43-4d0d-a600-9d781e529097)	3		21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
54	2020-12-31 17:18:55.399873+05:30	c5ecc274-7aa7-4d5c-b3a0-4828fe585e6a	Asset object (c5ecc274-7aa7-4d5c-b3a0-4828fe585e6a)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
55	2020-12-31 17:19:14.935613+05:30	0f677ed0-e2a7-434e-8ae0-03e6e5154fff	File object (0f677ed0-e2a7-434e-8ae0-03e6e5154fff)	3		19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
56	2020-12-31 17:19:31.755721+05:30	f857af22-d0a3-4519-a3fb-ce8e2b177598	File object (f857af22-d0a3-4519-a3fb-ce8e2b177598)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
57	2020-12-31 17:23:27.713302+05:30	82c13751-22d6-4d93-842c-06fb06bd1f26	Asset object (82c13751-22d6-4d93-842c-06fb06bd1f26)	2	[{"changed": {"fields": ["Asset type"]}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
58	2020-12-31 17:23:51.451115+05:30	82c13751-22d6-4d93-842c-06fb06bd1f26	Asset object (82c13751-22d6-4d93-842c-06fb06bd1f26)	2	[{"changed": {"fields": ["Asset type"]}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
59	2020-12-31 17:23:59.649244+05:30	c5ecc274-7aa7-4d5c-b3a0-4828fe585e6a	Asset object (c5ecc274-7aa7-4d5c-b3a0-4828fe585e6a)	2	[{"changed": {"fields": ["Asset type"]}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
60	2020-12-31 18:46:12.651195+05:30	2	ItemType object (2)	1	[{"added": {}}]	23	0a8462e9-7414-470d-bd7e-18b2235ebd0b
61	2020-12-31 18:46:27.660418+05:30	3	ItemType object (3)	1	[{"added": {}}]	23	0a8462e9-7414-470d-bd7e-18b2235ebd0b
62	2020-12-31 18:46:38.063241+05:30	4	ItemType object (4)	1	[{"added": {}}]	23	0a8462e9-7414-470d-bd7e-18b2235ebd0b
63	2020-12-31 18:46:49.982755+05:30	5	ItemType object (5)	1	[{"added": {}}]	23	0a8462e9-7414-470d-bd7e-18b2235ebd0b
64	2020-12-31 19:34:35.769543+05:30	c5ecc274-7aa7-4d5c-b3a0-4828fe585e6a	Asset object (c5ecc274-7aa7-4d5c-b3a0-4828fe585e6a)	2	[{"changed": {"fields": ["Asset type"]}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
65	2020-12-31 19:35:03.912813+05:30	c5ecc274-7aa7-4d5c-b3a0-4828fe585e6a	Asset object (c5ecc274-7aa7-4d5c-b3a0-4828fe585e6a)	2	[{"changed": {"fields": ["Asset type"]}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
66	2020-12-31 19:40:23.917697+05:30	12724671-cbbd-4e76-a1b8-2f9f948542cd	Asset object (12724671-cbbd-4e76-a1b8-2f9f948542cd)	2	[{"changed": {"fields": ["Asset type"]}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
67	2020-12-31 19:40:39.825514+05:30	0d452712-26a1-4730-83b1-2a95714b59eb	Asset object (0d452712-26a1-4730-83b1-2a95714b59eb)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
68	2021-01-01 15:07:33.045692+05:30	5e9932f4-d3aa-4ddc-a280-f075dcb36964	5e9932f4-d3aa-4ddc-a280-f075dcb36964	2	[{"changed": {"fields": ["Item type"]}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
69	2021-01-01 15:08:12.532846+05:30	aa6aa20c-f5ce-4410-827f-8f9dc91098bf	Asset object (aa6aa20c-f5ce-4410-827f-8f9dc91098bf)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
70	2021-01-01 15:08:28.086786+05:30	0ba44b44-f50e-47d3-add1-079a24565522	Asset object (0ba44b44-f50e-47d3-add1-079a24565522)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
71	2021-01-01 15:09:44.442262+05:30	770c4d90-ad63-4955-a576-3cc105c872bd	File object (770c4d90-ad63-4955-a576-3cc105c872bd)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
72	2021-01-01 15:10:03.804224+05:30	f025ca10-cb9c-4d76-978b-c904ed657b15	File object (f025ca10-cb9c-4d76-978b-c904ed657b15)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
73	2021-01-01 15:10:55.084686+05:30	f9031a0b-e059-4570-af45-10c072f552c9	f9031a0b-e059-4570-af45-10c072f552c9	2	[{"changed": {"fields": ["Item type"]}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
74	2021-01-01 15:11:15.564391+05:30	66371883-5e62-46d3-b4fa-5c17deabbdb8	Asset object (66371883-5e62-46d3-b4fa-5c17deabbdb8)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
75	2021-01-01 15:11:27.807321+05:30	69310df8-e880-456f-acaf-aea1ff949dc7	Asset object (69310df8-e880-456f-acaf-aea1ff949dc7)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
76	2021-01-01 15:11:40.661971+05:30	52a2eff8-abf6-4f90-91af-36af7edad158	Asset object (52a2eff8-abf6-4f90-91af-36af7edad158)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
77	2021-01-01 15:12:05.462838+05:30	fd252c87-561a-473f-8bb2-4fcfd1628ee0	File object (fd252c87-561a-473f-8bb2-4fcfd1628ee0)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
78	2021-01-01 15:12:29.994041+05:30	a47499cb-748c-4141-aa8e-55c92791f7bf	File object (a47499cb-748c-4141-aa8e-55c92791f7bf)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
79	2021-01-01 15:12:53.47579+05:30	36baca2b-e56e-4037-8c7f-1f988f7c12f9	File object (36baca2b-e56e-4037-8c7f-1f988f7c12f9)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
80	2021-01-01 15:14:39.172992+05:30	481bd339-61cc-48ff-bc44-be4ab94dee3b	Asset object (481bd339-61cc-48ff-bc44-be4ab94dee3b)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
81	2021-01-01 15:14:59.930003+05:30	c0b0f337-eb72-4cd2-bce3-5a9803add8c8	Asset object (c0b0f337-eb72-4cd2-bce3-5a9803add8c8)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
82	2021-01-01 15:15:13.216357+05:30	1370b41e-d7c1-423a-8bd6-4d7a2cf796f4	Asset object (1370b41e-d7c1-423a-8bd6-4d7a2cf796f4)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
83	2021-01-01 15:16:59.71056+05:30	54dda88a-ed7a-4496-a02d-9be280611772	File object (54dda88a-ed7a-4496-a02d-9be280611772)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
84	2021-01-01 15:17:31.653823+05:30	b592ff98-364a-493e-a615-9de33b95db0e	File object (b592ff98-364a-493e-a615-9de33b95db0e)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
85	2021-01-01 15:17:49.887813+05:30	a0df1063-7bb8-463d-b65e-8793e11c1fa7	File object (a0df1063-7bb8-463d-b65e-8793e11c1fa7)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
86	2021-01-01 15:20:41.175656+05:30	3c1e5a03-cab1-46c5-8c71-a356050942f1	3c1e5a03-cab1-46c5-8c71-a356050942f1	2	[{"changed": {"fields": ["Item type"]}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
87	2021-01-01 15:21:41.844819+05:30	e131db14-0321-4ee9-b59a-e58010859da7	Asset object (e131db14-0321-4ee9-b59a-e58010859da7)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
88	2021-01-01 15:21:57.550395+05:30	97c63b69-a668-4673-bde7-53fecab86593	Asset object (97c63b69-a668-4673-bde7-53fecab86593)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
89	2021-01-01 15:22:32.679347+05:30	c55d728f-0c81-4520-9729-ee127cdcc082	File object (c55d728f-0c81-4520-9729-ee127cdcc082)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
90	2021-01-01 15:22:58.3311+05:30	ca73ce3d-e1e7-4e63-aa5d-c2de84e584fb	File object (ca73ce3d-e1e7-4e63-aa5d-c2de84e584fb)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
91	2021-01-01 15:23:51.759208+05:30	71cb76c1-5452-4d77-9940-a22106f04fe1	71cb76c1-5452-4d77-9940-a22106f04fe1	2	[{"changed": {"fields": ["Item type"]}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
92	2021-01-01 15:24:22.725645+05:30	c7630a04-99cf-44e5-bbe6-072646242355	Asset object (c7630a04-99cf-44e5-bbe6-072646242355)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
93	2021-01-01 15:24:37.997001+05:30	7056907d-efd6-4749-b8a9-d6c32c50235f	Asset object (7056907d-efd6-4749-b8a9-d6c32c50235f)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
94	2021-01-01 15:25:06.808638+05:30	382dd5d1-3143-4982-a9a1-bde0db417ae2	File object (382dd5d1-3143-4982-a9a1-bde0db417ae2)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
95	2021-01-01 15:25:21.611103+05:30	03ef2ee7-95fe-4fc8-b0f2-750b0737ad1d	File object (03ef2ee7-95fe-4fc8-b0f2-750b0737ad1d)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
96	2021-01-01 15:26:32.476261+05:30	df680a63-78d4-4af5-91b2-f225c7a672de	Asset object (df680a63-78d4-4af5-91b2-f225c7a672de)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
97	2021-01-01 15:26:44.676282+05:30	7c327cf7-73a4-4603-8d1c-6d0efc90a138	Asset object (7c327cf7-73a4-4603-8d1c-6d0efc90a138)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
98	2021-01-01 15:27:21.318232+05:30	75a5af36-9c0c-467a-b241-6c180a38f84b	Asset object (75a5af36-9c0c-467a-b241-6c180a38f84b)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
99	2021-01-01 15:27:34.430057+05:30	0bab833f-d959-483b-887c-6db69af76e40	Asset object (0bab833f-d959-483b-887c-6db69af76e40)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
100	2021-01-01 15:27:46.93425+05:30	3e2af4b1-3e77-4964-be3a-885842f2bfe2	Asset object (3e2af4b1-3e77-4964-be3a-885842f2bfe2)	1	[{"added": {}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
101	2021-01-01 15:28:23.25451+05:30	29cebf73-e34b-4f6e-8898-bb8200d2b6cd	File object (29cebf73-e34b-4f6e-8898-bb8200d2b6cd)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
102	2021-01-01 15:28:39.347379+05:30	091464f6-bc52-44f2-bbe8-e84889340e16	File object (091464f6-bc52-44f2-bbe8-e84889340e16)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
103	2021-01-01 15:28:54.462583+05:30	6780d9af-40dc-4e55-b3c3-3007e1b6b961	File object (6780d9af-40dc-4e55-b3c3-3007e1b6b961)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
104	2021-01-01 15:29:12.688918+05:30	936b1fd2-5897-4f34-9ba6-2dfc967b54c4	File object (936b1fd2-5897-4f34-9ba6-2dfc967b54c4)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
105	2021-01-01 15:29:26.312756+05:30	30a1d584-5de4-42c5-9557-b03afdfd735f	File object (30a1d584-5de4-42c5-9557-b03afdfd735f)	1	[{"added": {}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
106	2021-01-01 15:30:44.032382+05:30	71cb76c1-5452-4d77-9940-a22106f04fe1	71cb76c1-5452-4d77-9940-a22106f04fe1	2	[{"changed": {"fields": ["Item type"]}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
107	2021-01-01 15:32:15.10671+05:30	0d452712-26a1-4730-83b1-2a95714b59eb	Asset object (0d452712-26a1-4730-83b1-2a95714b59eb)	2	[{"changed": {"fields": ["Asset type"]}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
108	2021-01-01 15:32:36.411751+05:30	c5ecc274-7aa7-4d5c-b3a0-4828fe585e6a	Asset object (c5ecc274-7aa7-4d5c-b3a0-4828fe585e6a)	2	[{"changed": {"fields": ["Asset type"]}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
109	2021-01-01 15:33:17.668612+05:30	12724671-cbbd-4e76-a1b8-2f9f948542cd	Asset object (12724671-cbbd-4e76-a1b8-2f9f948542cd)	2	[{"changed": {"fields": ["Asset type"]}}]	21	0a8462e9-7414-470d-bd7e-18b2235ebd0b
110	2021-01-01 15:35:00.23609+05:30	4f507239-237e-4a87-a857-9f761e5c7b80	File object (4f507239-237e-4a87-a857-9f761e5c7b80)	2	[{"changed": {"fields": ["Filename"]}}]	19	0a8462e9-7414-470d-bd7e-18b2235ebd0b
111	2021-01-31 15:35:51.717827+05:30	300	audio	2	[{"changed": {"fields": ["Item type id"]}}]	23	0a8462e9-7414-470d-bd7e-18b2235ebd0b
112	2021-01-31 15:36:16.373261+05:30	4	audio	2	[{"changed": {"fields": ["Item type id"]}}]	23	0a8462e9-7414-470d-bd7e-18b2235ebd0b
113	2021-01-31 16:45:01.211095+05:30	5e9932f4-d3aa-4ddc-a280-f075dcb36964	5e9932f4-d3aa-4ddc-a280-f075dcb36964	2	[{"changed": {"fields": ["Metadata"]}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
114	2021-02-02 16:16:10.171341+05:30	300	audio	3		23	0a8462e9-7414-470d-bd7e-18b2235ebd0b
115	2021-02-02 16:17:06.586171+05:30	100	video	2	[{"changed": {"fields": ["Item type id"]}}]	23	0a8462e9-7414-470d-bd7e-18b2235ebd0b
116	2021-02-02 16:17:57.115552+05:30	ef644edc-c566-46d0-9894-441b9e6b4af7	ef644edc-c566-46d0-9894-441b9e6b4af7	2	[{"changed": {"fields": ["Item type"]}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
117	2021-02-02 16:18:04.224433+05:30	e9ae57a2-8c7d-41ab-be1f-d5af084acd04	e9ae57a2-8c7d-41ab-be1f-d5af084acd04	2	[{"changed": {"fields": ["Item type"]}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
118	2021-02-02 16:18:12.092022+05:30	cc8316cf-308b-4734-847d-2b30694f4636	cc8316cf-308b-4734-847d-2b30694f4636	2	[{"changed": {"fields": ["Item type"]}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
119	2021-02-02 16:18:25.34363+05:30	100	videos	2	[{"changed": {"fields": ["Item type"]}}]	23	0a8462e9-7414-470d-bd7e-18b2235ebd0b
120	2021-02-02 16:18:43.350305+05:30	200	images	1	[{"added": {}}]	23	0a8462e9-7414-470d-bd7e-18b2235ebd0b
121	2021-02-02 16:18:55.364039+05:30	300	audios	1	[{"added": {}}]	23	0a8462e9-7414-470d-bd7e-18b2235ebd0b
122	2021-02-02 16:19:12.058619+05:30	f9031a0b-e059-4570-af45-10c072f552c9	f9031a0b-e059-4570-af45-10c072f552c9	2	[{"changed": {"fields": ["Item type"]}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
123	2021-02-02 16:19:32.614441+05:30	c49a4b5f-f0c7-42a2-b857-e2116fe00fac	c49a4b5f-f0c7-42a2-b857-e2116fe00fac	2	[{"changed": {"fields": ["Item type"]}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
124	2021-02-02 16:19:40.1102+05:30	3c1e5a03-cab1-46c5-8c71-a356050942f1	3c1e5a03-cab1-46c5-8c71-a356050942f1	2	[{"changed": {"fields": ["Item type"]}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
125	2021-02-02 16:19:47.046888+05:30	4a4801c5-0114-48b3-a757-852fd608e63d	4a4801c5-0114-48b3-a757-852fd608e63d	2	[{"changed": {"fields": ["Item type"]}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
126	2021-02-02 16:19:53.446611+05:30	5e9932f4-d3aa-4ddc-a280-f075dcb36964	5e9932f4-d3aa-4ddc-a280-f075dcb36964	2	[{"changed": {"fields": ["Item type"]}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
127	2021-02-02 16:20:00.093103+05:30	71cb76c1-5452-4d77-9940-a22106f04fe1	71cb76c1-5452-4d77-9940-a22106f04fe1	2	[{"changed": {"fields": ["Item type"]}}]	14	0a8462e9-7414-470d-bd7e-18b2235ebd0b
128	2021-02-02 16:20:34.583373+05:30	5	ebook	3		23	0a8462e9-7414-470d-bd7e-18b2235ebd0b
129	2021-02-02 16:20:34.699795+05:30	4	audio	3		23	0a8462e9-7414-470d-bd7e-18b2235ebd0b
130	2021-02-02 16:20:34.710921+05:30	3	video	3		23	0a8462e9-7414-470d-bd7e-18b2235ebd0b
131	2021-02-02 16:20:34.721762+05:30	2	image	3		23	0a8462e9-7414-470d-bd7e-18b2235ebd0b
132	2021-02-02 16:20:40.194062+05:30	300	audio	2	[{"changed": {"fields": ["Item type"]}}]	23	0a8462e9-7414-470d-bd7e-18b2235ebd0b
133	2021-02-02 16:20:44.929728+05:30	200	image	2	[{"changed": {"fields": ["Item type"]}}]	23	0a8462e9-7414-470d-bd7e-18b2235ebd0b
134	2021-02-02 16:20:50.264181+05:30	100	video	2	[{"changed": {"fields": ["Item type"]}}]	23	0a8462e9-7414-470d-bd7e-18b2235ebd0b
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	contenttypes	contenttype
5	sessions	session
6	authtoken	token
7	tutorials	tutorial
8	profile_app	userprofile
9	profile_app	item
10	profile_app	rating
11	profile_app	list
12	profile_app	history
13	profile_app	bookmark
14	api	item
15	api	list
16	api	history
17	api	bookmark
18	api	rating
19	api	file
20	api	device
21	api	asset
22	auth	user
23	api	itemtype
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2020-12-09 15:31:02.371737+05:30
2	contenttypes	0002_remove_content_type_name	2020-12-09 15:31:02.441097+05:30
3	auth	0001_initial	2020-12-09 15:31:02.614718+05:30
4	auth	0002_alter_permission_name_max_length	2020-12-09 15:31:02.910732+05:30
5	auth	0003_alter_user_email_max_length	2020-12-09 15:31:02.934218+05:30
6	auth	0004_alter_user_username_opts	2020-12-09 15:31:02.950784+05:30
7	auth	0005_alter_user_last_login_null	2020-12-09 15:31:02.962748+05:30
8	auth	0006_require_contenttypes_0002	2020-12-09 15:31:02.968271+05:30
9	auth	0007_alter_validators_add_error_messages	2020-12-09 15:31:02.983797+05:30
10	auth	0008_alter_user_username_max_length	2020-12-09 15:31:02.993981+05:30
11	auth	0009_alter_user_last_name_max_length	2020-12-09 15:31:03.007398+05:30
12	auth	0010_alter_group_name_max_length	2020-12-09 15:31:03.029342+05:30
13	auth	0011_update_proxy_permissions	2020-12-09 15:31:03.038807+05:30
14	auth	0012_alter_user_first_name_max_length	2020-12-09 15:31:03.050241+05:30
15	profile_app	0001_initial	2020-12-09 15:31:03.779711+05:30
16	admin	0001_initial	2020-12-09 15:31:04.687065+05:30
17	admin	0002_logentry_remove_auto_add	2020-12-09 15:31:04.797296+05:30
18	admin	0003_logentry_add_action_flag_choices	2020-12-09 15:31:04.821719+05:30
19	authtoken	0001_initial	2020-12-09 15:31:04.888202+05:30
20	authtoken	0002_token_user	2020-12-09 15:31:04.99772+05:30
21	profile_app	0002_auto_20201209_1000	2020-12-09 15:31:05.121474+05:30
22	sessions	0001_initial	2020-12-09 15:31:05.344864+05:30
23	tutorials	0001_initial	2020-12-09 15:31:05.478144+05:30
24	profile_app	0003_auto_20201209_1007	2020-12-09 15:37:23.319423+05:30
25	api	0001_initial	2020-12-12 18:26:29.995453+05:30
26	profile_app	0004_auto_20201212_1255	2020-12-12 18:26:31.439004+05:30
27	api	0002_asset_device_file	2020-12-15 20:11:18.556725+05:30
28	api	0003_auto_20201215_1442	2020-12-15 20:12:12.616835+05:30
29	api	0004_auto_20201225_1017	2020-12-25 15:47:45.861318+05:30
30	api	0005_auto_20201231_1305	2020-12-31 18:35:19.985395+05:30
31	api	0006_auto_20201231_1315	2020-12-31 18:45:42.168751+05:30
32	api	0007_item_item_type	2020-12-31 18:51:51.612312+05:30
33	api	0008_auto_20201231_1404	2020-12-31 19:34:08.581686+05:30
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
e59d9qbwb2xumead1vfq5xehsgde22dh	.eJxVzMsOwiAQheF3YS0EplDApXufoZlhBls1bdLLyvju2qQLXZ__Oy_V4bb23bbI3A2szspi8i1I1tE7r320rImjaJcIoAlCbEmdfhlheci4W77jeJtMmcZ1HsjsiTnWxVwnluflaP8Oelz6r8bWImBNQDEAA_kEGSXkkpMLTJECVedd5eiEpaklxYo2hBIsNikn9f4A1AVBQw:1kmwik:hfF0Nb6Wqnj-Uu8Zc7pDijxcfiDTPA2sG6lmB5X84D0	2020-12-23 15:59:18.233082+05:30
n1j0qdr6tjm5cypl4hdy54uphgiztmuy	.eJxVzMsOwiAQheF3YS0EplDApXufoZlhBls1bdLLyvju2qQLXZ__Oy_V4bb23bbI3A2szspi8i1I1tE7r320rImjaJcIoAlCbEmdfhlheci4W77jeJtMmcZ1HsjsiTnWxVwnluflaP8Oelz6r8bWImBNQDEAA_kEGSXkkpMLTJECVedd5eiEpaklxYo2hBIsNikn9f4A1AVBQw:1knzWc:qk8aE7CkrgALggnrNAqNVbeOcc1NGyWl0ZfgdQPvvFU	2020-12-26 13:11:06.164362+05:30
he47pfch9ldumyqai5nefd73i1teqekf	.eJxVzMsOwiAQheF3YS0EplDApXufoZlhBls1bdLLyvju2qQLXZ__Oy_V4bb23bbI3A2szspi8i1I1tE7r320rImjaJcIoAlCbEmdfhlheci4W77jeJtMmcZ1HsjsiTnWxVwnluflaP8Oelz6r8bWImBNQDEAA_kEGSXkkpMLTJECVedd5eiEpaklxYo2hBIsNikn9f4A1AVBQw:1kpCFP:WQk6Of85V35nbH6vu6Ivv9tbNmt3gWzaUWxSpCJLIw0	2020-12-29 20:58:19.413381+05:30
7e0hcunwifciaze8yupr886vqbb8iew6	.eJxVzMsOwiAQheF3YS0EplDApXufoZlhBls1bdLLyvju2qQLXZ__Oy_V4bb23bbI3A2szspi8i1I1tE7r320rImjaJcIoAlCbEmdfhlheci4W77jeJtMmcZ1HsjsiTnWxVwnluflaP8Oelz6r8bWImBNQDEAA_kEGSXkkpMLTJECVedd5eiEpaklxYo2hBIsNikn9f4A1AVBQw:1ktCvX:yfZ6XTyMAnW8JefgxX82zEXCYzFl61qCMMI1V1bfl7Y	2021-01-09 22:30:23.919566+05:30
hsfj3jphp0vjqj51378ishdp5ayx9g39	.eJxVzMsOwiAQheF3YS0EplDApXufoZlhBls1bdLLyvju2qQLXZ__Oy_V4bb23bbI3A2szspi8i1I1tE7r320rImjaJcIoAlCbEmdfhlheci4W77jeJtMmcZ1HsjsiTnWxVwnluflaP8Oelz6r8bWImBNQDEAA_kEGSXkkpMLTJECVedd5eiEpaklxYo2hBIsNikn9f4A1AVBQw:1kucKP:LP6SWR-r1wwCe0GkWcsQMEnrN08eZjgAqxPOvz06MHI	2021-01-13 19:49:53.606484+05:30
mdduu8h4rs9kgyadjqj0ulnqfq7rqlf3	.eJxVzMsOwiAQheF3YS0EplDApXufoZlhBls1bdLLyvju2qQLXZ__Oy_V4bb23bbI3A2szspi8i1I1tE7r320rImjaJcIoAlCbEmdfhlheci4W77jeJtMmcZ1HsjsiTnWxVwnluflaP8Oelz6r8bWImBNQDEAA_kEGSXkkpMLTJECVedd5eiEpaklxYo2hBIsNikn9f4A1AVBQw:1kudfK:enA-Xga--Q6IlMwKSYNR4hwYNk8HyYZzPm-ckgQQOQ0	2021-01-13 21:15:34.035704+05:30
1c0q173dpyf3aagcjkwmc3r1it9vvev8	.eJxVzMsOwiAQheF3YS0EplDApXufoZlhBls1bdLLyvju2qQLXZ__Oy_V4bb23bbI3A2szspi8i1I1tE7r320rImjaJcIoAlCbEmdfhlheci4W77jeJtMmcZ1HsjsiTnWxVwnluflaP8Oelz6r8bWImBNQDEAA_kEGSXkkpMLTJECVedd5eiEpaklxYo2hBIsNikn9f4A1AVBQw:1kudy0:4GTSt1QVSMlVUCEoqYtgzQdS5F0AzUbmPXj_E2w_q2c	2021-01-13 21:34:52.481626+05:30
qp8thx68r86gv19g5fitds0hqgii8hk7	.eJxVzMsOwiAQheF3YS0EplDApXufoZlhBls1bdLLyvju2qQLXZ__Oy_V4bb23bbI3A2szspi8i1I1tE7r320rImjaJcIoAlCbEmdfhlheci4W77jeJtMmcZ1HsjsiTnWxVwnluflaP8Oelz6r8bWImBNQDEAA_kEGSXkkpMLTJECVedd5eiEpaklxYo2hBIsNikn9f4A1AVBQw:1l2Gy9:EK43xdRkb7zL50d8H4lNh_mJnpRR3b5W3-S6Q9K3LbU	2021-02-03 22:38:33.9902+05:30
zzofb0612edman163e8vwkb310m86wdi	.eJxVzMsOwiAQheF3YS0EplDApXufoZlhBls1bdLLyvju2qQLXZ__Oy_V4bb23bbI3A2szspi8i1I1tE7r320rImjaJcIoAlCbEmdfhlheci4W77jeJtMmcZ1HsjsiTnWxVwnluflaP8Oelz6r8bWImBNQDEAA_kEGSXkkpMLTJECVedd5eiEpaklxYo2hBIsNikn9f4A1AVBQw:1l6snI:eUtiCBd_HlbVrlj7pczRzBNptTkX_2UOIDb7gUqZTyg	2021-02-16 15:50:24.701123+05:30
\.


--
-- Data for Name: profile_app_userprofile; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.profile_app_userprofile (password, last_login, is_superuser, user_uuid, email, name, is_active, is_staff) FROM stdin;
pbkdf2_sha256$216000$O2LvzCjjYTEy$VtSifJjEM3ofOv3p0ETlYTLL1KxSEg06KG84hEaz+ck=	\N	t	426efbcd-99b1-4870-8178-39bd4c22e66c	u1@gmail.com	u1	t	t
u2	\N	f	4c423eb7-9319-4093-801c-31860cd01d4a	u2@gmail.com	u2	f	f
pbkdf2_sha256$216000$Qmyf4g9tAj6e$pA86mhTzy/xcs6X3kJP5iKz5Zxz9d8jUyr1DdQY07u8=	2021-02-02 15:50:24.684097+05:30	t	0a8462e9-7414-470d-bd7e-18b2235ebd0b	test@gmail.com	test	t	t
\.


--
-- Data for Name: profile_app_userprofile_groups; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.profile_app_userprofile_groups (id, userprofile_id, group_id) FROM stdin;
\.


--
-- Data for Name: profile_app_userprofile_user_permissions; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.profile_app_userprofile_user_permissions (id, userprofile_id, permission_id) FROM stdin;
\.


--
-- Data for Name: tutorials_tutorial; Type: TABLE DATA; Schema: public; Owner: dipak
--

COPY public.tutorials_tutorial (id, title, description, published) FROM stdin;
\.


--
-- Name: api_item_favourites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dipak
--

SELECT pg_catalog.setval('public.api_item_favourites_id_seq', 4, true);


--
-- Name: api_item_likes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dipak
--

SELECT pg_catalog.setval('public.api_item_likes_id_seq', 14, true);


--
-- Name: api_rating_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dipak
--

SELECT pg_catalog.setval('public.api_rating_id_seq', 5, true);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dipak
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dipak
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dipak
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 96, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dipak
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 134, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dipak
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 23, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dipak
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 33, true);


--
-- Name: profile_app_userprofile_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dipak
--

SELECT pg_catalog.setval('public.profile_app_userprofile_groups_id_seq', 1, false);


--
-- Name: profile_app_userprofile_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dipak
--

SELECT pg_catalog.setval('public.profile_app_userprofile_user_permissions_id_seq', 1, false);


--
-- Name: tutorials_tutorial_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dipak
--

SELECT pg_catalog.setval('public.tutorials_tutorial_id_seq', 1, false);


--
-- Name: api_asset api_asset_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_asset
    ADD CONSTRAINT api_asset_pkey PRIMARY KEY (asset_uuid);


--
-- Name: api_bookmark api_bookmark_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_bookmark
    ADD CONSTRAINT api_bookmark_pkey PRIMARY KEY (bookmark_uuid);


--
-- Name: api_device api_device_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_device
    ADD CONSTRAINT api_device_pkey PRIMARY KEY (device_uuid);


--
-- Name: api_file api_file_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_file
    ADD CONSTRAINT api_file_pkey PRIMARY KEY (file_uuid);


--
-- Name: api_history api_history_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_history
    ADD CONSTRAINT api_history_pkey PRIMARY KEY (history_uuid);


--
-- Name: api_item_favourites api_item_favourites_item_id_userprofile_id_96a8fba9_uniq; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_item_favourites
    ADD CONSTRAINT api_item_favourites_item_id_userprofile_id_96a8fba9_uniq UNIQUE (item_id, userprofile_id);


--
-- Name: api_item_favourites api_item_favourites_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_item_favourites
    ADD CONSTRAINT api_item_favourites_pkey PRIMARY KEY (id);


--
-- Name: api_item_likes api_item_likes_item_id_userprofile_id_4aed803f_uniq; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_item_likes
    ADD CONSTRAINT api_item_likes_item_id_userprofile_id_4aed803f_uniq UNIQUE (item_id, userprofile_id);


--
-- Name: api_item_likes api_item_likes_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_item_likes
    ADD CONSTRAINT api_item_likes_pkey PRIMARY KEY (id);


--
-- Name: api_item api_item_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_item
    ADD CONSTRAINT api_item_pkey PRIMARY KEY (item_uuid);


--
-- Name: api_itemtype api_itemtypes_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_itemtype
    ADD CONSTRAINT api_itemtypes_pkey PRIMARY KEY (item_type_id);


--
-- Name: api_list api_list_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_list
    ADD CONSTRAINT api_list_pkey PRIMARY KEY (list_uuid);


--
-- Name: api_rating api_rating_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_rating
    ADD CONSTRAINT api_rating_pkey PRIMARY KEY (id);


--
-- Name: api_rating api_rating_user_uuid_id_item_uuid_id_69a6c91b_uniq; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_rating
    ADD CONSTRAINT api_rating_user_uuid_id_item_uuid_id_69a6c91b_uniq UNIQUE (user_uuid_id, item_uuid_id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: authtoken_token authtoken_token_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_pkey PRIMARY KEY (key);


--
-- Name: authtoken_token authtoken_token_user_id_key; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_key UNIQUE (user_id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: profile_app_userprofile_groups profile_app_userprofile__userprofile_id_group_id_78fe0eac_uniq; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.profile_app_userprofile_groups
    ADD CONSTRAINT profile_app_userprofile__userprofile_id_group_id_78fe0eac_uniq UNIQUE (userprofile_id, group_id);


--
-- Name: profile_app_userprofile_user_permissions profile_app_userprofile__userprofile_id_permissio_5c5fade4_uniq; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.profile_app_userprofile_user_permissions
    ADD CONSTRAINT profile_app_userprofile__userprofile_id_permissio_5c5fade4_uniq UNIQUE (userprofile_id, permission_id);


--
-- Name: profile_app_userprofile profile_app_userprofile_email_key; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.profile_app_userprofile
    ADD CONSTRAINT profile_app_userprofile_email_key UNIQUE (email);


--
-- Name: profile_app_userprofile_groups profile_app_userprofile_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.profile_app_userprofile_groups
    ADD CONSTRAINT profile_app_userprofile_groups_pkey PRIMARY KEY (id);


--
-- Name: profile_app_userprofile profile_app_userprofile_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.profile_app_userprofile
    ADD CONSTRAINT profile_app_userprofile_pkey PRIMARY KEY (user_uuid);


--
-- Name: profile_app_userprofile_user_permissions profile_app_userprofile_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.profile_app_userprofile_user_permissions
    ADD CONSTRAINT profile_app_userprofile_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: tutorials_tutorial tutorials_tutorial_pkey; Type: CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.tutorials_tutorial
    ADD CONSTRAINT tutorials_tutorial_pkey PRIMARY KEY (id);


--
-- Name: api_asset_item_uuid_id_684ff3fd; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX api_asset_item_uuid_id_684ff3fd ON public.api_asset USING btree (item_uuid_id);


--
-- Name: api_bookmark_item_uuid_id_0854a247; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX api_bookmark_item_uuid_id_0854a247 ON public.api_bookmark USING btree (item_uuid_id);


--
-- Name: api_bookmark_user_uuid_id_f3748716; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX api_bookmark_user_uuid_id_f3748716 ON public.api_bookmark USING btree (user_uuid_id);


--
-- Name: api_file_asset_uuid_id_06e626ff; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX api_file_asset_uuid_id_06e626ff ON public.api_file USING btree (asset_uuid_id);


--
-- Name: api_file_device_uuid_id_8b47e6b0; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX api_file_device_uuid_id_8b47e6b0 ON public.api_file USING btree (device_uuid_id);


--
-- Name: api_history_item_uuid_id_eaaa9f07; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX api_history_item_uuid_id_eaaa9f07 ON public.api_history USING btree (item_uuid_id);


--
-- Name: api_history_user_uuid_id_60037e68; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX api_history_user_uuid_id_60037e68 ON public.api_history USING btree (user_uuid_id);


--
-- Name: api_item_favourites_item_id_693ccf18; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX api_item_favourites_item_id_693ccf18 ON public.api_item_favourites USING btree (item_id);


--
-- Name: api_item_favourites_userprofile_id_ca78ff90; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX api_item_favourites_userprofile_id_ca78ff90 ON public.api_item_favourites USING btree (userprofile_id);


--
-- Name: api_item_item_type_id_445b0994; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX api_item_item_type_id_445b0994 ON public.api_item USING btree (item_type_id);


--
-- Name: api_item_likes_item_id_8d570e30; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX api_item_likes_item_id_8d570e30 ON public.api_item_likes USING btree (item_id);


--
-- Name: api_item_likes_userprofile_id_d3ac9029; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX api_item_likes_userprofile_id_d3ac9029 ON public.api_item_likes USING btree (userprofile_id);


--
-- Name: api_list_user_uuid_id_63b49599; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX api_list_user_uuid_id_63b49599 ON public.api_list USING btree (user_uuid_id);


--
-- Name: api_rating_item_uuid_id_8fa5018b; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX api_rating_item_uuid_id_8fa5018b ON public.api_rating USING btree (item_uuid_id);


--
-- Name: api_rating_user_uuid_id_fc4ccce5; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX api_rating_user_uuid_id_fc4ccce5 ON public.api_rating USING btree (user_uuid_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: authtoken_token_key_10f0b77e_like; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX authtoken_token_key_10f0b77e_like ON public.authtoken_token USING btree (key varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: profile_app_userprofile_email_d121798a_like; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX profile_app_userprofile_email_d121798a_like ON public.profile_app_userprofile USING btree (email varchar_pattern_ops);


--
-- Name: profile_app_userprofile_groups_group_id_af341cf7; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX profile_app_userprofile_groups_group_id_af341cf7 ON public.profile_app_userprofile_groups USING btree (group_id);


--
-- Name: profile_app_userprofile_groups_userprofile_id_54f7c4f6; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX profile_app_userprofile_groups_userprofile_id_54f7c4f6 ON public.profile_app_userprofile_groups USING btree (userprofile_id);


--
-- Name: profile_app_userprofile_us_userprofile_id_4aa86a8e; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX profile_app_userprofile_us_userprofile_id_4aa86a8e ON public.profile_app_userprofile_user_permissions USING btree (userprofile_id);


--
-- Name: profile_app_userprofile_user_permissions_permission_id_cb95cac9; Type: INDEX; Schema: public; Owner: dipak
--

CREATE INDEX profile_app_userprofile_user_permissions_permission_id_cb95cac9 ON public.profile_app_userprofile_user_permissions USING btree (permission_id);


--
-- Name: api_asset api_asset_item_uuid_id_684ff3fd_fk_api_item_item_uuid; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_asset
    ADD CONSTRAINT api_asset_item_uuid_id_684ff3fd_fk_api_item_item_uuid FOREIGN KEY (item_uuid_id) REFERENCES public.api_item(item_uuid) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_bookmark api_bookmark_item_uuid_id_0854a247_fk_api_item_item_uuid; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_bookmark
    ADD CONSTRAINT api_bookmark_item_uuid_id_0854a247_fk_api_item_item_uuid FOREIGN KEY (item_uuid_id) REFERENCES public.api_item(item_uuid) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_bookmark api_bookmark_user_uuid_id_f3748716_fk_profile_a; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_bookmark
    ADD CONSTRAINT api_bookmark_user_uuid_id_f3748716_fk_profile_a FOREIGN KEY (user_uuid_id) REFERENCES public.profile_app_userprofile(user_uuid) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_file api_file_asset_uuid_id_06e626ff_fk_api_asset_asset_uuid; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_file
    ADD CONSTRAINT api_file_asset_uuid_id_06e626ff_fk_api_asset_asset_uuid FOREIGN KEY (asset_uuid_id) REFERENCES public.api_asset(asset_uuid) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_file api_file_device_uuid_id_8b47e6b0_fk_api_device_device_uuid; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_file
    ADD CONSTRAINT api_file_device_uuid_id_8b47e6b0_fk_api_device_device_uuid FOREIGN KEY (device_uuid_id) REFERENCES public.api_device(device_uuid) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_history api_history_item_uuid_id_eaaa9f07_fk_api_item_item_uuid; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_history
    ADD CONSTRAINT api_history_item_uuid_id_eaaa9f07_fk_api_item_item_uuid FOREIGN KEY (item_uuid_id) REFERENCES public.api_item(item_uuid) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_history api_history_user_uuid_id_60037e68_fk_profile_a; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_history
    ADD CONSTRAINT api_history_user_uuid_id_60037e68_fk_profile_a FOREIGN KEY (user_uuid_id) REFERENCES public.profile_app_userprofile(user_uuid) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_item_favourites api_item_favourites_item_id_693ccf18_fk_api_item_item_uuid; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_item_favourites
    ADD CONSTRAINT api_item_favourites_item_id_693ccf18_fk_api_item_item_uuid FOREIGN KEY (item_id) REFERENCES public.api_item(item_uuid) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_item_favourites api_item_favourites_userprofile_id_ca78ff90_fk_profile_a; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_item_favourites
    ADD CONSTRAINT api_item_favourites_userprofile_id_ca78ff90_fk_profile_a FOREIGN KEY (userprofile_id) REFERENCES public.profile_app_userprofile(user_uuid) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_item api_item_item_type_id_445b0994_fk_api_itemtype_item_type_id; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_item
    ADD CONSTRAINT api_item_item_type_id_445b0994_fk_api_itemtype_item_type_id FOREIGN KEY (item_type_id) REFERENCES public.api_itemtype(item_type_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_item_likes api_item_likes_item_id_8d570e30_fk_api_item_item_uuid; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_item_likes
    ADD CONSTRAINT api_item_likes_item_id_8d570e30_fk_api_item_item_uuid FOREIGN KEY (item_id) REFERENCES public.api_item(item_uuid) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_item_likes api_item_likes_userprofile_id_d3ac9029_fk_profile_a; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_item_likes
    ADD CONSTRAINT api_item_likes_userprofile_id_d3ac9029_fk_profile_a FOREIGN KEY (userprofile_id) REFERENCES public.profile_app_userprofile(user_uuid) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_list api_list_user_uuid_id_63b49599_fk_profile_a; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_list
    ADD CONSTRAINT api_list_user_uuid_id_63b49599_fk_profile_a FOREIGN KEY (user_uuid_id) REFERENCES public.profile_app_userprofile(user_uuid) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_rating api_rating_item_uuid_id_8fa5018b_fk_api_item_item_uuid; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_rating
    ADD CONSTRAINT api_rating_item_uuid_id_8fa5018b_fk_api_item_item_uuid FOREIGN KEY (item_uuid_id) REFERENCES public.api_item(item_uuid) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_rating api_rating_user_uuid_id_fc4ccce5_fk_profile_a; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.api_rating
    ADD CONSTRAINT api_rating_user_uuid_id_fc4ccce5_fk_profile_a FOREIGN KEY (user_uuid_id) REFERENCES public.profile_app_userprofile(user_uuid) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: authtoken_token authtoken_token_user_id_35299eff_fk_profile_a; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_35299eff_fk_profile_a FOREIGN KEY (user_id) REFERENCES public.profile_app_userprofile(user_uuid) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_profile_a; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_profile_a FOREIGN KEY (user_id) REFERENCES public.profile_app_userprofile(user_uuid) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: profile_app_userprofile_groups profile_app_userprof_group_id_af341cf7_fk_auth_grou; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.profile_app_userprofile_groups
    ADD CONSTRAINT profile_app_userprof_group_id_af341cf7_fk_auth_grou FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: profile_app_userprofile_user_permissions profile_app_userprof_permission_id_cb95cac9_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.profile_app_userprofile_user_permissions
    ADD CONSTRAINT profile_app_userprof_permission_id_cb95cac9_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: profile_app_userprofile_user_permissions profile_app_userprof_userprofile_id_4aa86a8e_fk_profile_a; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.profile_app_userprofile_user_permissions
    ADD CONSTRAINT profile_app_userprof_userprofile_id_4aa86a8e_fk_profile_a FOREIGN KEY (userprofile_id) REFERENCES public.profile_app_userprofile(user_uuid) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: profile_app_userprofile_groups profile_app_userprof_userprofile_id_54f7c4f6_fk_profile_a; Type: FK CONSTRAINT; Schema: public; Owner: dipak
--

ALTER TABLE ONLY public.profile_app_userprofile_groups
    ADD CONSTRAINT profile_app_userprof_userprofile_id_54f7c4f6_fk_profile_a FOREIGN KEY (userprofile_id) REFERENCES public.profile_app_userprofile(user_uuid) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

