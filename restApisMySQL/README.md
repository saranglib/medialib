Follow below steps to run the project on your local system.

Python Version : 3.8

**1. Open Shell and move to restApisMySQL folder**  
**2. Create virtual environment (If you are using windows, create new conda environment)**  
 python3 -m venv env : This will create virtual environment named "env"  
**3. Activate the virtual environment**  
 source env/bin/activate  
**4. Install required packages**  
 python3 -m pip install -r requirements.txt  
**5. Open setting.py file from restApisMySQL folder and change database credentials.**  
 Change values according to your local setup  
 DATABASES = {  
 'default': {  
 'ENGINE': 'django.db.backends.postgresql_psycopg2',  
 'NAME': 'saranglib',  
 'USER': 'dipak',  
 'PASSWORD': 'dipak',  
 'HOST': '127.0.0.1',  
 'PORT': '5432',  
 }  
 }  
**6. Perform migration and run the server.**  
 python3 manage.py makemigrations profile_app  
 python3 manage.py migrate  
 python3 manage.py makemigrations api  
 python3 manage.py migrate  
 python3 manage.py makemigrations  
 python3 manage.py migrate  
 python3 manage.py createsuperuser  
 python3 manage.py runserver  
**7. Use /docs endpoint to view list of available APIs.**  
 To login into admin endpoint or to view API docs use email id and password you have registered using createsuperuser command.

Use below API to get content  
 http://localhost:8000/api/item/?type=audio - list of audios  
 http://localhost:8000/api/item/?type=video - list of videos  
 http://localhost:8000/docs/ - to view API docs

**8. After you finish the work deactivate the virtual environment.**  
 deactivate

Postgres Installation  
 sudo apt-get install postgresql libpq-dev postgresql-client postgresql-client-common postgresql-contrib

**9. Nginx setup is needed to access static files.**
