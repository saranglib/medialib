from django.contrib import admin

# Register your models here.
from api import models

admin.site.register(models.Item)
admin.site.register(models.Rating)
admin.site.register(models.Bookmark)
admin.site.register(models.History)
admin.site.register(models.List)
admin.site.register(models.Asset)
admin.site.register(models.File)
admin.site.register(models.Device)
admin.site.register(models.ItemType)
