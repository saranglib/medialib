import sys
import pprint
import os
import pathlib
import logging
import django
from django.db.models import manager
from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response

from django.utils.encoding import escape_uri_path, smart_str
from rest_framework.pagination import PageNumberPagination
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.decorators import permission_classes, authentication_classes, api_view
#from rest_framework.settings import api_settings

from django.shortcuts import get_object_or_404
from django.db import models as db_models

#from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_exempt

from api import serializers
from api import models
from api import utils
from api.documents import query_elastic
from api.utils import asset_mappings, type_mappings
from profile_app.models import UserProfile


@api_view(['GET'])
def file_view(request, file):
    response = HttpResponse(content_type='')
    path = "/data/" + file
    response['X-Accel-Redirect'] = path
    return response


@api_view(['GET'])
def file_metadata(request):
    filepath = request.query_params.get('filepath', False)
    if filepath:
        metadata = utils.get_file_metadata(filepath)
        if not metadata.get("error"):
            return Response(metadata)
        else:
            return Response(metadata, status=status.HTTP_400_BAD_REQUEST)
    else:
        return Response({"Invalid request"}, status=status.HTTP_400_BAD_REQUEST)


class ItemTypesViewset(viewsets.ModelViewSet):
    serializer_class = serializers.ItemTypesSerializer
    queryset = models.ItemType.objects.all()


class ItemDetailedViewset(viewsets.ModelViewSet):
    serializer_class = serializers.DetailedSerializer
    queryset = models.Item.objects.all()
    pagination_class = None

    def get_serializer_class(self):
        if self.action == "create":
            return serializers.BulkDetailedSerializer
        return serializers.DetailedSerializer


class ItemViewset(viewsets.ModelViewSet):
    serializer_class = serializers.ItemSerializer
    queryset = models.Item.objects.all()
    pagination_class = PageNumberPagination
    # authentication_classes = (SessionAuthentication,)
    # permission_classes = (IsAuthenticatedOrReadOnly, )

    def get_serializer_class(self):
        if self.action == "retrieve" or self.action == "list":
            return serializers.ItemDetailedSerializer
        return serializers.ItemSerializer

    # TODO : over ride retrive method
    def list(self, request):
        # TODO : validate parameters; it returns [] now
        item_uuid = request.query_params.get('item_uuid', False)
        item_type = request.query_params.get('type', False)
        # print(item_type[0])
        item_type_id = type_mappings.get(item_type, False)
        print(item_type_id)
        query_parameters = request.query_params
        # print(query_parameters)
        if item_type_id and not item_uuid:
            queryset = models.Item.objects.filter(
                item_type_id=int(item_type_id))
            page = self.paginate_queryset(queryset)
            # TODO Wrong item type
            if query_parameters == {}:
                print("Item Type Data")
                queryset = models.Item.objects.filter(
                    item_type_id=int(item_type_id))
            else:
                print("Elastic Data")
                ids = query_elastic(query_parameters, item_type)
                if ids == None:
                    queryset = models.Item.objects.all()
                else:
                    queryset = models.Item.objects.filter(pk__in=ids)
            #page = self.paginate_queryset(queryset)
        elif item_uuid:
            print("Single item")
            queryset = models.Item.objects.filter(item_uuid=item_uuid)
        else:
            print("All Item")
            queryset = models.Item.objects.all()
            # TODO : paginate all responses
            page = self.paginate_queryset(queryset)
            # Temporay fix, remove after all responses are paginated
            # queryset = page
        items = list()
        item_type_object = utils.Creator().create_object(item_type)
        if queryset.exists() and item_type:
            for item_object in queryset:
                # if item_type == 'audio':
                #     output = utils.get_audio_info(item_object)
                # elif item_type == "video":
                #     output = utils.get_item_info(item_object)
                # else:
                #     output = utils.get_item_info(item_object)
                #item_info = utils.get_item_info(item)
                output = item_type_object.item_info(item_object)
                items.append(output)
        else:
            #serializer = self.get_serializer(page, many=True)
            print("Default Response")
            items = serializers.ItemDetailedSerializer(page, many=True)
            return self.get_paginated_response(items.data)
        return Response(items)

    def perform_create(self, serializer):
        user_email = self.request.user
        user_uuid = get_object_or_404(UserProfile, email=user_email).user_uuid
        print(user_uuid)
        serializer.save()

# @csrf_exempt
# @authentication_classes([TokenAuthentication])
# @permission_classes([IsAuthenticated])


@api_view(['GET'])
def metadata(request):
    queryset = models.Item.objects.all()
    #data = serializers.ItemSerializer(queryset, many=True).data
    data = [q.metadata for q in queryset]
    response_data = {
        'album': set([album for item in data for album in item['album']]),
        'artist': set([artist for item in data for artist in item['artist'].split(',')]),
        'category': set([category for item in data for category in item['category'].split(',')])
    }
    return Response(response_data)


@api_view(['GET'])
def dir_structure(request):
    # TODO: Save path some where in env
    # path = '/home/dipak/medialib/restApisMySQL/api'
    path = request.query_params.get('path', None)
    depth = request.query_params.get('depth', 5)
    if path == None:
        return Response({'pass value for path parameter'}, status=status.HTTP_400_BAD_REQUEST)
    directory = pathlib.Path(path)
    if not directory.is_dir():
        return Response({'Invalid Path'}, status=status.HTTP_404_NOT_FOUND)
    output = list()
    utils.get_dir_structure(path, output, depth)
    return Response(output)


@api_view(['GET'])
def favourites(request):
    # csrf_context = RequestContext(request)
    # print(dir(request))
    # print(request)
    # https://www.revsys.com/tidbits/tips-using-djangos-manytomanyfield/
    user = get_object_or_404(
        UserProfile, email=request.user)
    item_type = request.query_params.get('type', False)
    item_type_id = type_mappings.get(item_type, False)
    print(user)
    if item_type_id:
        result = user.item_favourites.filter(item_type=item_type_id)
        # TODO: Raise exception when item type is not implemented
        item_type_object = utils.Creator().create_object(item_type)
        data = list()
        for item in result:
            serialized_data = item_type_object.item_info(item)
            data.append(serialized_data)
        return Response(data)
    else:
        result = user.item_favourites.all()
        data = serializers.ItemDetailedSerializer(
            result, many=True).data
        return Response(data)
    # data = list()
    # for item in result:
    #     # TODO : Use factory pattern here
    #     serialized_data = utils.get_item_info(item)
    #     data.append(serialized_data)
    # return Response(data)
    # if request.method == 'GET':
    #     user = get_object_or_404(
    #         UserProfile, email=request.user)
    #     print(user)
    #     result = user.item_favourites.all()
    #     print(type(result))
    #     data = list()
    #     for item in result:
    #         print(item)
    #         serialized_data = serializers.ItemDetailedSerializer(item)
    #         data.append(serialized_data)
    #     return Response({data})
    #     item = get_object_or_404(models.Item, item_uuid=pk)
    #     is_liked = False
    #     user_uuid = get_object_or_404(
    #         UserProfile, email=request.user).user_uuid
    #     if item.likes.filter(user_uuid=user_uuid).exists():
    #         is_liked = True
    #     return Response({"is_liked": is_liked})
    # print(request.user)
    # # TODO : wrong username
    # item = get_object_or_404(models.Item, item_uuid=pk)
    # user_uuid = get_object_or_404(UserProfile, email=request.user).user_uuid
    # # print(user_uuid)
    # if item.likes.filter(user_uuid=user_uuid).exists():
    #     item.likes.remove(request.user)
    # else:
    #     item.likes.add(request.user)
    # # TODO : Return Item - User reverse URL
    # # print(item.get_absolute_url())
    # return HttpResponseRedirect(item.get_absolute_url())


#
@api_view(['POST', 'GET'])
def favourite_view(request, pk):
    if request.method == 'GET':
        item = get_object_or_404(models.Item, item_uuid=pk)
        is_favourite = False
        user_uuid = get_object_or_404(
            UserProfile, email=request.user).user_uuid
        if item.favourites.filter(user_uuid=user_uuid).exists():
            is_favourite = True
        return Response({"is_favourite": is_favourite})
    if request.user.is_authenticated:
        item = get_object_or_404(models.Item, item_uuid=pk)
        user_uuid = get_object_or_404(
            UserProfile, email=request.user).user_uuid
        if item.favourites.filter(user_uuid=user_uuid).exists():
            item.favourites.remove(request.user)
        else:
            item.favourites.add(request.user)
        return HttpResponseRedirect(item.get_absolute_url())
    else:
        return HttpResponse("Unauthorized", status=401)


# Get a single item
@api_view(['GET'])
def ItemView(request, pk=None):
    try:
        #item_object = get_object_or_404(models.Item, item_uuid=pk)
        print(request.query_params)
        # wrong pk raises exception
        item_object = models.Item.objects.get(pk=pk)
        item_type = request.query_params.get('type', 'video')
        if item_type == 'audio':
            output = utils.get_audio_info(item_object, pk)
        elif item_type == "video":
            output = utils.get_item_info(item_object, pk)
        else:
            output = utils.get_item_info(item_object, pk)
        return Response(output)
    # send 404 response when object is not found
    # TODO: Add these exceptions to ItemViewset
    except models.Item.DoesNotExist:
        #print("Unexpected error:", sys.exc_info()[0])
        return Response(status=status.HTTP_404_NOT_FOUND)
    except:
        return Response({'status': sys.exc_info()[0]}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class RatingViewset(viewsets.ModelViewSet):
    pagination_class = PageNumberPagination
    serializer_class = serializers.RatingSerializer
    queryset = models.Rating.objects.all()

    def perform_create(self, serializer):
        user_email = self.request.user
        user_uuid = get_object_or_404(UserProfile, email=user_email)
        serializer.save(user_uuid=user_uuid)

    def list(self, request):
        user_uuid = get_object_or_404(
            UserProfile, email=request.user).user_uuid
        item_uuid = request.query_params.get('item_uuid', None)
        if item_uuid:
            queryset = models.Rating.objects.filter(
                user_uuid=user_uuid, item_uuid=item_uuid)
        else:
            queryset = models.Rating.objects.filter(
                user_uuid=user_uuid)
        page = self.paginate_queryset(queryset)  # test
        serializer = serializers.RatingSerializer(page, many=True)
        return Response(serializer.data)
    # TODO : update rating on second post request

    def create(self, request):
        # print("Function called")
        user_uuid = get_object_or_404(
            UserProfile, email=request.user).user_uuid
        try:
            item_uuid = request.data['item_uuid']
            user_rating = request.data['rating']
        except KeyError:
            data = {"rating or item_uuid doesn't exist"}
            return Response(data=data, status=422)

        queryset = models.Rating.objects.filter(
            user_uuid=user_uuid, item_uuid=item_uuid)
        # print(len(queryset))
        if len(queryset) == 0:
            # print("Create new entry")
            return super().create(request)
        elif len(queryset) == 1:
            # print("Update existing data")
            rating = queryset[0]
            rating.rating = user_rating
            rating.save()
            serializer = serializers.RatingSerializer(rating)
            return Response(serializer.data, 200)
        else:
            # Multiple rating exist for a item
            data = {"Multiple rating exist for a given item"}
            return Response(data=data, status=500)


class BookmarkViewset(viewsets.ModelViewSet):
    serializer_class = serializers.BookmarkSerializer
    queryset = models.Bookmark.objects.all()
    pagination_class = PageNumberPagination

    def perform_create(self, serializer):
        user_email = self.request.user
        user_uuid = get_object_or_404(UserProfile, email=user_email)
        print(user_uuid)
        serializer.save(user_uuid=user_uuid)

    def list(self, request):
        user_uuid = get_object_or_404(
            UserProfile, email=request.user).user_uuid
        item_uuid = request.query_params.get('item_uuid', False)
        item_type = request.query_params.get('type', "video")
        item_type_id = type_mappings.get(item_type, False)
        print(item_type)
        # Case 1 : end point has item_uuid
        # Case 1.1  : item_type is present
        # return bookmarks of that particular item
        # Case 1.2 : item_type is None
        # find item_type from item_uuid and return same response as above
        if item_uuid:
            if item_type:
                queryset = models.Bookmark.objects.filter(
                    user_uuid=user_uuid, item_uuid=item_uuid, bookmark_type=item_type_id)
                page = self.paginate_queryset(queryset)  # test
                item_type_object = utils.Creator().create_object(item_type)
                output = item_type_object.item_bookmarks(page)
                return Response(output)
            else:
                # TODO Case 1.2
                # Find item_type and then copy code from the above case
                raise RuntimeError('Implementation in progress')
        # Case 2 : end point has item_type and item_uuid is None
        # Return all the bookmarks of the particular item_type for the logged in user
        if item_type and not item_uuid:
            unique_items = models.Bookmark.objects.filter(
                user_uuid=user_uuid, bookmark_type=item_type_id).order_by().values('item_uuid').distinct()
            page = self.paginate_queryset(unique_items)  # test
            item_type_object = utils.Creator().create_object(item_type)
            output = item_type_object.user_bookmarks(
                page, item_type_id)
            return Response(output)
        # Case 3 : edn point don't have item_uuid and item_type
        # Return all the bookmarks of the looged in user in db format
        else:
            # Return all bookmarks of the logged in user
            queryset = models.Bookmark.objects.filter(
                user_uuid=user_uuid)
            page = self.paginate_queryset(queryset)
            serializer = serializers.BookmarkSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)
            # return Response(serializer.data)


class HistoryViewset(viewsets.ModelViewSet):
    serializer_class = serializers.HistorySerializer
    queryset = models.History.objects.all()
    pagination_class = PageNumberPagination

    def perform_create(self, serializer):
        user_email = self.request.user
        user_uuid = get_object_or_404(UserProfile, email=user_email)
        serializer.save(user_uuid=user_uuid)

    # No need to use single item
    def list(self, request):
        user_uuid = get_object_or_404(
            UserProfile, email=request.user).user_uuid
        item_uuid = request.query_params.get('item_uuid', False)
        item_type = request.query_params.get('type', False)
        item_type_id = type_mappings.get(item_type, False)
        if item_uuid:
            queryset = models.History.objects.filter(
                user_uuid=user_uuid, item_uuid=item_uuid)
        elif item_type_id:
            queryset = models.History.objects.filter(
                user_uuid=user_uuid, history_type=item_type_id).order_by('-visited_at')
        else:
            queryset = models.History.objects.filter(
                user_uuid=user_uuid)
        page = self.paginate_queryset(queryset)
        serializer = serializers.HistorySerializer(page, many=True)
        if item_type:
            item_type_object = utils.Creator().create_object(item_type)
            output = item_type_object.user_history(serializer)
            return Response(output)
        else:
            return Response(serializer.data)
        # response_data = list()
        # for element in serializer.data:
        #     element_data = dict()
        #     item_uuid = element['item_uuid']
        #     item_object = models.Item.objects.get(pk=item_uuid)
        #     item_data = utils.get_item_info(item_object)
        #     element_data['rating'] = item_data['rating']
        #     element_data['view'] = item_data['view']
        #     element_data['duration'] = item_data['duration']
        #     element_data['title'] = item_data['title']
        #     element_data['src'] = item_data['src']
        #     element_data['time'] = element['visited_at']
        #     element_data['id'] = element['history_uuid']
        #     response_data.append(element_data)
        # return Response(response_data)

    def update(self, request, pk):
        return super().update(request, pk=pk)

    def create(self, request):
        try:
            print("Function called")
            user_uuid = get_object_or_404(
                UserProfile, email=request.user).user_uuid
            try:
                item_uuid = request.data['item_uuid']
            except:
                data = {"item_uuid missing in request body"}
                return Response(data=data, status=422)
            # Handle invalid data
            try:
                queryset = models.History.objects.filter(
                    user_uuid=user_uuid, item_uuid=item_uuid)
            except Exception as e:
                print(e)
                return Response(data={e}, status=422)
            print(len(queryset))
            if len(queryset) == 0:
                print("Create new entry")
                return super().create(request)
            elif len(queryset) == 1:
                print("Update existing data")
                history = queryset[0]
                data = request.data
                try:
                    history.visited_at = data['visited_at']
                    history.viewed_time = data['viewed_time']
                    history.metrics = data['metrics']
                    history.view_counted = data['view_counted']
                except KeyError:
                    data = {"some details are missing in request body"}
                    return Response(data=data, status=422)
                history.save()
                serializer = serializers.HistorySerializer(history)
                return Response(serializer.data)
            else:
                data = {"Multiple entry in history table for given item"}
                return Response(data=data, status=500)
        except Exception as e:
            return Response(data={str(e)}, status=500)


class ListViewset(viewsets.ModelViewSet):
    pagination_class = PageNumberPagination
    serializer_class = serializers.ListSerializer
    queryset = models.List.objects.all()

    def perform_create(self, serializer):
        user_email = self.request.user
        user_uuid = get_object_or_404(UserProfile, email=user_email)
        serializer.save(user_uuid=user_uuid)

    def list(self, request):
        user_uuid = get_object_or_404(
            UserProfile, email=request.user).user_uuid
        item_uuid = request.query_params.get('item_uuid', False)
        item_type = request.query_params.get('type', False)
        item_type_id = type_mappings.get(item_type, False)
        if item_uuid and item_type_id:
            print("Check whether item is in playlist")
            # Find type of the item or receive it from api call
            queryset = models.List.objects.filter(
                user_uuid=user_uuid, list_type=item_type_id)
            page = self.paginate_queryset(queryset)
            serializer = serializers.ListSerializer(page, many=True)
            data = list()
            for element in serializer.data:
                temp_data = dict()
                temp_data['name'] = element['name']
                temp_data['list_uuid'] = element['list_uuid']
                if item_uuid in element['items']:
                    temp_data['is_added'] = True
                else:
                    temp_data['is_added'] = False
                data.append(temp_data)
            if len(data) == 0:
                data = {}
            return Response(data)
        if item_type_id:
            print(item_type)
            print("User list of the particular type")
            queryset = models.List.objects.filter(
                user_uuid=user_uuid, list_type=item_type_id)
            serializer = serializers.ListSerializer(queryset, many=True)
            item_type_object = utils.Creator().create_object(item_type)
            output = item_type_object.user_playlist(serializer)
            return Response(output)
        else:
            print("Default output")
            queryset = models.List.objects.filter(
                user_uuid=user_uuid)
            page = self.paginate_queryset(queryset)
            serializer = serializers.ListSerializer(page, many=True)
            return Response(serializer.data)

    def partial_update(self, request, pk=None):
        user_uuid = get_object_or_404(
            UserProfile, email=request.user).user_uuid
        # Find given list for the logged in user
        list_data = get_object_or_404(
            models.List, list_uuid=pk, user_uuid=user_uuid)
        serializer = serializers.ListSerializer(
            list_data)
        # serializer.is_valid(raise_exception=True)
        print("test")
        item_uuid = request.data.get('item_uuid', None)
        remove = request.data.get('remove', False)
        # Check if item+uuid is valid
        items = serializer.data['items']
        temp_data = serializer.data
        print(items)
        if models.Item.objects.filter(item_uuid=item_uuid).exists():
            if item_uuid not in items and remove == False:
                items.append(item_uuid)
                temp_data['items'] = items
            elif item_uuid in items and remove == True:
                items.remove(item_uuid)
                temp_data['items'] = items
            serializer = serializers.ListSerializer(
                list_data, data=temp_data)
            serializer.is_valid(raise_exception=True)
            temp_data = serializer.validated_data
            serializer.save()
        else:
            return Response({'Error': 'Inalid item_uuid'}, status=status.HTTP_404_NOT_FOUND)
        print("End")
        # serializer = serializers.ListSerializer(
        #     list_data, data=temp_data)
        # serializer.is_valid(raise_exception=True)
        temp_data = serializer.data
        # print(temp_data)
        # print(serializer.validated_data)
        # data = serializer.validated_data
        # serializer.save()
        return Response(temp_data)


class AssetViewset(viewsets.ModelViewSet):
    serializer_class = serializers.AssetSerializer
    queryset = models.Asset.objects.all()


class FileViewset(viewsets.ModelViewSet):
    serializer_class = serializers.FileSerializer
    queryset = models.File.objects.all()


class DeviceViewset(viewsets.ModelViewSet):
    serializer_class = serializers.DeviceSerializer
    queryset = models.Device.objects.all()
