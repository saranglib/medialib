import uuid
from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.conf import settings
from django.urls import reverse


class ItemType(models.Model):
    item_type_id = models.PositiveIntegerField(primary_key=True)
    item_type = models.CharField(max_length=50)

    def __str__(self):
        return self.item_type


class Item(models.Model):
    item_uuid = models.UUIDField(primary_key=True, default=uuid.uuid4)
    metadata = models.JSONField()
    tags = models.JSONField()
    views = models.IntegerField()
    item_type = models.ForeignKey(
        ItemType, on_delete=models.CASCADE, related_name='item_item_type')
    # TODO : On delete testing
    likes = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name='item_likes', blank=True)
    favourites = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name='item_favourites', blank=True)

    def total_likes(self):
        return self.likes.count()

    def total_favourites(self):
        return self.favourites.count()

    def get_absolute_url(self):
        return reverse('api:get_item', kwargs={"pk": self.item_uuid})

    def __str__(self):
        return str(self.item_uuid)


class Rating(models.Model):
    user_uuid = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="user_rating")
    item_uuid = models.ForeignKey(
        Item, on_delete=models.CASCADE, related_name="item_rating")
    rating = models.DecimalField(decimal_places=1, max_digits=2)

    class Meta:
        unique_together = (("user_uuid", "item_uuid"),)

    def __str__(self):
        # Join two foreign key
        return str(self.user_uuid)


class Bookmark(models.Model):
    bookmark_uuid = models.UUIDField(primary_key=True, default=uuid.uuid4)
    bookmark_type = models.ForeignKey(
        ItemType, on_delete=models.CASCADE, related_name="itemtype_bookmark")
    name = models.TextField()
    user_uuid = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="user_bookmark")
    item_uuid = models.ForeignKey(
        Item, on_delete=models.CASCADE, related_name="item_bookmark")
    bookmark = models.CharField(max_length=50)

    def __str__(self):
        return str(self.bookmark_uuid)


class History(models.Model):
    history_uuid = models.UUIDField(primary_key=True, default=uuid.uuid4)
    user_uuid = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="user_history")
    item_uuid = models.ForeignKey(
        Item, on_delete=models.CASCADE, related_name="item_history")
    history_type = models.ForeignKey(
        ItemType, on_delete=models.CASCADE, related_name="itemtype_history")
    visited_at = models.DateTimeField()
    viewed_time = models.IntegerField()
    view_counted = models.BooleanField()
    metrics = models.JSONField()

    def __str__(self):
        return str(self.history_uuid)


class List(models.Model):
    list_uuid = models.UUIDField(primary_key=True, default=uuid.uuid4)
    list_type = models.ForeignKey(
        ItemType, on_delete=models.CASCADE, related_name="itemtype_list")
    name = models.TextField(max_length=100)
    items = ArrayField(models.UUIDField(), null=True, blank=True)
    user_uuid = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="user_list")

    def __str__(self):
        return str(self.list_uuid)


class Asset(models.Model):
    ASSET_TYPE = [
        (100, 'video'),
        (101, 'video_proxy'),
        (200, 'image'),
        (201, 'image_preview'),
        (202, 'image_thumbnail'),
        (203, 'video_thumbnail_sprite'),
        (300, 'audio'),
        (301, 'audio_proxy')
    ]
    asset_uuid = models.UUIDField(primary_key=True, default=uuid.uuid4)
    item_uuid = models.ForeignKey(
        Item, on_delete=models.CASCADE, related_name="item_asset")
    asset_type = models.PositiveIntegerField(choices=ASSET_TYPE)
    mime_type = models.CharField(max_length=150)
    metadata = models.JSONField()


class File(models.Model):
    file_uuid = models.UUIDField(primary_key=True, default=uuid.uuid4)
    asset_uuid = models.ForeignKey(
        Asset, on_delete=models.CASCADE, related_name='asset_file')
    device_uuid = models.ForeignKey(
        'Device', on_delete=models.CASCADE, related_name='device_file')
    path = models.CharField(max_length=500)
    filename = models.CharField(max_length=255)


class Device(models.Model):
    device_uuid = models.UUIDField(primary_key=True, default=uuid.uuid4)
    site_id = models.IntegerField()
    device_type = models.PositiveIntegerField()
    details = models.JSONField()
    resource_path = models.TextField()