import os
import datetime
from abc import ABCMeta, abstractmethod
from django.db import models as db_models
from rest_framework.response import Response
from api import serializers
from api import models
from django.conf import settings
import subprocess
import pathlib

asset_mappings = {
    '100': 'video',
    '101': 'video_proxy',
    '200': 'image',
    '201': 'image_preview',
    '202': 'image_thumbnail',
    '300': 'audio',
    '301': 'audio_proxy'
}

type_mappings = {
    'video': '100',
    'image': '200',
    'audio': '300'
}


def get_item_info(item_object, pk=None, type="video"):
    data = serializers.ItemDetailedSerializer(item_object).data
    # print(item_object['user_uuid'])
    # print(data.get('favourites'))
    # TODO : No rating
    if pk == None:
        pk = data.get('item_uuid', None)
    ratings = models.Rating.objects.filter(
        item_uuid=pk).aggregate(avg_rating=db_models.Avg('rating'))

    item = dict()
    item['id'] = data.get('item_uuid', None)
    item['rating'] = ratings.get('avg_rating', None)
    item['item_type'] = data.get('item_type', None)
    item['view'] = data.get("views", None)
    item['title'] = data.get("metadata", {}).get("title", None)
    item['duration'] = data.get("metadata", {}).get("duration", None)
    item['favourites'] = item_object.total_favourites()
    item['likes'] = item_object.total_likes()
    # print(request.query_params.get('type', None))
    file_sources = dict()
    for asset in data.get('item_asset', None):
        # Check if asset file is not empty
        if bool(asset.get('asset_file', [])):
            # TODO : check path on various os
            source = os.path.join(asset.get('asset_file', [{}])[0].get(
                'path', None), asset.get('asset_file', [{}])[0].get('filename', None))
        else:
            source = ""
        asset_type = asset.get('asset_type', 'unknown')
        file_sources[asset_mappings.get(
            str(asset_type), 'unknown')] = source
    item['src'] = file_sources
    return item


def get_audio_info(item_object, pk=None):
    data = serializers.ItemDetailedSerializer(item_object).data
    # TODO : No rating
    if pk == None:
        pk = data.get('item_uuid', None)
    ratings = models.Rating.objects.filter(
        item_uuid=pk).aggregate(avg_rating=db_models.Avg('rating'))
    item = dict()
    item['title'] = data.get("metadata", {}).get("title", None)
    item['type'] = data.get('item_type', None)
    item['duration'] = data.get("metadata", {}).get("duration", None)
    item['artist'] = data.get("metadata", {}).get("artist", None)
    item['album'] = data.get("metadata", {}).get("album", None)
    item['category'] = data.get("metadata", {}).get("category", None)
    item['rating'] = ratings.get('avg_rating', None)
    item['view'] = data.get("views", None)
    item['favourites'] = item_object.total_favourites()
    item['trackInfo'] = data.get("metadata", {}).get("trackInfo", None)
    item['likes'] = item_object.total_likes()
    item['item_type'] = data.get('item_type', None)
    item['id'] = data.get('item_uuid', None)
    file_sources = dict()
    for asset in data.get('item_asset', None):
        # Check if asset file is not empty
        if bool(asset.get('asset_file', [])):
            # TODO : check path on various os
            source = os.path.join(asset.get('asset_file', [{}])[0].get(
                'path', None), asset.get('asset_file', [{}])[0].get('filename', None))
        else:
            source = ""
        asset_type = asset.get('asset_type', 'unknown')
        file_sources[asset_mappings.get(
            str(asset_type), 'unknown')] = source
    item['src'] = file_sources.get('audio', "")
    item['sources'] = file_sources
    return item


class BaseView(metaclass=ABCMeta):
    @staticmethod
    @abstractmethod
    def item_info():
        "An abstract item info method"

    @staticmethod
    @abstractmethod
    def user_history():
        "An abstract method to list history"

    @staticmethod
    @abstractmethod
    def user_bookmarks():
        "An abstract method to list bookmark"

    @staticmethod
    @abstractmethod
    def item_bookmarks():
        "An abstract method to get item bookmark"

    @staticmethod
    @abstractmethod
    def user_playlist():
        "An abstract method to get user playlist"


class VideoView(BaseView):

    def __init__(self):
        self.name = "VideoView"

    def item_info(self, item_object, pk=None):
        data = serializers.ItemDetailedSerializer(item_object).data
        # TODO : No rating
        if pk == None:
            pk = data.get('item_uuid', None)
        ratings = models.Rating.objects.filter(
            item_uuid=pk).aggregate(avg_rating=db_models.Avg('rating'))

        item = dict()
        item['id'] = data.get('item_uuid', None)
        item['rating'] = ratings.get('avg_rating', None)
        item['item_type'] = data.get('item_type', None)
        item['view'] = data.get("views", None)
        item['title'] = data.get("metadata", {}).get("title", None)
        item['duration'] = data.get("metadata", {}).get("duration", None)
        item['favourites'] = item_object.total_favourites()
        item['likes'] = item_object.total_likes()
        # print(request.query_params.get('type', None))
        file_sources = dict()
        for asset in data.get('item_asset', None):
            # Check if asset file is not empty
            if bool(asset.get('asset_file', [])):
                # TODO : check path on various os
                source = os.path.join(asset.get('asset_file', [{}])[0].get(
                    'path', None), asset.get('asset_file', [{}])[0].get('filename', None))
            else:
                source = ""
            asset_type = asset.get('asset_type', 'unknown')
            file_sources[asset_mappings.get(
                str(asset_type), 'unknown')] = source
        item['src'] = file_sources
        return item

    def user_history(self, serializer):
        history_edited = list()
        for el in serializer.data:
            item_data = dict()
            single_item = models.Item.objects.get(pk=el['item_uuid'])
            #item_s_data = utils.get_item_info(single_item)
            #video_obj = VideoView()
            item_s_data = self.item_info(single_item)
            item_data['id'] = item_s_data['id']
            item_data['date'] = el['visited_at']
            item_data['title'] = item_s_data['title']
            item_data['src'] = item_s_data['src']
            item_data['rating'] = item_s_data['rating']
            item_data['view'] = item_s_data['view']
            item_data['duration'] = item_s_data['duration']
            history_edited.append(item_data)
        return history_edited

    def user_bookmarks(self, unique_items, item_type_id):
        bookmark_edited = list()
        for el in unique_items:
            item_data = dict()
            single_item = models.Item.objects.get(pk=el['item_uuid'])
            #video_obj = VideoView()
            item_s_data = self.item_info(single_item)
            item_data['id'] = item_s_data['id']
            item_data['title'] = item_s_data['title']
            list_bookmarks = models.Bookmark.objects.filter(
                item_uuid=el['item_uuid'], bookmark_type=item_type_id)
            list_of_bookmarks_for_single_item = list()
            for bm in list_bookmarks:
                bookmarks_for_single_item = dict()
                single_item = models.Bookmark.objects.get(pk=str(bm))
                si_serializer = serializers.BookmarkSerializer(single_item)
                bookmarks_for_single_item['id'] = si_serializer.data['bookmark_uuid']
                bookmarks_for_single_item['time'] = si_serializer.data['bookmark']
                bookmarks_for_single_item['description'] = si_serializer.data['name']
                list_of_bookmarks_for_single_item.append(
                    bookmarks_for_single_item)
            item_data['data'] = list_of_bookmarks_for_single_item
            bookmark_edited.append(item_data)
        return bookmark_edited

    def item_bookmarks(self, item_bks):
        list_of_bookmarks_for_single_item = list()
        for bm in item_bks:
            bookmarks_for_single_item = dict()
            single_item = models.Bookmark.objects.get(pk=str(bm))
            si_serializer = serializers.BookmarkSerializer(single_item)
            bookmarks_for_single_item['id'] = si_serializer.data['bookmark_uuid']
            bookmarks_for_single_item['time'] = si_serializer.data['bookmark']
            bookmarks_for_single_item['description'] = si_serializer.data['name']
            list_of_bookmarks_for_single_item.append(bookmarks_for_single_item)
        return list_of_bookmarks_for_single_item

    def user_playlist(self, serializer):
        playlist_edited = list()
        for el in serializer.data:
            print(el)
            item_data = dict()
            item_data['id'] = el['list_uuid']
            item_data['name'] = el['name']
            list_items = el['items']
            list_of_video_for_single_playlist = list()
            for pl in list_items:
                print(pl)
                item_object = models.Item.objects.get(item_uuid=str(pl))
                output = self.item_info(item_object, str(pl))
                list_of_video_for_single_playlist.append(output)
            item_data['videos'] = list_of_video_for_single_playlist
            playlist_edited.append(item_data)
        return playlist_edited


class AudioView(BaseView):
    def __init__(self):
        self.name = "VideoView"

    def item_info(self, item_object, pk=None):
        data = serializers.ItemDetailedSerializer(item_object).data
        # TODO : No rating
        if pk == None:
            pk = data.get('item_uuid', None)
        ratings = models.Rating.objects.filter(
            item_uuid=pk).aggregate(avg_rating=db_models.Avg('rating'))
        item = dict()
        item['title'] = data.get("metadata", {}).get("title", None)
        item['type'] = data.get('item_type', None)
        item['duration'] = data.get("metadata", {}).get("duration", None)
        item['artist'] = data.get("metadata", {}).get("artist", None)
        item['album'] = data.get("metadata", {}).get("album", None)
        item['category'] = data.get("metadata", {}).get("category", None)
        item['rating'] = ratings.get('avg_rating', None)
        item['view'] = data.get("views", None)
        item['favourites'] = item_object.total_favourites()
        item['trackInfo'] = data.get("metadata", {}).get("trackInfo", None)
        item['likes'] = item_object.total_likes()
        item['item_type'] = data.get('item_type', None)
        item['id'] = data.get('item_uuid', None)
        file_sources = dict()
        for asset in data.get('item_asset', None):
            # Check if asset file is not empty
            if bool(asset.get('asset_file', [])):
                # TODO : check path on various os
                source = os.path.join(asset.get('asset_file', [{}])[0].get(
                    'path', None), asset.get('asset_file', [{}])[0].get('filename', None))
            else:
                source = ""
            asset_type = asset.get('asset_type', 'unknown')
            file_sources[asset_mappings.get(
                str(asset_type), 'unknown')] = source
        item['src'] = file_sources.get('audio', "")
        item['sources'] = file_sources
        return item

    def user_history(self, serializer):
        history_edited = list()
        for el in serializer.data:
            item_data = dict()
            single_item = models.Item.objects.get(pk=el['item_uuid'])
            #item_s_data = utils.get_item_info(single_item)
            #video_obj = VideoView()
            item_s_data = self.item_info(single_item)
            item_data['id'] = item_s_data['id']
            date_time = datetime.datetime.strptime(
                el['visited_at'], "%Y-%m-%dT%H:%M:%SZ")
            item_data['date'] = date_time.date()
            item_data['title'] = item_s_data['title']
            item_data['src'] = item_s_data['src']
            item_data['rating'] = item_s_data['rating']
            item_data['view'] = item_s_data['view']
            item_data['duration'] = item_s_data['duration']
            item_data['time'] = date_time.time()
            history_edited.append(item_data)
        return history_edited

    def user_bookmarks(self, unique_items, item_type_id):
        bookmark_edited = list()
        for el in unique_items:
            item_data = dict()
            single_item = models.Item.objects.get(pk=el['item_uuid'])
            #video_obj = VideoView()
            item_s_data = self.item_info(single_item)
            item_data['id'] = item_s_data['id']
            item_data['title'] = item_s_data['title']
            list_bookmarks = models.Bookmark.objects.filter(
                item_uuid=el['item_uuid'], bookmark_type=item_type_id)
            list_of_bookmarks_for_single_item = list()
            for bm in list_bookmarks:
                bookmarks_for_single_item = dict()
                single_item = models.Bookmark.objects.get(pk=str(bm))
                si_serializer = serializers.BookmarkSerializer(single_item)
                bookmarks_for_single_item['id'] = si_serializer.data['bookmark_uuid']
                bookmarks_for_single_item['time'] = si_serializer.data['bookmark']
                bookmarks_for_single_item['description'] = si_serializer.data['name']
                list_of_bookmarks_for_single_item.append(
                    bookmarks_for_single_item)
            item_data['data'] = list_of_bookmarks_for_single_item
            bookmark_edited.append(item_data)
        return bookmark_edited

    def item_bookmarks(self, item_bks):
        list_of_bookmarks_for_single_item = list()
        for bm in item_bks:
            bookmarks_for_single_item = dict()
            single_item = models.Bookmark.objects.get(pk=str(bm))
            si_serializer = serializers.BookmarkSerializer(single_item)
            bookmarks_for_single_item['id'] = si_serializer.data['bookmark_uuid']
            bookmarks_for_single_item['time'] = si_serializer.data['bookmark']
            bookmarks_for_single_item['description'] = si_serializer.data['name']
            list_of_bookmarks_for_single_item.append(bookmarks_for_single_item)
        return list_of_bookmarks_for_single_item

    def user_playlist(self, serializer):
        playlist_edited = list()
        for el in serializer.data:
            print(el)
            item_data = dict()
            item_data['id'] = el['list_uuid']
            item_data['name'] = el['name']
            list_items = el['items']
            list_of_video_for_single_playlist = list()
            for pl in list_items:
                print(pl)
                item_object = models.Item.objects.get(item_uuid=str(pl))
                output = self.item_info(item_object, str(pl))
                list_of_video_for_single_playlist.append(output)
            item_data['audios'] = list_of_video_for_single_playlist
            playlist_edited.append(item_data)
        return playlist_edited
        pass


class Creator:
    "The factory class"

    @staticmethod
    def create_object(type):
        if type == "video":
            return VideoView()
        elif type == "audio":
            return AudioView()
        return None


def get_file_metadata(filepath):
    command = ["exiftool"]
    command.append(filepath)
    output = subprocess.run(command, capture_output=True)
    if output.stderr != b'':
        print(output.stderr)
        return {"error": output.stderr.decode("utf-8")}
    metadata = dict()
    for item in output.stdout.decode("utf-8").split("\n"):
        if item == "":
            continue
        items = item.split(":")
        key = items[0].replace(" ", "")
        value = items[1].replace(" ", "")
        metadata[key] = value
    return metadata


def get_dir_structure(path, result, depth):
    if depth == 0:
        return
    if pathlib.Path(path).is_file():
        return
    else:
        dir_list = os.listdir(path)
        for item in dir_list:
            temp = dict()
            value = list()
            temp['name'] = item
            temp['children'] = value
            result.append(temp)
            get_dir_structure(os.path.join(path, item), value, depth-1)
