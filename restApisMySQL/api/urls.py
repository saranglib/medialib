from django.urls import path, include
from rest_framework.routers import DefaultRouter
from api import views

app_name = 'api'

router = DefaultRouter()
router.register('item', views.ItemViewset)
router.register('rating', views.RatingViewset)
router.register('bookmark', views.BookmarkViewset)
router.register('history', views.HistoryViewset)
router.register('list', views.ListViewset)
router.register('asset', views.AssetViewset)
router.register('file', views.FileViewset)
router.register('device', views.DeviceViewset)
router.register('item-types', views.ItemTypesViewset)
router.register('details', views.ItemDetailedViewset)

urlpatterns = [

    # path('like/<uuid:pk>/', views.LikeView, name='like_item'),
    path('get_metadata/', views.file_metadata, name='get_file_metadata'),
    path('favourites/', views.favourites, name='like_item'),
    path('items/<uuid:pk>/', views.ItemView, name='get_item'),
    path('media/<str:file>', views.file_view, name='get_file'),
    path('favourite/<uuid:pk>/', views.favourite_view, name='favourite_item'),
    path('metadata/', views.metadata, name='item_metadata'),
    path('datafiles/', views.dir_structure, name='dir_structure'),
    path('', include((router.urls, 'api')))
]
