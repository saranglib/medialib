from elasticsearch import Elasticsearch
import os

ES_HOST = os.environ.get('ES_HOST')
print(f'----------------{ES_HOST}---------------')

es = Elasticsearch(hosts=ES_HOST)


filter_mappings = {
    "artist": "metadata.artist",
    "album": "metadata.album",
    "title": "metadata.title",
    "subject": "metadata.category",
    "tags": "tags.tag.tag_name",
    "location": "tags.location.location_name"

}


def query_elastic(search_parameters, index_name):
    print(search_parameters)
    query = {
        'from': 0,
        'size': 10,
        'query': {
            'bool': {
            }
        }
    }
    filter_search = list()
    try:
        for key, value in search_parameters.items():
            value = value.replace(", ", " ")
            value = value.split(" ")
            value = [text.lower() for text in value]
            try:
                if key != "query" and key !="type":
                    key = filter_mappings[key]
                    filter_search.append({'terms': {key: value}})
            except:
                print(f'Unsupported Search Key : {key}')
    except KeyError:
        print("No values")
    try:
        multi_match_search = search_parameters["query"]
        multi_match = {
            'multi_match': {
                'query':    multi_match_search,
                'fields': ["metadata.artist", "metadata.album", "metadata.title^3", "metadata.category",
                           "tags.tag.tag_name", "tags.location.location_name"]
            }
        }
    except:
        multi_match_search = False
    if multi_match_search:
        query['query']['bool']['must'] = multi_match
    if filter_search:
        query['query']['bool']['filter'] = filter_search
    print(index_name, query)
    ids = list()
    # print(index_name)
    # print(ES_HOST)
    try:
        result = es.search(index=index_name, body=query)
    except ConnectionRefusedError:
        print('Unable to connect es')
        return None
    if result["hits"]["total"]["value"] > 0:
        for item in result["hits"]["hits"]:
            ids.append(item["_id"])
        print(ids, 'ids')
        return ids
    else:
        print('[]', 'ids')
        return []
