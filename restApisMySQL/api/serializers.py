from django.db.models import fields
from rest_framework import serializers
from rest_framework.response import Response
from api import models
import pprint


class ItemTypesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ItemType
        fields = '__all__'


class ItemSerializer(serializers.ModelSerializer):
    #item_type = serializers.StringRelatedField()

    class Meta:
        model = models.Item
        fields = '__all__'


class RatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Rating
        fields = '__all__'
        read_only_fields = ['user_uuid']


class BookmarkSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Bookmark
        fields = '__all__'
        read_only_fields = ['user_uuid']


class HistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.History
        fields = '__all__'
        read_only_fields = ['user_uuid']


class ListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.List
        fields = '__all__'
        read_only_fields = ['user_uuid']


class AssetSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Asset
        fields = '__all__'


class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.File
        fields = '__all__'


class DeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Device
        fields = '__all__'


class AssetDetailSerializer(AssetSerializer):
    asset_file = FileSerializer(many=True)


class ItemDetailedSerializer(ItemSerializer):
    #rating_item = RatingSerializer(many=True, read_only=True)
    item_asset = AssetDetailSerializer(many=True, read_only=True)


class DetailedFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.File
        exclude = ('asset_uuid',)


class DetailedAssetSerializer(serializers.ModelSerializer):
    asset_file = DetailedFileSerializer(many=True)

    class Meta:
        model = models.Asset
        exclude = ('item_uuid',)


class DetailedSerializer(serializers.ModelSerializer):
    item_asset = DetailedAssetSerializer(many=True)

    class Meta:
        model = models.Item
        fields = '__all__'

    def create(self, validated_data):
        assets_data = validated_data.pop('item_asset')
        item = models.Item.objects.create(**validated_data)
        for asset_data in assets_data:
            files_data = asset_data.pop('asset_file')
            asset = models.Asset.objects.create(item_uuid=item, **asset_data)
            for file_data in files_data:
                file_iinstance = models.File.objects.create(
                    asset_uuid=asset, **file_data)
        return item

    def update(self, instance, validated_data):
        assets_data = validated_data.pop('item_asset')
        assets = (instance.item_asset).all()
        assets = list(assets)
        instance = super().update(instance, validated_data)
        for asset_data in assets_data:
            asset = assets.pop(0)
            asset.asset_type = asset_data.get('asset_type', asset.asset_type)
            asset.mime_type = asset_data.get('mime_type', asset.mime_type)
            asset.metadata = asset_data.get('metadata', asset.metadata)
            asset.save()
            files_data = asset_data.pop('asset_file')
            files = (asset.asset_file).all()
            files = list(files)
            for file_data in files_data:
                file = files.pop(0)
                file.path = file_data.get('path', file.path)
                file.filename = file_data.get('filename', file.filename)
                file.device_uuid = file_data.get(
                    'device_uuid', file.device_uuid)
                file.save()
        return instance


class BulkDetailedSerializer(serializers.ListSerializer):
    child = DetailedSerializer()
    class Meta:
        fields = ['items']
    
    def create(self, validated_datas):
        create_objects_list = []
        for validated_data in validated_datas:
            assets_data = validated_data.pop('item_asset')
            item = models.Item.objects.create(**validated_data)
            for asset_data in assets_data:
                files_data = asset_data.pop('asset_file')
                asset = models.Asset.objects.create(item_uuid=item, **asset_data)
                for file_data in files_data:
                    file_iinstance = models.File.objects.create(
                        asset_uuid=asset, **file_data)
            create_objects_list.append(item)
        return create_objects_list