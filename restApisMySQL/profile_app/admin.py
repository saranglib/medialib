from django.contrib import admin

# Register your models here.
from profile_app import models

admin.site.register(models.UserProfile)
