from rest_framework import serializers
from profile_app import models


class UserProfileSerializer(serializers.ModelSerializer):
    """Serializer User Profile Object"""

    class Meta:
        model = models.UserProfile
        fields = ('user_uuid', 'email', 'name', 'password')
        extra_kwargs = {
            'password': {
                'write_only': True,
                'style': {'input_type': 'password'}
            }
        }

    def create(self, validated_data):
        """Create and return new user"""
        user = models.UserProfile.objects.create_user(
            email=validated_data['email'],
            name=validated_data['name'],
            password=validated_data['password']
        )
        return user
