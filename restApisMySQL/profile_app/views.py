from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from profile_app import serializers
from profile_app import models
from profile_app import permissions


class UserProfileViewset(viewsets.ModelViewSet):
    serializer_class = serializers.UserProfileSerializer
    queryset = models.UserProfile.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (permissions.UpdateOwnProfile,)
