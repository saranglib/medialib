from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken.views import ObtainAuthToken
from profile_app import views

router = DefaultRouter()
router.register('profile', views.UserProfileViewset)

urlpatterns = [
    #path('login/', ObtainAuthToken.as_view()),
    path('', include(router.urls))
]
