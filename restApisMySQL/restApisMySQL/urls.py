from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path
from rest_framework import permissions
from rest_framework.authtoken.views import obtain_auth_token
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

schema_view = get_schema_view(
    openapi.Info(
        title="MedialIb API",
        default_version='v1',
        description="MediaLib API",
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
)


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    #url(r'^', include('tutorials.urls')),
    path('api/', include('api.urls')),
    path('accounts/', include('profile_app.urls')),
    path('auth/login/', obtain_jwt_token),
    path('auth/refresh-token/', refresh_jwt_token),
    # path('auth/', include('rest_framework.urls')),
    path('get-token/', obtain_auth_token, name='api_token_auth'),
    path('docs/', schema_view.with_ui('swagger',
                                      cache_timeout=0), name='schema-swagger-ui')
]
